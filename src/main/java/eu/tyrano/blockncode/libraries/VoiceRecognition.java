package eu.tyrano.blockncode.libraries;

import de.maxhenkel.voicechat.api.VoicechatConnection;
import de.maxhenkel.voicechat.api.events.MicrophonePacketEvent;
import de.maxhenkel.voicechat.api.opus.OpusDecoder;
import eu.tyrano.blockncode.Block_n_code;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import net.minecraft.world.entity.player.Player;
import org.apache.commons.lang3.tuple.ImmutablePair;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class VoiceRecognition {

    private static final Map<UUID, ImmutablePair<ByteArrayOutputStream, OpusDecoder>> voices = Collections
            .synchronizedMap(new HashMap<>());
    private static final List<UUID> playersToListen = Collections
            .synchronizedList(new ArrayList<>());
    private static final Map<UUID, String> solutions = Collections
            .synchronizedMap(new HashMap<>());
    private static final Object recognizerLock = new Object();
    private static final AtomicBoolean isLocked = new AtomicBoolean(false);

    public static void packageReceived(MicrophonePacketEvent packetEvent) {
        VoicechatConnection voicechatConnection = packetEvent.getSenderConnection();
        if (voicechatConnection == null) {
            return;
        }
        Player player = (Player) voicechatConnection.getPlayer().getPlayer();
        if (packetEvent.getSenderConnection() != null) {
            UUID uuid = player.getUUID();
            if (playersToListen.contains(uuid)) {
                try {
                    ImmutablePair<ByteArrayOutputStream, OpusDecoder> pair = voices.get(uuid);
                    pair.left.write(VoiceChatPlugin.API.getAudioConverter()
                            .shortsToBytes(pair.right.decode(packetEvent
                                    .getPacket().getOpusEncodedData())));
                } catch (IOException e) {
                    Block_n_code.LOGGER.warn("An error occurred during microphone packet reading");
                }
            }
        }
    }

    public static void listen(UUID player) {
        playersToListen.add(player);
        voices.put(player, new ImmutablePair<>(new ByteArrayOutputStream(),
                VoiceChatPlugin.API.createDecoder()));
    }

    public static void stopListening(UUID player) {
        playersToListen.remove(player);
        if (voices.containsKey(player)) {
            try {
                ByteArrayOutputStream outputStream = voices.get(player).left;
                if (outputStream != null) {
                    outputStream.close();
                }
                OpusDecoder decoder = voices.get(player).right;
                if (decoder != null && !decoder.isClosed()) {
                    decoder.close();
                }
                voices.remove(player);
            } catch (IOException ignored) {

            }
        }
    }

    public static void reset() {
        voices.clear();
        playersToListen.clear();
        solutions.clear();
    }

    public static String getRecognitionString(CodeRuntime runtime) {
        String solution = solutions.get(runtime.getUUID());
        solutions.remove(runtime.getUUID());
        return solution == null ? "" : solution;
    }

    public static String speechRecognition(String[] possibilities, UUID player, CodeRuntime runtime) {
        if (player == null || !voices.containsKey(player)) {
            return "Player not found";
        }
        if (VoskManager.recognizer == null) {
            return "Vosk voice models are not initialized !";
        }

        new Thread(() -> {
            synchronized (recognizerLock) {
                if (isLocked.get()) {
                    try {
                        recognizerLock.wait();
                    } catch (InterruptedException ignored) {

                    }
                }
            }
            isLocked.set(true);
            ByteArrayInputStream bis = new ByteArrayInputStream(voices.get(player).left.toByteArray());

            try (AudioInputStream ais = new AudioInputStream(bis,
                    new AudioFormat(48000, 16,
                            1, false, true), bis.available())) {

                int nBytes;
                byte[] b = new byte[4096];
                while ((nBytes = ais.read(b)) >= 0) {
                    VoskManager.recognizer.acceptWaveForm(b, nBytes);
                }

                String finalResult = VoskManager.recognizer.getFinalResult().split("\"")[3];

                int bestScore = Integer.MAX_VALUE;
                String bestString = finalResult;
                for (String possibility : possibilities) {
                    if (!possibility.equals("")) {
                        int score = Levenshtein.levenshteinDistance(possibility, finalResult);
                        if (score < bestScore) {
                            bestString = possibility;
                            bestScore = score;
                        }
                    }
                }
                stopListening(player);
                solutions.put(runtime.getUUID(), bestString);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            } finally {
                isLocked.set(false);
                recognizerLock.notify();
            }
        }).start();
        return null;
    }
}