package eu.tyrano.blockncode.libraries;

import eu.tyrano.blockncode.Block_n_code;
import org.vosk.Model;
import org.vosk.Recognizer;

import java.io.File;
import java.io.IOException;

public class VoskManager {
    private static final String voiceModelsDirectory = "config" + File.separator + "voice-models";
    public static Model model;
    public static Recognizer recognizer;

    public static void init() {
        try {
            model = new Model(voiceModelsDirectory);
            recognizer = new Recognizer(model, 44100);
            Block_n_code.LOGGER.info("Voice model loaded");
        } catch (IOException e) {
            recognizer = null;
            Block_n_code.LOGGER.error("Failed to load VOSK voice-models folder !");
        }
    }
}
