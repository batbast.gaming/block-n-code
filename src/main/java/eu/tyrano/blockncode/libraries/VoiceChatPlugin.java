package eu.tyrano.blockncode.libraries;

import de.maxhenkel.voicechat.api.ForgeVoicechatPlugin;
import de.maxhenkel.voicechat.api.VoicechatApi;
import de.maxhenkel.voicechat.api.VoicechatPlugin;
import de.maxhenkel.voicechat.api.events.EventRegistration;
import de.maxhenkel.voicechat.api.events.MicrophonePacketEvent;
import eu.tyrano.blockncode.Block_n_code;

@ForgeVoicechatPlugin
public class VoiceChatPlugin implements VoicechatPlugin {

    public static VoicechatApi API;

    @Override
    public String getPluginId() {
        return Block_n_code.MOD_ID;
    }

    @Override
    public void initialize(VoicechatApi api) {
        API = api;
    }

    @Override
    public void registerEvents(EventRegistration registration) {
        registration.registerEvent(MicrophonePacketEvent.class, VoiceRecognition::packageReceived);
    }
}
