package eu.tyrano.blockncode.runtime;

public record RuntimeResult(ExecutionResult executionResult) {
}
