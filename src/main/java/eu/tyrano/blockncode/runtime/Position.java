package eu.tyrano.blockncode.runtime;

public class Position {

    private int position;

    public Position() {
        this.position = 0;
    }

    public void incrementPosition() {
        this.position++;
    }

    public int getPosition() {
        return position;
    }
}
