package eu.tyrano.blockncode.runtime;

import eu.tyrano.blockncode.backend.variable.Variable;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;

import java.util.UUID;

public class CodeRuntimeExtends extends CodeRuntime {

    private final CodeRuntime parentCodeRuntime;

    public CodeRuntimeExtends(Level world, BlockPos blockPos, CodeRuntime parentCodeRuntime) {
        super(world, blockPos, parentCodeRuntime.maxCodeLength, parentCodeRuntime.bunch, () -> {
        });
        this.parentCodeRuntime = parentCodeRuntime;
        this.robot = parentCodeRuntime.robot;
    }

    @Override
    public UUID getUUID() {
        return parentCodeRuntime.getUUID();
    }

    @Override
    protected void setExitFork(boolean value) {
        parentCodeRuntime.setExitFork(value);
    }

    @Override
    protected boolean mustExitFork() {
        return parentCodeRuntime.mustExitFork();
    }

    @Override
    public void setVariable(String name, Variable variable) {
        this.variables.put(name, variable);
    }

    @Override
    public Variable getVariable(String name) {
        Variable superVariable = this.parentCodeRuntime.getVariable(name);
        if (superVariable != null) {
            return superVariable;
        }
        return this.variables.get(name);
    }

    @Override
    public boolean consumeBlock(BlockPos block) {
        return parentCodeRuntime.consumeBlock(block);
    }

    @Override
    public int getMaxCodeLength() {
        return parentCodeRuntime.getMaxCodeLength();
    }

    @Override
    public String toString() {
        return parentCodeRuntime + "\nfork -> variables = " + this.variables;
    }
}
