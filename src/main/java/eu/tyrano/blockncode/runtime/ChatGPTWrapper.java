package eu.tyrano.blockncode.runtime;

import eu.tyrano.blockncode.mod.common.ModConfigs;

import java.io.IOException;
import java.util.*;

public class ChatGPTWrapper {
    // TODO: 08/03/2023 runtime request limit

    private static final Map<UUID, String> answers = Collections.synchronizedMap(new HashMap<>());
    private static final List<ChatBot.ChatMessage> messages = Collections.synchronizedList(new ArrayList<>());

    public static void sendMessage(String value, CodeRuntime runtime) {
        ChatBot chatBot = ModConfigs.getChatGPT();
        if (chatBot != null && answers.containsKey(runtime.getUUID())) {
            new Thread(() -> {
                try {
                    ChatBot.ChatCompletionResponse response = chatBot.generateResponse(new ChatBot
                            .ChatCompletionRequest("gpt-3.5-turbo",
                            List.of(new ChatBot.ChatMessage("user", value))));
                    answers.put(runtime.getUUID(), response.getChoices().get(0).getMessage().getContent());
                    messages.add(response.getChoices().get(0).getMessage());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }).start();
        }
    }

    public static List<ChatBot.ChatMessage> getAllMessages() {
        return messages;
    }

    public static String getMessage(CodeRuntime runtime) {
        String answer = answers.get(runtime.getUUID());
        answers.remove(runtime.getUUID());
        return answer != null ? answer : "";
    }
}
