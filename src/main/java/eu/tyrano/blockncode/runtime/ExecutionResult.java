package eu.tyrano.blockncode.runtime;

import eu.tyrano.blockncode.mod.common.entities.robot.RobotControl;

public class ExecutionResult {
    private final Result result;
    private final String error;
    private final int repeatFork;
    private final RobotControl control;

    public ExecutionResult(
            Result result,
            String error,
            int repeatFork) {
        this.result = result;
        this.error = error;
        this.repeatFork = repeatFork;
        this.control = null;
    }

    public ExecutionResult(
            Result result,
            String error,
            int repeatFork,
            RobotControl control) {
        this.result = result;
        this.error = error;
        this.repeatFork = repeatFork;
        this.control = control;
    }

    public RobotControl getControl() {
        return control;
    }

    public Result result() {
        return result;
    }

    public String error() {
        return error;
    }

    public int repeatFork() {
        return repeatFork;
    }
}
