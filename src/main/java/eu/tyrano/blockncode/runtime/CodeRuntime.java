package eu.tyrano.blockncode.runtime;

import eu.tyrano.blockncode.backend.variable.Variable;
import eu.tyrano.blockncode.libraries.VoiceRecognition;
import eu.tyrano.blockncode.mod.common.TickRuntimeHandler;
import eu.tyrano.blockncode.mod.common.blocks.code.BlockCodeBlank;
import eu.tyrano.blockncode.mod.common.entities.robot.RobotEntity;
import eu.tyrano.blockncode.mod.common.entities.robot.RuntimeDataProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class CodeRuntime {

    private final Level world;
    protected final BlockPos starterPos;
    protected final ArrayList<BlockPos> consumedBlocks = new ArrayList<>();
    protected final HashMap<String, Variable> variables = new HashMap<>();
    protected final ArrayList<CompiledBlock> blockCodeBases = new ArrayList<>();
    protected CodeRuntime currentlyExecutedRuntime;
    protected UUID listeningPlayer = null;
    protected int currentRuntimePosition;
    private final UUID uuid;

    protected int repetitionsInFork;
    protected Position positionInForkRepetitions;

    protected final int maxCodeLength;
    private final TickRuntimeHandler.OnRuntimeFinished onRuntimeFinished;
    protected final double bunch;
    protected final double bunchInvert;
    protected final RuntimeDataProvider initData;
    protected RobotEntity robot;
    protected boolean mustExitForkStatus = false;

    public CodeRuntime(Level world, BlockPos starterPos, int maxCodeLength, double bunch,
                       TickRuntimeHandler.OnRuntimeFinished onRuntimeFinished) {
        this(world, starterPos, maxCodeLength, bunch, onRuntimeFinished, null);
    }

    public CodeRuntime(Level world, BlockPos starterPos, int maxCodeLength, double bunch,
                       TickRuntimeHandler.OnRuntimeFinished onRuntimeFinished,
                       RuntimeDataProvider initData) {

        this.uuid = UUID.randomUUID();
        this.world = world;
        this.starterPos = starterPos;
        this.maxCodeLength = maxCodeLength;
        this.onRuntimeFinished = onRuntimeFinished;
        this.initData = initData;

        this.currentlyExecutedRuntime = this;
        this.currentRuntimePosition = 0;
        this.bunch = bunch;
        this.bunchInvert = this.bunch < 0 ? 1 / this.bunch : 0;
    }

    public void addListeningPlayer(UUID player) {
        VoiceRecognition.stopListening(listeningPlayer);
        listeningPlayer = player;
    }

    public UUID getUUID() {
        return uuid;
    }

    public RuntimeResult runBunchOfBlocksBase(int tick) {
        if (bunch < 1) {
            if (bunch == 0) {
                return new RuntimeResult(new ExecutionResult(Result.finished, null, 0));
            }
            if (bunch > 0 && tick % this.bunchInvert < 1) {
                return runBunchOfBlocks(new Position());
            }
            return runEverything();
        }
        return runBunchOfBlocks(new Position());
    }

    public void shutdown() {
        VoiceRecognition.stopListening(listeningPlayer);
        onRuntimeFinished.action();
    }

    public Level getWorld() {
        return world;
    }

    public BlockPos getStarterPos() {
        return starterPos;
    }

    public RuntimeResult runEverything() {
        for (CompiledBlock compiledBlock : blockCodeBases) {

            ExecutionResult executionResult = compiledBlock.codeBlock()
                    .execute(world, compiledBlock.pos(), this);

            if (executionResult.result() != Result.ok) {
                if (executionResult.result() == Result.robotAction
                    && executionResult.getControl() != null && this.robot != null) {
                    this.robot.setActionData(executionResult.getControl());
                } else if (executionResult.result() == Result.exitFork
                           && this instanceof CodeRuntimeExtends) {
                    return new RuntimeResult(new ExecutionResult(Result.exitFork, null, 0));
                } else {
                    return new RuntimeResult(executionResult);
                }
            }

            if (compiledBlock.optionalFork() != null) {
                for (int i = 0; i < executionResult.repeatFork(); i++) {
                    RuntimeResult result = compiledBlock.optionalFork().runEverything();
                    if (result.executionResult().result() != Result.ok) {
                        if (result.executionResult().result() == Result.error || result.executionResult().result() == Result.terminated) {
                            return result;
                        } else if (result.executionResult().result() == Result.exitFork) {
                            break;
                        }
                    }
                }
            }
        }
        return new RuntimeResult(new ExecutionResult(Result.finished, null, 0));
    }

    public RuntimeResult runBunchOfBlocks(Position consumedPosition) {
        if (blockCodeBases.size() == 0 || currentRuntimePosition >= blockCodeBases.size()) {
            return new RuntimeResult(new ExecutionResult(Result.finished, null, 0));
        }
        while (consumedPosition.getPosition() < bunch) {
            if (currentlyExecutedRuntime == this) {
                CompiledBlock compiledBlock = blockCodeBases
                        .get(currentRuntimePosition);

                ExecutionResult executionResult = compiledBlock.codeBlock()
                        .execute(world, compiledBlock.pos(), this);

                if (executionResult.result() != Result.ok) {
                    if (executionResult.result() == Result.robotAction
                        && executionResult.getControl() != null && this.robot != null) {
                        this.robot.setActionData(executionResult.getControl());
                    } else {
                        return new RuntimeResult(executionResult);
                    }
                }

                if (compiledBlock.optionalFork() != null) {
                    currentlyExecutedRuntime = compiledBlock.optionalFork();
                    positionInForkRepetitions = new Position();
                    repetitionsInFork = executionResult.repeatFork();
                } else {
                    currentRuntimePosition++;
                }
                consumedPosition.incrementPosition();
            }
            if (currentlyExecutedRuntime != this) {
                if (!(this instanceof CodeRuntimeExtends)) {
                    setExitFork(false);
                }
                while (positionInForkRepetitions.getPosition() < repetitionsInFork && !mustExitFork()) {
                    RuntimeResult result = currentlyExecutedRuntime
                            .runBunchOfBlocks(consumedPosition);
                    if (result.executionResult().result() != Result.ok) {
                        if (result.executionResult().result() == Result.error || result.executionResult().result() == Result.terminated) {
                            return result;
                        } else if (result.executionResult().result() == Result.exitFork) {
                            if (this instanceof CodeRuntimeExtends) {
                                return result;
                            }
                            setExitFork(true);
                            consumedPosition.incrementPosition();
                            break;
                        }
                        setExitFork(false);
                    }
                    if (consumedPosition.getPosition() >= bunch) {
                        return new RuntimeResult(new ExecutionResult(Result.ok, null, 0));
                    }
                    consumedPosition.incrementPosition();
                    positionInForkRepetitions.incrementPosition();
                    currentlyExecutedRuntime.resetExecution();
                    if (consumedPosition.getPosition() >= bunch) {
                        return new RuntimeResult(new ExecutionResult(Result.ok, null, 0));
                    }
                }
                currentlyExecutedRuntime = this;
                positionInForkRepetitions = new Position();
                currentRuntimePosition++;
            }
            if (currentRuntimePosition >= blockCodeBases.size()) {
                return new RuntimeResult(new ExecutionResult(Result.finished, null, 0));
            }
        }
        return new RuntimeResult(new ExecutionResult(Result.ok, null, 0));
    }

    private void resetExecution() {
        this.currentRuntimePosition = 0;
        this.currentlyExecutedRuntime = this;
    }

    protected void setExitFork(boolean value) {
        mustExitForkStatus = value;
    }

    protected boolean mustExitFork() {
        return mustExitForkStatus;
    }

    public void compile() {
        if (initData != null) {
            this.variables.putAll(initData.getVariables());
            if (robot == null) {
                this.robot = initData.getRobot();
            }
        }

        BlockPos pos = starterPos;

        Block block = world.getBlockState(pos).getBlock();
        if (block instanceof BlockCodeBlank blockCodeBase) {
            blockCodeBases.add(new CompiledBlock(blockCodeBase, pos, null));
        }

        while (true) {
            block = world.getBlockState(pos).getBlock();

            if (block instanceof BlockCodeBlank blockCodeBase && consumeBlock(pos)) {

                CompiledBlock compiledBlock = blockCodeBase.getNext(world, pos);

                if (compiledBlock == null) {
                    break;
                }

                pos = compiledBlock.pos();

                blockCodeBases.add(compiledBlock);
            } else {
                break;
            }
        }

        for (int i = 0; i < blockCodeBases.size(); i++) {
            CompiledBlock compiledBlock = blockCodeBases.get(i);
            blockCodeBases.set(i, new CompiledBlock(compiledBlock.codeBlock(), compiledBlock.pos(),
                    compiledBlock.codeBlock().getOptionalFork(world, compiledBlock.pos(), this)));
        }

        blockCodeBases.removeIf((toTest) -> toTest.codeBlock().getClass() == BlockCodeBlank.class);
    }

    public Variable getVariable(String name) {
        return this.variables.get(name);
    }

    public void setVariable(String name, Variable variable) {
        this.variables.put(name, variable);
    }

    public boolean consumeBlock(BlockPos block) {
        boolean isBlockAlreadyConsumed = consumedBlocks.contains(block);
        if (!isBlockAlreadyConsumed) {
            consumedBlocks.add(block);
            return consumedBlocks.size() <= maxCodeLength;
        } else {
            return false;
        }
    }

    public RobotEntity getRobot() {
        return robot;
    }

    public int getMaxCodeLength() {
        return maxCodeLength;
    }

    @Override
    public String toString() {
        return "starterPos = " + starterPos + " ; maxCodeLength = " + maxCodeLength
               + " ; consumedBlocks.size() = " + consumedBlocks.size() + " ; variables = " + variables;
    }

    public UUID getListeningPlayer() {
        return listeningPlayer;
    }
}
