package eu.tyrano.blockncode.runtime;

public enum Result {
    ok,
    terminated,
    error,
    finished,
    exitFork,
    robotAction
}
