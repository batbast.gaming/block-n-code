package eu.tyrano.blockncode.runtime;

import eu.tyrano.blockncode.mod.common.blocks.code.BlockCodeBlank;
import net.minecraft.core.BlockPos;

public record CompiledBlock(BlockCodeBlank codeBlock, BlockPos pos, CodeRuntime optionalFork) {
}
