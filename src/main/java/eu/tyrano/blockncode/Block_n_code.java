package eu.tyrano.blockncode;

import com.mojang.logging.LogUtils;
import eu.tyrano.blockncode.libraries.VoskManager;
import eu.tyrano.blockncode.mod.client.ModEntityRenderers;
import eu.tyrano.blockncode.mod.client.ModKeyBinds;
import eu.tyrano.blockncode.mod.client.ModScreens;
import eu.tyrano.blockncode.mod.common.*;
import net.minecraft.client.renderer.Sheets;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.event.server.ServerStartingEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.loading.FMLPaths;
import org.slf4j.Logger;

@Mod(Block_n_code.MOD_ID)
public class Block_n_code {

    public static final String MOD_ID = "block_n_code";
    public static final Logger LOGGER = LogUtils.getLogger();

    public Block_n_code() {
        IEventBus eventBus = FMLJavaModLoadingContext.get().getModEventBus();

        // Register the setup method for mod loading
        eventBus.addListener(this::setup);
        // Register the enqueueIMC method for mod loading
        eventBus.addListener(this::enqueueIMC);
        // Register the processIMC method for mod loading
        eventBus.addListener(this::processIMC);
        // Register capabilities
        eventBus.addListener(this::registerCapabilities);
        // Register entity renderers
        eventBus.addListener(this::registerEntityRenderer);
        // Register entity attributes
        eventBus.addListener(this::registerEntityAttributes);
        // Register key binding event
        MinecraftForge.EVENT_BUS.register(new ModKeyBinds());

        String configFile = MOD_ID + "-config.toml";
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON,
                ModConfigs.GENERAL_SPEC, configFile);
        ModConfigs.init(FMLPaths.CONFIGDIR.get().resolve(configFile));
        ModConfigs.initGPT();

        ModItems.register(eventBus);
        ModBlocks.register(eventBus);
        ModMenus.register(eventBus);
        ModBlockEntities.register(eventBus);
        ModEntities.register(eventBus);

        eventBus.addListener(this::clientSetup);

        MinecraftForge.EVENT_BUS.register(this);

        // TODO: 26/03/2023 only do it on the client side
        if (isVoiceChatLoaded()) {
            VoskManager.init();
        }

        System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");
    }

    public static boolean isVoiceChatLoaded() {
        return ModList.get().isLoaded("voicechat");
    }

    private void registerEntityAttributes(final EntityAttributeCreationEvent event) {
        ModEntityAttributes.register(event);
    }

    private void registerCapabilities(final RegisterCapabilitiesEvent event) {
        ModCapabilities.register(event);
    }

    private void registerEntityRenderer(final EntityRenderersEvent.RegisterRenderers event) {
        ModEntityRenderers.register(event);
    }

    private void clientSetup(final FMLClientSetupEvent event) {
        ModScreens.register();
        ModKeyBinds.register();
    }

    private void setup(final FMLCommonSetupEvent event) {
        event.enqueueWork(() -> {
            ModPacketHandler.init();
            Sheets.addWoodType(ModWoodTypes.metal);
        });
    }

    private void enqueueIMC(final InterModEnqueueEvent event) {
        // Some example backend to dispatch IMC to another mod
    }

    private void processIMC(final InterModProcessEvent event) {
        // Some example backend to receive and process InterModComms from other mods
    }

    // You can use SubscribeEvent and let the Event Bus discover methods to call
    @SubscribeEvent
    public void onServerStarting(ServerStartingEvent event) {
        TickRuntimeHandler.instance = new TickRuntimeHandler();
        MinecraftForge.EVENT_BUS.register(TickRuntimeHandler.instance);
    }
}