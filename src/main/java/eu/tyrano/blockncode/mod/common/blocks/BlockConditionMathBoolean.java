package eu.tyrano.blockncode.mod.common.blocks;

import eu.tyrano.blockncode.runtime.CodeRuntime;
import eu.tyrano.blockncode.backend.variable.bool.Condition;
import eu.tyrano.blockncode.backend.variable.bool.ConditionMathType;
import eu.tyrano.blockncode.backend.variable.bool.MathCondition;
import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorBase;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorType;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.network.NetworkHooks;
import org.jetbrains.annotations.Nullable;

public class BlockConditionMathBoolean extends BlockConditionBinaryBoolean {

    public BlockConditionMathBoolean() {
        super();
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos,
                                 Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        if (!pLevel.isClientSide()) {
            BlockEntity entity = pLevel.getBlockEntity(pPos);
            if (entity instanceof BlockEntitySelectorBase<?,?> && ((BlockEntitySelectorBase<?, ?>) entity).getSelectorType() == BlockEntitySelectorType.mathBoolean) {
                NetworkHooks.openGui(((ServerPlayer) pPlayer), ((BlockEntitySelectorBase<?, ?>) entity), pPos);
            } else {
                throw new IllegalStateException("Our Container provider is missing!");
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    private BlockPos getNext(Level worldIn, BlockPos currentPos, boolean invert) {
        BlockPos nextCodePos = null;

        if (!invert) {
            switch (worldIn.getBlockState(currentPos).getValue(FACING)) {
                case EAST -> nextCodePos = new BlockPos(currentPos.east());
                case WEST -> nextCodePos = new BlockPos(currentPos.west());
                case NORTH -> nextCodePos = new BlockPos(currentPos.north());
                case SOUTH -> nextCodePos = new BlockPos(currentPos.south());
            }
        } else {
            switch (worldIn.getBlockState(currentPos).getValue(FACING)) {
                case EAST -> nextCodePos = new BlockPos(currentPos.west());
                case WEST -> nextCodePos = new BlockPos(currentPos.east());
                case NORTH -> nextCodePos = new BlockPos(currentPos.south());
                case SOUTH -> nextCodePos = new BlockPos(currentPos.north());
            }
        }

        return nextCodePos;
    }

    @Override
    public Condition compileCondition(Level worldIn, BlockPos currentBlockPos, CodeRuntime runtime) {

        BlockEntity blockEntity = worldIn.getBlockEntity(currentBlockPos);
        if (blockEntity instanceof BlockEntitySelectorBase<?, ?> blockEntitySelectorBase && blockEntitySelectorBase.getSelectorType() == BlockEntitySelectorType.mathBoolean) {

            BlockPos nextPosA = getNext(worldIn, currentBlockPos, false);
            BlockPos nextPosB = getNext(worldIn, currentBlockPos, true);

            if (nextPosA != null && nextPosB != null) {
                Block nextA = worldIn.getBlockState(nextPosA).getBlock();
                Block nextB = worldIn.getBlockState(nextPosB).getBlock();
                if (nextA instanceof BlockValueNumberInput nextNumberInputA && nextB instanceof BlockValueNumberInput nextNumberInputB) {
                    return new MathCondition(
                            nextNumberInputA.compileExpression(worldIn, nextPosA, runtime),
                            nextNumberInputB.compileExpression(worldIn, nextPosB, runtime),
                            ConditionMathType.valueOf(blockEntitySelectorBase.getEnumValue().toString()));
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntities.conditionMathBoolean.get().create(pos, state);
    }
}
