package eu.tyrano.blockncode.mod.common.menus;

import eu.tyrano.blockncode.mod.common.ModMenus;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueBooleanInput;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueStringInput;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;

public class MenuValueStringInput extends AbstractContainerMenu {

    private final BlockEntityValueStringInput blockEntity;
    private final BlockPos blockPos;

    public MenuValueStringInput(int pContainerId, Inventory inv, FriendlyByteBuf extraData) {
        this(pContainerId, inv, extraData.readBlockPos());
    }

    public MenuValueStringInput(int pContainerId, Inventory inv, BlockPos blockPos) {
        super(ModMenus.menuValueStringInput.get(), pContainerId);
        this.blockEntity = ((BlockEntityValueStringInput) inv.player.level.getBlockEntity(blockPos));
        this.blockPos = blockPos;
    }

    public String getBaseValue() {
        return blockEntity.getValue();
    }

    public BlockPos getBlockPos() {
        return blockPos;
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }
}
