package eu.tyrano.blockncode.mod.common;

import eu.tyrano.blockncode.Block_n_code;
import eu.tyrano.blockncode.mod.common.items.*;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.SignItem;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModItems {
    public static final DeferredRegister<Item> ITEMS =
            DeferredRegister.create(ForgeRegistries.ITEMS, Block_n_code.MOD_ID);

    public static final RegistryObject<Item> booleanVariable = ITEMS.register("variable_boolean",
            () -> new BooleanVariableItem(new Item.Properties().tab(ModCreativeTab.BlockNCodeTab)));

    public static final RegistryObject<Item> numberVariable = ITEMS.register("variable_number",
            () -> new NumberVariableItem(new Item.Properties().tab(ModCreativeTab.BlockNCodeTab)));

    public static final RegistryObject<Item> stringVariable = ITEMS.register("variable_string",
            () -> new StringVariableItem(new Item.Properties().tab(ModCreativeTab.BlockNCodeTab)));

    public static final RegistryObject<Item> arrayVariable = ITEMS.register("variable_array",
            () -> new ArrayVariableItem(new Item.Properties().tab(ModCreativeTab.BlockNCodeTab)));

    public static final RegistryObject<Item> robotBottle = ITEMS.register("robot_bottle",
            () -> new RobotBottleItem(new Item.Properties().tab(ModCreativeTab.BlockNCodeTab).stacksTo(1).rarity(Rarity.EPIC)));

    public static final RegistryObject<Item> metalSign = ITEMS.register("metal_sign",
            () -> new SignItem( new Item.Properties().tab(ModCreativeTab.BlockNCodeTab).stacksTo(16),
                    ModBlocks.standingIronSign.get(), ModBlocks.wallIronSign.get()));

    public static final RegistryObject<Item> codeCore = ITEMS.register("code_core",
            () -> new Item(new Item.Properties().tab(ModCreativeTab.BlockNCodeTab).rarity(Rarity.UNCOMMON)));
    
    public static void register(IEventBus eventBus) {
        ITEMS.register(eventBus);
    }
}
