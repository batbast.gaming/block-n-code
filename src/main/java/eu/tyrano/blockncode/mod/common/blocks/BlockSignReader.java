package eu.tyrano.blockncode.mod.common.blocks;

import eu.tyrano.blockncode.backend.variable.string.StringModification;
import eu.tyrano.blockncode.backend.variable.string.StringReader;
import eu.tyrano.blockncode.backend.variable.string.StringVariable;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityMonitor;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.SignBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

import javax.annotation.Nullable;
import java.util.UUID;

public class BlockSignReader extends BlockValueStringInput {

    public BlockSignReader() {
        super();
    }

    @Override
    public StringModification compileModification(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {

        BlockPos signPos = currentPos.below();
        BlockEntity sign = worldIn.getBlockEntity(signPos);
        if (sign instanceof SignBlockEntity metalSign) {
            return readSign(metalSign);
        } else if (sign instanceof BlockEntityMonitor monitor) {
            return readMonitor(monitor);
        }

        signPos = signPos.below();
        sign = worldIn.getBlockEntity(signPos);
        if (sign instanceof SignBlockEntity metalSign) {
            return readSign(metalSign);
        } else if (sign instanceof BlockEntityMonitor monitor) {
            return readMonitor(monitor);
        }

        return new StringReader(new StringVariable( ""));
    }

    private StringReader readMonitor(BlockEntityMonitor monitor) {
        return new StringReader(new StringVariable( monitor.getOutput()));
    }

    private StringReader readSign(SignBlockEntity metalSign) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < 4; i++) {
            builder.append(metalSign.getMessage(i, false).getString());
        }

        return new StringReader(new StringVariable( builder.toString()));
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return null;
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos,
                                 Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        return InteractionResult.PASS;
    }
}