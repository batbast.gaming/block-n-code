package eu.tyrano.blockncode.mod.common.blocks;

import eu.tyrano.blockncode.backend.variable.Variable;
import eu.tyrano.blockncode.backend.variable.string.StringModification;
import eu.tyrano.blockncode.backend.variable.string.StringReader;
import eu.tyrano.blockncode.backend.variable.string.StringVariable;
import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityStringVariableReader;
import eu.tyrano.blockncode.mod.common.items.StringVariableItem;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.network.NetworkHooks;
import org.jetbrains.annotations.Nullable;

public class BlockVariableReaderString extends BlockValueStringInput {
    public BlockVariableReaderString() {
        super();
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntities.variableStringReader.get().create(pos, state);
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos,
                                 Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if (!pLevel.isClientSide()) {
            BlockEntity entity = pLevel.getBlockEntity(pPos);
            if (entity instanceof BlockEntityStringVariableReader) {
                NetworkHooks.openGui(((ServerPlayer) pPlayer), (BlockEntityStringVariableReader) entity, pPos);
            } else {
                throw new IllegalStateException("Our Container provider is missing!");
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    @Override
    public StringModification compileModification(Level worldIn, BlockPos currentBlockPos, CodeRuntime runtime) {

        BlockEntity blockEntity = worldIn.getBlockEntity(currentBlockPos);
        if (blockEntity instanceof BlockEntityStringVariableReader entity) {
            ItemStack item = entity.getItem();
            if (item.getItem() instanceof StringVariableItem) {
                StringVariable stringVariable = null;
                if (item.getTag() != null) {
                    Variable variable = runtime.getVariable(item.getTag().getString("variable_name"));
                    if (variable instanceof StringVariable) {
                        stringVariable = (StringVariable) variable;
                    }
                }
                if (stringVariable != null) {
                    return new StringReader(new StringVariable(stringVariable));
                } else {
                    return new StringReader(new StringVariable(""));
                }
            } else {
                return new StringReader(new StringVariable(""));
            }
        } else {
            return null;
        }
    }
}
