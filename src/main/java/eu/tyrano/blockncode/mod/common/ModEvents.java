package eu.tyrano.blockncode.mod.common;

import eu.tyrano.blockncode.Block_n_code;
import eu.tyrano.blockncode.libraries.VoiceRecognition;
import eu.tyrano.blockncode.mod.common.capabilities.WorldCodeCapabilityProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = Block_n_code.MOD_ID)
public class ModEvents {

    @SubscribeEvent
    public static void onAttachCapabilitiesLevel(AttachCapabilitiesEvent<Level> event) {
        if (!event.getObject().getCapability(WorldCodeCapabilityProvider.codeCapability).isPresent()) {
            event.addCapability(new ResourceLocation(Block_n_code.MOD_ID, "properties"), new WorldCodeCapabilityProvider());
        }
    }

    @SubscribeEvent
    public static void onWorldLoaded(WorldEvent.Load event) {
        if (event.getWorld().isClientSide()) {
            VoiceRecognition.reset();
        }
    }
}
