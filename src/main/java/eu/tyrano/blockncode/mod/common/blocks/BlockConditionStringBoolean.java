package eu.tyrano.blockncode.mod.common.blocks;

import eu.tyrano.blockncode.backend.variable.bool.Condition;
import eu.tyrano.blockncode.backend.variable.bool.ConditionStringType;
import eu.tyrano.blockncode.backend.variable.bool.StringCondition;
import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorBase;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorType;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.network.NetworkHooks;
import org.jetbrains.annotations.Nullable;

public class BlockConditionStringBoolean extends BlockConditionBinaryBoolean {

    public BlockConditionStringBoolean() {
        super();
    }

    @Override
    public InteractionResult use(BlockState state, Level level, BlockPos pos,
                                 Player player, InteractionHand hand, BlockHitResult hit) {
        if (!level.isClientSide()) {
            BlockEntity entity = level.getBlockEntity(pos);
            if (entity instanceof BlockEntitySelectorBase<?,?> && ((BlockEntitySelectorBase<?, ?>) entity).getSelectorType() == BlockEntitySelectorType.stringBoolean) {
                NetworkHooks.openGui(((ServerPlayer) player), ((BlockEntitySelectorBase<?, ?>) entity), pos);
            } else {
                throw new IllegalStateException("Our Container provider is missing!");
            }
        }

        return InteractionResult.sidedSuccess(level.isClientSide());
    }

    private BlockPos getNext(Level worldIn, BlockPos currentPos, boolean invert) {
        BlockPos nextCodePos = null;

        if (!invert) {
            switch (worldIn.getBlockState(currentPos).getValue(FACING)) {
                case EAST -> nextCodePos = new BlockPos(currentPos.east());
                case WEST -> nextCodePos = new BlockPos(currentPos.west());
                case NORTH -> nextCodePos = new BlockPos(currentPos.north());
                case SOUTH -> nextCodePos = new BlockPos(currentPos.south());
            }
        } else {
            switch (worldIn.getBlockState(currentPos).getValue(FACING)) {
                case EAST -> nextCodePos = new BlockPos(currentPos.west());
                case WEST -> nextCodePos = new BlockPos(currentPos.east());
                case NORTH -> nextCodePos = new BlockPos(currentPos.south());
                case SOUTH -> nextCodePos = new BlockPos(currentPos.north());
            }
        }

        return nextCodePos;
    }

    @Override
    public Condition compileCondition(Level worldIn, BlockPos currentBlockPos, CodeRuntime runtime) {

        BlockEntity blockEntity = worldIn.getBlockEntity(currentBlockPos);
        if (blockEntity instanceof BlockEntitySelectorBase<?, ?> blockEntitySelectorBase && blockEntitySelectorBase.getSelectorType() == BlockEntitySelectorType.stringBoolean) {

            BlockPos nextPosA = getNext(worldIn, currentBlockPos, false);
            BlockPos nextPosB = getNext(worldIn, currentBlockPos, true);

            if (nextPosA != null && nextPosB != null) {
                Block nextA = worldIn.getBlockState(nextPosA).getBlock();
                Block nextB = worldIn.getBlockState(nextPosB).getBlock();
                if (nextA instanceof BlockValueStringInput nextStringInputA && nextB instanceof BlockValueStringInput nextStringInputB) {
                    return new StringCondition(
                            nextStringInputA.compileModification(worldIn, nextPosA, runtime),
                            nextStringInputB.compileModification(worldIn, nextPosB, runtime),
                            ConditionStringType.valueOf(blockEntitySelectorBase.getEnumValue().toString()));
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntities.conditionStringBoolean.get().create(pos, state);
    }
}
