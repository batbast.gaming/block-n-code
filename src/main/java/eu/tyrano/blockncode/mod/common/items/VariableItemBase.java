package eu.tyrano.blockncode.mod.common.items;

import eu.tyrano.blockncode.mod.common.menus.MenuProviderVariableRename;
import net.minecraft.ChatFormatting;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextColor;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.text.Style;
import java.util.ArrayList;
import java.util.List;

public abstract class VariableItemBase extends Item {
    public VariableItemBase(Properties p_41383_) {
        super(p_41383_);
    }

    @Override
    public @NotNull InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand) {
        if (!level.isClientSide() && hand == InteractionHand.MAIN_HAND) {
            openMenu(player, player.getItemBySlot(EquipmentSlot.MAINHAND));
        }
        return super.use(level, player, hand);
    }

    protected void openMenu(Player player, ItemStack stack) {
        MenuProviderVariableRename.openGUI((ServerPlayer) player, stack);
    }

    @Override
    public void appendHoverText(ItemStack p_41421_, @Nullable Level p_41422_, List<Component> p_41423_, TooltipFlag p_41424_) {
        CompoundTag tag = p_41421_.getTag();
        if (tag != null) {
            String variableName = tag.getString("variable_name");
            p_41423_.add(new TextComponent("---> " + variableName));
        }

        super.appendHoverText(p_41421_, p_41422_, p_41423_, p_41424_);
    }
}