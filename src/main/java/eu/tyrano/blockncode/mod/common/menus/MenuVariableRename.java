package eu.tyrano.blockncode.mod.common.menus;

import eu.tyrano.blockncode.mod.common.ModMenus;
import eu.tyrano.blockncode.mod.common.items.VariableItemBase;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.item.ItemStack;

public class MenuVariableRename extends AbstractContainerMenu {

    private final String baseItemName;

    public MenuVariableRename(int pContainerId, Inventory inv, FriendlyByteBuf ignored) {
        this(pContainerId, inv, createInventory(inv));
    }

    public MenuVariableRename(int pContainerId, Inventory inv, MenuProviderVariableRename ignored) {
        super(ModMenus.menuVariableRename.get(), pContainerId);
        CompoundTag tag = inv.player.getItemBySlot(EquipmentSlot.MAINHAND).getTag();
        if (tag != null) {
            baseItemName = tag.getString("variable_name");
        } else {
            baseItemName = "";
        }
    }

    private static MenuProviderVariableRename createInventory(final Inventory inventory) {
        final ItemStack stack;

        stack = inventory.player.getItemBySlot(EquipmentSlot.MAINHAND);

        if (stack.getItem() instanceof VariableItemBase) {
            return new MenuProviderVariableRename(stack);
        } else {
            return null;
        }
    }

    public String getStackName() {
        return baseItemName;
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }
}
