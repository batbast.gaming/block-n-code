package eu.tyrano.blockncode.mod.common.blocks.code;

import eu.tyrano.blockncode.runtime.CodeRuntime;
import eu.tyrano.blockncode.runtime.CompiledBlock;
import eu.tyrano.blockncode.runtime.ExecutionResult;
import eu.tyrano.blockncode.runtime.Result;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;

public class BlockCodeBreak extends BlockCodeBlank {
    public CompiledBlock getNext(Level worldIn, BlockPos currentPos) {
        return null;
    }

    public ExecutionResult execute(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {
        return new ExecutionResult(Result.exitFork, null, 0);
    }
}
