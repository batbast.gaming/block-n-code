package eu.tyrano.blockncode.mod.common;

import eu.tyrano.blockncode.Block_n_code;
import eu.tyrano.blockncode.backend.variable.bool.ConditionBooleanType;
import eu.tyrano.blockncode.backend.variable.bool.ConditionMathType;
import eu.tyrano.blockncode.backend.variable.bool.ConditionStringType;
import eu.tyrano.blockncode.backend.variable.number.OperationType;
import eu.tyrano.blockncode.backend.variable.string.ModificationType;
import eu.tyrano.blockncode.mod.client.screens.buttontypes.*;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorBase;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorType;
import eu.tyrano.blockncode.mod.common.entities.robot.RobotAction;
import eu.tyrano.blockncode.mod.common.menus.*;
import net.minecraft.core.Direction;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraftforge.common.extensions.IForgeMenuType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.network.IContainerFactory;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModMenus {
    public static final DeferredRegister<MenuType<?>> MENUS =
            DeferredRegister.create(ForgeRegistries.CONTAINERS, Block_n_code.MOD_ID);


    public static final RegistryObject<MenuType<MenuVariableWriter>> menuVariableWriter =
            registerMenuType(MenuVariableWriter::new, "variable_writer_menu");


    public static final RegistryObject<MenuType<MenuVariableReader>> menuVariableBooleanReader =
            registerMenuType((container, inventory, buf) ->
                            new MenuVariableReader(container, inventory, buf, ModMenus.menuVariableBooleanReader, ModBlocks.variableBooleanReaderBlock),
                    "variable_boolean_reader_menu");


    public static final RegistryObject<MenuType<MenuVariableReader>> menuVariableNumberReader =
            registerMenuType((container, inventory, buf) ->
                            new MenuVariableReader(container, inventory, buf, ModMenus.menuVariableNumberReader, ModBlocks.variableNumberReaderBlock),
                    "variable_number_reader_menu");


    public static final RegistryObject<MenuType<MenuVariableReader>> menuVariableStringReader =
            registerMenuType((container, inventory, buf) ->
                            new MenuVariableReader(container, inventory, buf, ModMenus.menuVariableStringReader, ModBlocks.variableStringReaderBlock),
                    "variable_string_reader_menu");


    public static final RegistryObject<MenuType<MenuVariableRename>> menuVariableRename =
            registerMenuType(MenuVariableRename::new, "variable_rename_menu");


    public static final RegistryObject<MenuType<MenuCodeRobot>> menuCodeRobot =
            registerMenuType(MenuCodeRobot::new, "robot_menu");


    public static final RegistryObject<MenuType<MenuContainerRobot>> menuContainerRobot =
            registerMenuType(MenuContainerRobot::new, "robot_container_menu");


    public static final RegistryObject<MenuType<MenuSelectorBase<ConditionUnaryBooleanType, BlockEntitySelectorBase<ConditionBooleanType, ConditionUnaryBooleanType>>>> menuConditionUnaryBoolean =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuConditionUnaryBoolean.get(), BlockEntitySelectorType.unaryBoolean), "condition_unary_boolean_menu");


    public static final RegistryObject<MenuType<MenuSelectorBase<RobotActionType, BlockEntitySelectorBase<RobotAction, RobotActionType>>>> menuRobotInstruction =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuRobotInstruction.get(), BlockEntitySelectorType.robotInstruction), "robot_instruction_menu");


    public static final RegistryObject<MenuType<MenuSelectorBase<DirectionType, BlockEntitySelectorBase<Direction, DirectionType>>>> menuDirection =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuDirection.get(), BlockEntitySelectorType.direction), "direction_menu");


    public static final RegistryObject<MenuType<MenuSelectorBase<ConditionBinaryBooleanType, BlockEntitySelectorBase<ConditionBooleanType, ConditionBinaryBooleanType>>>> menuConditionBinaryBoolean =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuConditionBinaryBoolean.get(), BlockEntitySelectorType.binaryBoolean), "condition_binary_boolean_menu");


    public static final RegistryObject<MenuType<MenuSelectorBase<ConditionMathBooleanType, BlockEntitySelectorBase<ConditionMathType, ConditionMathBooleanType>>>> menuConditionMathBoolean =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuConditionMathBoolean.get(), BlockEntitySelectorType.mathBoolean), "condition_math_boolean_menu");


    public static final RegistryObject<MenuType<MenuSelectorBase<ConditionStringBooleanType, BlockEntitySelectorBase<ConditionStringType, ConditionStringBooleanType>>>> menuConditionStringBoolean =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuConditionStringBoolean.get(), BlockEntitySelectorType.stringBoolean), "condition_string_boolean_menu");


    public static final RegistryObject<MenuType<MenuSelectorBase<ExpressionUnaryMathType, BlockEntitySelectorBase<OperationType, ExpressionUnaryMathType>>>> menuOperationUnaryMath =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuOperationUnaryMath.get(), BlockEntitySelectorType.unaryMath), "operation_unary_math");


    public static final RegistryObject<MenuType<MenuSelectorBase<ExpressionBinaryMathType, BlockEntitySelectorBase<OperationType, ExpressionBinaryMathType>>>> menuOperationBinaryMath =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuOperationBinaryMath.get(), BlockEntitySelectorType.binaryMath), "operation_binary_math");


    public static final RegistryObject<MenuType<MenuSelectorBase<ModificationStringType, BlockEntitySelectorBase<ModificationType, ModificationStringType>>>> menuModificationBinaryString =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuModificationBinaryString.get(), BlockEntitySelectorType.binaryString), "operation_binary_string");


    public static final RegistryObject<MenuType<MenuSelectorBase<ModificationParseBooleanType, BlockEntitySelectorBase<ModificationType, ModificationParseBooleanType>>>> menuModificationBooleanString =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuModificationBooleanString.get(), BlockEntitySelectorType.booleanToString), "operation_boolean_string");


    public static final RegistryObject<MenuType<MenuSelectorBase<ModificationParseNumberType, BlockEntitySelectorBase<ModificationType, ModificationParseNumberType>>>> menuModificationNumberString =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuModificationNumberString.get(), BlockEntitySelectorType.numberToString), "operation_number_string");


    public static final RegistryObject<MenuType<MenuSelectorBase<ModificationChatReadType, BlockEntitySelectorBase<ModificationType, ModificationChatReadType>>>> menuModificationChatRead =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuModificationChatRead.get(), BlockEntitySelectorType.chatToString), "operation_chat_read");


    public static final RegistryObject<MenuType<MenuSelectorBase<ModificationPlayerListenType, BlockEntitySelectorBase<ModificationType, ModificationPlayerListenType>>>> menuModificationPlayerListen =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuModificationPlayerListen.get(), BlockEntitySelectorType.voiceToString), "modification_player_listen");


    public static final RegistryObject<MenuType<MenuSelectorBase<ConditionParseStringType, BlockEntitySelectorBase<ConditionBooleanType, ConditionParseStringType>>>> menuConditionStringToBoolean =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuConditionStringToBoolean.get(), BlockEntitySelectorType.stringToBoolean), "condition_string_to_boolean");


    public static final RegistryObject<MenuType<MenuSelectorBase<ExpressionParseStringType, BlockEntitySelectorBase<OperationType, ExpressionParseStringType>>>> menuOperationStringToNumber =
            registerMenuType((container, inventory, buf) -> new MenuSelectorBase<>(container, inventory, buf,
                    ModMenus.menuOperationStringToNumber.get(), BlockEntitySelectorType.stringToNumber), "expression_string_to_number");


    public static final RegistryObject<MenuType<MenuValueBooleanInput>> menuValueBooleanInput =
            registerMenuType(MenuValueBooleanInput::new, "value_boolean_input");


    public static final RegistryObject<MenuType<MenuValueNumberInput>> menuValueNumberInput =
            registerMenuType(MenuValueNumberInput::new, "value_number_input");


    public static final RegistryObject<MenuType<MenuValueComplexInput>> menuValueComplexInput =
            registerMenuType(MenuValueComplexInput::new, "value_complex_input");


    public static final RegistryObject<MenuType<MenuValueStringInput>> menuValueStringInput =
            registerMenuType(MenuValueStringInput::new, "value_string_input");


    public static final RegistryObject<MenuType<MenuFunctionCoordinates>> menuFunctionCoordinates =
            registerMenuType(MenuFunctionCoordinates::new, "function_coordinates");


    private static <T extends AbstractContainerMenu> RegistryObject<MenuType<T>>
    registerMenuType(IContainerFactory<T> factory, String name) {
        return MENUS.register(name, () -> IForgeMenuType.create(factory));
    }

    public static void register(IEventBus eventBus) {
        MENUS.register(eventBus);
    }
}
