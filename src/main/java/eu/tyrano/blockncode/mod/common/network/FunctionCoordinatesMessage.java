package eu.tyrano.blockncode.mod.common.network;

import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityFunctionCall;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueComplexInput;
import io.netty.buffer.ByteBuf;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class FunctionCoordinatesMessage {

    private int x;
    private int y;
    private int z;
    private BlockPos blockPos;

    private final boolean failed;

    public FunctionCoordinatesMessage(int x, int y, int z, BlockPos blockPos) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.blockPos = blockPos;
        this.failed = false;
    }

    private FunctionCoordinatesMessage(boolean failed) {
        this.failed = failed;
    }

    public static FunctionCoordinatesMessage decode(ByteBuf buf) {
        try {
            int valX = buf.readInt();
            int valY = buf.readInt();
            int valZ = buf.readInt();
            int x = buf.readInt();
            int y = buf.readInt();
            int z = buf.readInt();
            return new FunctionCoordinatesMessage(valX, valY, valZ, new BlockPos(x, y, z));
        } catch (IndexOutOfBoundsException e) {
            return new FunctionCoordinatesMessage(true);
        }
    }

    public static void messageConsumer(FunctionCoordinatesMessage msg, Supplier<NetworkEvent.Context> ctx) {
        handle(msg, ctx);
        ctx.get().setPacketHandled(true);
    }

    public static void encode(FunctionCoordinatesMessage msg, FriendlyByteBuf buf) {
        buf.writeInt(msg.x);
        buf.writeInt(msg.y);
        buf.writeInt(msg.z);
        buf.writeInt(msg.blockPos.getX());
        buf.writeInt(msg.blockPos.getY());
        buf.writeInt(msg.blockPos.getZ());
    }

    public static void handle(FunctionCoordinatesMessage msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            if (!msg.failed) {
                if (ctx.get().getSender() != null && ctx.get().getSender().position().distanceTo(Vec3.atCenterOf(msg.blockPos)) <= 10) {
                    BlockEntity blockEntity = ctx.get().getSender().getLevel().getBlockEntity(msg.blockPos);
                    if (blockEntity instanceof BlockEntityFunctionCall functionCall) {
                        functionCall.setValue(msg.x, msg.y, msg.z);
                    }
                }
            }
        });
    }
}
