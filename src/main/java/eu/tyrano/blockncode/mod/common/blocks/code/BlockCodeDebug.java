package eu.tyrano.blockncode.mod.common.blocks.code;

import eu.tyrano.blockncode.runtime.CodeRuntime;
import eu.tyrano.blockncode.runtime.ExecutionResult;
import eu.tyrano.blockncode.runtime.Result;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;

public class BlockCodeDebug extends BlockCodeBlank {
    public ExecutionResult execute(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {
        System.out.println(runtime.toString());
        return new ExecutionResult(Result.ok, null, 0);
    }
}
