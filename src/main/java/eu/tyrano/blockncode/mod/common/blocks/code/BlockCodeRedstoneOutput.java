package eu.tyrano.blockncode.mod.common.blocks.code;

import eu.tyrano.blockncode.backend.variable.number.ExpressionResult;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import eu.tyrano.blockncode.backend.variable.number.Expression;
import eu.tyrano.blockncode.backend.variable.number.NumberVariable;
import eu.tyrano.blockncode.mod.common.blocks.BlockValueNumberInput;
import eu.tyrano.blockncode.runtime.ExecutionResult;
import eu.tyrano.blockncode.runtime.Result;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RedStoneOreBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.redstone.Redstone;

import java.util.Random;

public class BlockCodeRedstoneOutput extends BlockCodeBlank {

    public static final IntegerProperty signalPower = BlockStateProperties.POWER;

    public BlockCodeRedstoneOutput() {
        super();
        this.registerDefaultState(this.getStateDefinition().any().setValue(FACING, Direction.NORTH).setValue(signalPower, 0));
    }

    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(FACING, signalPower);
    }

    public ExecutionResult execute(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {

        BlockPos valueProviderPos = currentPos.above();
        Block valueInput = worldIn.getBlockState(valueProviderPos).getBlock();

        if (valueInput instanceof BlockValueNumberInput valueNumberInput) {
            Expression expression = valueNumberInput.compileExpression(worldIn, valueProviderPos, runtime);

            if (expression == null) {
                return new ExecutionResult(Result.error, "Expression generated at "
                        + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ() + " is null", 0);
            }

            ExpressionResult expressionResult = expression.execute();

            if (expressionResult.error() == null) {

                NumberVariable result = expressionResult.variable();

                int power = (int) result.getRealValue();
                if (power > 15) {
                    power = 15;
                } else if (power < 0) {
                    power = 0;
                }

                BlockState blockState = worldIn.getBlockState(currentPos);

                worldIn.setBlockAndUpdate(currentPos, blockState.setValue(signalPower, power));
                worldIn.scheduleTick(currentPos, this, 4);
                updateNeighbours(worldIn, currentPos);

                return new ExecutionResult(Result.ok, null, 0);
            } else {
                return new ExecutionResult(Result.error, expressionResult.error() + " at "
                        + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
            }
        } else {
            return new ExecutionResult(Result.error, "No valid value provider above "
                    + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
        }
    }

    @Override
    public void tick(BlockState blockState, ServerLevel world, BlockPos blockPos, Random random) {
        world.setBlockAndUpdate(blockPos, blockState.setValue(signalPower, 0));
        updateNeighbours(world, blockPos);
    }

    @Override
    public boolean isSignalSource(BlockState state) {
        return true;
    }

    public int getSignal(BlockState state, BlockGetter getter, BlockPos pos, Direction direction) {
        return state.getValue(signalPower);
    }

    private void updateNeighbours(Level world, BlockPos blockPos) {
        world.updateNeighborsAt(blockPos, this);
    }
}
