package eu.tyrano.blockncode.mod.common.blocks.code;

import eu.tyrano.blockncode.backend.variable.number.Expression;
import eu.tyrano.blockncode.backend.variable.number.ExpressionResult;
import eu.tyrano.blockncode.backend.variable.number.NumberVariable;
import eu.tyrano.blockncode.mod.common.blocks.BlockValueNumberInput;
import eu.tyrano.blockncode.runtime.*;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;

public class BlockCodeLoop extends BlockCodeBlank {

    public ExecutionResult execute(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {

        BlockPos valueProviderPos = currentPos.above();
        Block valueInput = worldIn.getBlockState(valueProviderPos).getBlock();

        if (valueInput instanceof BlockValueNumberInput valueNumberInput) {
            Expression expression = valueNumberInput.compileExpression(worldIn, valueProviderPos, runtime);

            if (expression == null) {
                return new ExecutionResult(Result.error, "Expression generated at "
                        + currentPos.getX() + ","
                        + currentPos.getY() + ","
                        + currentPos.getZ() + " is null", 0);
            }

            ExpressionResult expressionResult = expression.execute();

            if (expressionResult.error() == null) {
                NumberVariable result = expressionResult.variable();

                // TODO: 29/03/2023 Make while loop if input is a boolean
                return new ExecutionResult(Result.ok, null, (int) result.getRealValue());
            } else {
                return new ExecutionResult(Result.error, expressionResult.error() + " at "
                        + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
            }
        } else {
            return new ExecutionResult(Result.error, "No valid value provider above "
                    + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
        }
    }

    @Override
    public CodeRuntime getOptionalFork(Level world, BlockPos currentPos, CodeRuntime runtime) {
        BlockPos altCodePos;
        switch (world.getBlockState(currentPos).getValue(FACING)) {
            case EAST -> altCodePos = new BlockPos(currentPos.north());
            case WEST -> altCodePos = new BlockPos(currentPos.south());
            case NORTH -> altCodePos = new BlockPos(currentPos.west());
            case SOUTH -> altCodePos = new BlockPos(currentPos.east());
            default -> {
                return null;
            }
        }

        CodeRuntimeExtends compiler = new CodeRuntimeExtends(world, altCodePos, runtime);
        compiler.compile();
        return compiler;
    }
}
