package eu.tyrano.blockncode.mod.common.blocks.code;

import eu.tyrano.blockncode.backend.variable.number.Expression;
import eu.tyrano.blockncode.backend.variable.number.ExpressionResult;
import eu.tyrano.blockncode.backend.variable.number.NumberVariable;
import eu.tyrano.blockncode.mod.common.blocks.BlockDirectionValue;
import eu.tyrano.blockncode.mod.common.blocks.BlockRobotInstruction;
import eu.tyrano.blockncode.mod.common.blocks.BlockValueNumberInput;
import eu.tyrano.blockncode.mod.common.entities.robot.RobotAction;
import eu.tyrano.blockncode.mod.common.entities.robot.RobotControl;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import eu.tyrano.blockncode.runtime.CodeRuntimeExtends;
import eu.tyrano.blockncode.runtime.ExecutionResult;
import eu.tyrano.blockncode.runtime.Result;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;

public class BlockCodeRobotInstruction extends BlockCodeBlank {

    public BlockCodeRobotInstruction() {
        super();
    }

    public ExecutionResult execute(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {

        BlockPos valueProviderPos = currentPos.above();
        Block robotInstructionValue = worldIn.getBlockState(valueProviderPos).getBlock();

        if (robotInstructionValue instanceof BlockRobotInstruction robotInstruction) {
            RobotAction instruction = robotInstruction.getInstruction(worldIn, valueProviderPos);

            Block nextValue = worldIn.getBlockState(valueProviderPos.above()).getBlock();
            if (instruction == RobotAction.disassemble) {
                return new ExecutionResult(Result.robotAction, null, 0, new RobotControl(instruction, null, 0));
            } else if (instruction == RobotAction.waiting) {
                if (nextValue instanceof BlockValueNumberInput numberInput) {
                    return getNumericalValue(numberInput, worldIn, valueProviderPos.above(), runtime, currentPos, instruction, null);
                } else {
                    return new ExecutionResult(Result.error, "No valid value provider above "
                            + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                }
            } else {
                if (nextValue instanceof BlockDirectionValue robotDirection) {
                    Direction direction = robotDirection.getDirection(worldIn, valueProviderPos.above());

                    Block dataValue = worldIn.getBlockState(valueProviderPos.above().above()).getBlock();
                    switch (instruction) {
                        case moving:
                            if (dataValue instanceof BlockValueNumberInput numberInput) {
                                return getNumericalValue(numberInput, worldIn, valueProviderPos.above().above(), runtime, currentPos, instruction, direction);
                            } else {
                                return new ExecutionResult(Result.error, "No valid value provider above "
                                        + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                            }
                        case placing:
                            if (dataValue instanceof BlockValueNumberInput numberInput) {
                                return getNumericalValue(numberInput, worldIn, valueProviderPos.above().above(), runtime, currentPos, instruction, direction);
                            } else {
                                return new ExecutionResult(Result.robotAction, null, 0,
                                        new RobotControl(instruction, direction, -1));
                            }
                        default:
                            return new ExecutionResult(Result.robotAction, null, 0,
                                    new RobotControl(instruction, direction, 0));
                    }
                } else {
                    return new ExecutionResult(Result.error, "No valid value provider above "
                            + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                }
            }
        } else {
            return new ExecutionResult(Result.error, "No valid value provider above "
                    + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
        }
    }

    @Override
    public CodeRuntime getOptionalFork(Level world, BlockPos currentPos, CodeRuntime runtime) {
        BlockPos altCodePos;
        switch (world.getBlockState(currentPos).getValue(FACING)) {
            case EAST -> altCodePos = new BlockPos(currentPos.north());
            case WEST -> altCodePos = new BlockPos(currentPos.south());
            case NORTH -> altCodePos = new BlockPos(currentPos.west());
            case SOUTH -> altCodePos = new BlockPos(currentPos.east());
            default -> {
                return null;
            }
        }

        CodeRuntimeExtends compiler = new CodeRuntimeExtends(world, altCodePos, runtime);
        compiler.compile();
        return compiler;
    }

    private ExecutionResult getNumericalValue(BlockValueNumberInput numberInput, Level worldIn, BlockPos valueProviderPos,
                                              CodeRuntime runtime, BlockPos currentPos, RobotAction instruction, Direction direction) {
        Expression expression = numberInput.compileExpression(worldIn, valueProviderPos, runtime);

        if (expression == null) {
            return new ExecutionResult(Result.error, "Expression generated at "
                    + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ() + " is not valid", 0);
        }

        ExpressionResult result = expression.execute();

        if (result.error() == null) {
            NumberVariable variable = result.variable();
            return new ExecutionResult(Result.robotAction, null, 0,
                    new RobotControl(instruction, direction, (int) variable.getRealValue()));
        } else {
            return new ExecutionResult(Result.error, result.error() + " at "
                    + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
        }
    }
}
