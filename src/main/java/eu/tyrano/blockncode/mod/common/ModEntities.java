package eu.tyrano.blockncode.mod.common;

import eu.tyrano.blockncode.Block_n_code;
import eu.tyrano.blockncode.mod.common.entities.robot.RobotEntity;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModEntities {

    public static final DeferredRegister<EntityType<?>> ENTITIES =
            DeferredRegister.create(ForgeRegistries.ENTITIES, Block_n_code.MOD_ID);

    public static final RegistryObject<EntityType<RobotEntity>> robot =
            ENTITIES.register("robot",
                    () -> EntityType.Builder.of(RobotEntity::new, MobCategory.MISC)
                            .sized(1f, 1f)
                            .build(new ResourceLocation(Block_n_code.MOD_ID, "robot")
                                    .toString()));

    public static void register(IEventBus eventBus) {
        ENTITIES.register(eventBus);
    }
}