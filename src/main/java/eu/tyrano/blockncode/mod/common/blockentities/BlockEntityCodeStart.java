package eu.tyrano.blockncode.mod.common.blockentities;

import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.TickRuntimeHandler;
import eu.tyrano.blockncode.mod.common.entities.robot.RuntimeDataProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import java.util.UUID;

public class BlockEntityCodeStart extends BlockEntity {

    private UUID runtimeUUID = null;

    public BlockEntityCodeStart(BlockPos pos, BlockState state) {
        super(ModBlockEntities.codeStart.get(), pos, state);
    }

    public void startRuntime(Level world, BlockPos pos, RuntimeDataProvider initData) {
        if (this.runtimeUUID == null) {
            this.runtimeUUID = TickRuntimeHandler.instance
                    .registerRuntime(world, pos, this::closeRuntime, initData);
        }
    }

    public void stopRuntime() {
        TickRuntimeHandler.instance.removeRuntime(runtimeUUID);
        closeRuntime();
    }

    private void closeRuntime() {
        this.runtimeUUID = null;
    }

    @Override
    public void setRemoved() {
        if (runtimeUUID != null) {
            TickRuntimeHandler.instance.removeRuntime(runtimeUUID);
        }
        super.setRemoved();
    }
}
