package eu.tyrano.blockncode.mod.common.blocks;

import eu.tyrano.blockncode.backend.variable.array.ArrayExpression;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public abstract class BlockValueArrayInput extends BlockValueProvider {
    public abstract ArrayExpression<?> compileExpression(Level worldIn, BlockPos currentPos, CodeRuntime runtime);

    @Override
    public abstract BlockEntity newBlockEntity(BlockPos pos, BlockState state);
}
