package eu.tyrano.blockncode.mod.common.network;

import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueComplexInput;
import io.netty.buffer.ByteBuf;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ValueComplexMessage {

    private double real;
    private double imaginary;
    private BlockPos blockPos;

    private final boolean failed;

    public ValueComplexMessage(double real, double imaginary, BlockPos blockPos) {
        this.real = real;
        this.imaginary = imaginary;
        this.blockPos = blockPos;
        this.failed = false;
    }

    private ValueComplexMessage(boolean failed) {
        this.failed = failed;
    }

    public static ValueComplexMessage decode(ByteBuf buf) {
        try {
            double real = buf.readDouble();
            double imaginary = buf.readDouble();
            int x = buf.readInt();
            int y = buf.readInt();
            int z = buf.readInt();
            return new ValueComplexMessage(real, imaginary, new BlockPos(x, y, z));
        } catch (IndexOutOfBoundsException e) {
            return new ValueComplexMessage(true);
        }
    }

    public static void messageConsumer(ValueComplexMessage msg, Supplier<NetworkEvent.Context> ctx) {
        handle(msg, ctx);
        ctx.get().setPacketHandled(true);
    }

    public static void encode(ValueComplexMessage msg, FriendlyByteBuf buf) {
        buf.writeDouble(msg.real);
        buf.writeDouble(msg.imaginary);
        buf.writeInt(msg.blockPos.getX());
        buf.writeInt(msg.blockPos.getY());
        buf.writeInt(msg.blockPos.getZ());
    }

    public static void handle(ValueComplexMessage msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            if (!msg.failed) {
                if (ctx.get().getSender() != null && ctx.get().getSender().position().distanceTo(Vec3.atCenterOf(msg.blockPos)) <= 10) {
                    BlockEntity blockEntity = ctx.get().getSender().getLevel().getBlockEntity(msg.blockPos);
                    if (blockEntity instanceof BlockEntityValueComplexInput valueComplexInput) {
                        valueComplexInput.setRealValue(msg.real);
                        valueComplexInput.setImaginaryValue(msg.imaginary);
                    }
                }
            }
        });
    }
}
