package eu.tyrano.blockncode.mod.common.blocks;

import eu.tyrano.blockncode.backend.variable.string.BooleanParserModification;
import eu.tyrano.blockncode.backend.variable.string.ModificationType;
import eu.tyrano.blockncode.backend.variable.string.StringModification;
import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorBase;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorType;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.network.NetworkHooks;

import javax.annotation.Nullable;

public class BlockModificationBooleanString extends BlockModificationBinaryString {
    public BlockModificationBooleanString() {
        super();
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos,
                                 Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        if (!pLevel.isClientSide()) {
            BlockEntity entity = pLevel.getBlockEntity(pPos);
            if (entity instanceof BlockEntitySelectorBase<?, ?> && ((BlockEntitySelectorBase<?, ?>) entity).getSelectorType() == BlockEntitySelectorType.booleanToString) {
                NetworkHooks.openGui(((ServerPlayer) pPlayer), ((BlockEntitySelectorBase<?, ?>) entity), pPos);
            } else {
                throw new IllegalStateException("Our Container provider is missing!");
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    private BlockPos getNext(Level worldIn, BlockPos currentPos) {
        BlockPos nextCodePos = null;

        switch (worldIn.getBlockState(currentPos).getValue(FACING)) {
            case EAST -> nextCodePos = new BlockPos(currentPos.east());
            case WEST -> nextCodePos = new BlockPos(currentPos.west());
            case NORTH -> nextCodePos = new BlockPos(currentPos.north());
            case SOUTH -> nextCodePos = new BlockPos(currentPos.south());
        }

        return nextCodePos;
    }

    @Override
    public StringModification compileModification(Level worldIn, BlockPos currentBlockPos, CodeRuntime runtime) {

        BlockEntity blockEntity = worldIn.getBlockEntity(currentBlockPos);
        if (blockEntity instanceof BlockEntitySelectorBase<?, ?> blockEntitySelectorBase && blockEntitySelectorBase.getSelectorType() == BlockEntitySelectorType.booleanToString) {

            BlockPos nextPosA = getNext(worldIn, currentBlockPos);

            if (nextPosA != null) {
                Block nextA = worldIn.getBlockState(nextPosA).getBlock();
                if (nextA instanceof BlockValueBooleanInput nextStringInputA) {
                    return new BooleanParserModification(
                            nextStringInputA.compileCondition(worldIn, nextPosA, runtime),
                            ModificationType.valueOf(blockEntitySelectorBase.getEnumValue().toString()));
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntities.modificationStringBoolean.get().create(pos, state);
    }
}
