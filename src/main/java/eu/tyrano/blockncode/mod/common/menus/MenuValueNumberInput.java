package eu.tyrano.blockncode.mod.common.menus;

import eu.tyrano.blockncode.mod.common.ModMenus;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueBooleanInput;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueNumberInput;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;

public class MenuValueNumberInput extends AbstractContainerMenu {

    private final BlockEntityValueNumberInput blockEntity;
    private final BlockPos blockPos;

    public MenuValueNumberInput(int pContainerId, Inventory inv, FriendlyByteBuf extraData) {
        this(pContainerId, inv, extraData.readBlockPos());
    }

    public MenuValueNumberInput(int pContainerId, Inventory inv, BlockPos blockPos) {
        super(ModMenus.menuValueNumberInput.get(), pContainerId);
        this.blockEntity = ((BlockEntityValueNumberInput) inv.player.level.getBlockEntity(blockPos));
        this.blockPos = blockPos;
    }

    public double getBaseValue() {
        return blockEntity.getValue();
    }

    public BlockPos getBlockPos() {
        return blockPos;
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }
}
