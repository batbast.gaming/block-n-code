package eu.tyrano.blockncode.mod.common;

import eu.tyrano.blockncode.mod.common.entities.robot.RobotEntity;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;

public class ModEntityAttributes {
    public static void register(EntityAttributeCreationEvent attributeCreationEvent) {
        attributeCreationEvent.put(ModEntities.robot.get(), RobotEntity.createLivingAttributes().build());
    }
}
