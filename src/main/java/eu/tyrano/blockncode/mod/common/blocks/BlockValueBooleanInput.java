package eu.tyrano.blockncode.mod.common.blocks;

import eu.tyrano.blockncode.runtime.CodeRuntime;
import eu.tyrano.blockncode.backend.variable.bool.BooleanConditionReader;
import eu.tyrano.blockncode.backend.variable.bool.Condition;
import eu.tyrano.blockncode.backend.variable.bool.BooleanVariable;
import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueBooleanInput;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.network.NetworkHooks;

import javax.annotation.Nullable;
import java.util.UUID;

public class BlockValueBooleanInput extends BlockValueProvider {

    public BlockValueBooleanInput() {
        super();
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos,
                                 Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        if (!pLevel.isClientSide()) {
            BlockEntity entity = pLevel.getBlockEntity(pPos);
            if(entity instanceof BlockEntityValueBooleanInput) {
                NetworkHooks.openGui(((ServerPlayer)pPlayer), (BlockEntityValueBooleanInput)entity, pPos);
            } else {
                throw new IllegalStateException("Our Container provider is missing!");
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntities.valueBooleanInput.get().create(pos, state);
    }

    public Condition compileCondition(Level worldIn, BlockPos currentBlockPos, CodeRuntime runtime) {

        BlockEntity blockEntity = worldIn.getBlockEntity(currentBlockPos);
        if (blockEntity instanceof BlockEntityValueBooleanInput) {
            return new BooleanConditionReader(new BooleanVariable(((BlockEntityValueBooleanInput) blockEntity).getValue()));
        } else {
            return null;
        }
    }
}