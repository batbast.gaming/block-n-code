package eu.tyrano.blockncode.mod.common.menus;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.network.NetworkHooks;
import org.jetbrains.annotations.Nullable;

public class MenuProviderVariableRename implements MenuProvider {
    private final ItemStack stack;

    public MenuProviderVariableRename(ItemStack stack) {
        this.stack = stack;
    }

    @Override
    public Component getDisplayName() {
        return new TextComponent("Variable rename");
    }

    @Nullable
    @Override
    public AbstractContainerMenu createMenu(int p_39954_, Inventory p_39955_, Player p_39956_) {
        return new MenuVariableRename(p_39954_, p_39955_, this);
    }

    public static void openGUI(ServerPlayer serverPlayerEntity, ItemStack itemStack) {
        NetworkHooks.openGui(serverPlayerEntity, new MenuProviderVariableRename(itemStack));
    }
}
