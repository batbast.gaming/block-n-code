package eu.tyrano.blockncode.mod.common.blocks;

import eu.tyrano.blockncode.runtime.CodeRuntime;
import eu.tyrano.blockncode.backend.variable.number.ComplexVariable;
import eu.tyrano.blockncode.backend.variable.number.Expression;
import eu.tyrano.blockncode.backend.variable.number.MathExpressionReader;
import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueComplexInput;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.network.NetworkHooks;

import javax.annotation.Nullable;
import java.util.UUID;

public class BlockValueComplexInput extends BlockValueNumberInput {

    public BlockValueComplexInput() {
        super();
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos,
                                 Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        if (!pLevel.isClientSide()) {
            BlockEntity entity = pLevel.getBlockEntity(pPos);
            if(entity instanceof BlockEntityValueComplexInput) {
                NetworkHooks.openGui(((ServerPlayer)pPlayer), (BlockEntityValueComplexInput)entity, pPos);
            } else {
                throw new IllegalStateException("Our Container provider is missing!");
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntities.valueComplexInput.get().create(pos, state);
    }

    public Expression compileExpression(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {

        BlockEntity blockEntity = worldIn.getBlockEntity(currentPos);
        if (blockEntity instanceof BlockEntityValueComplexInput complexInput) {
            return new MathExpressionReader(new ComplexVariable(complexInput.getRealValue(), complexInput.getImaginaryValue()));
        } else {
            return null;
        }
    }
}
