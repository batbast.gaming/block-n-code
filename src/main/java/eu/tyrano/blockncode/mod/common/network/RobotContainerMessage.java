package eu.tyrano.blockncode.mod.common.network;

import eu.tyrano.blockncode.mod.common.menus.MenuCodeRobot;
import eu.tyrano.blockncode.mod.common.menus.MenuContainerRobot;
import io.netty.buffer.ByteBuf;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class RobotContainerMessage {

    private int x;
    private int y;
    private int z;

    private final boolean failed;

    public RobotContainerMessage(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.failed = false;
    }

    private RobotContainerMessage(boolean failed) {
        this.failed = failed;
    }

    public static RobotContainerMessage decode(ByteBuf buf) {
        try {
            int valX = buf.readInt();
            int valY = buf.readInt();
            int valZ = buf.readInt();
            return new RobotContainerMessage(valX, valY, valZ);
        } catch (IndexOutOfBoundsException e) {
            return new RobotContainerMessage(true);
        }
    }

    public static void messageConsumer(RobotContainerMessage msg, Supplier<NetworkEvent.Context> ctx) {
        handle(msg, ctx);
        ctx.get().setPacketHandled(true);
    }

    public static void encode(RobotContainerMessage msg, FriendlyByteBuf buf) {
        buf.writeInt(msg.x);
        buf.writeInt(msg.y);
        buf.writeInt(msg.z);
    }

    public static void handle(RobotContainerMessage msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            if (!msg.failed && ctx.get().getSender() != null) {
                AbstractContainerMenu containerMenu = ctx.get().getSender().containerMenu;
                if (containerMenu instanceof MenuContainerRobot menuRobot) {
                    menuRobot.setRobotData(msg.x, msg.y, msg.z);
                    ctx.get().getSender().closeContainer();
                }
            }
        });
    }
}