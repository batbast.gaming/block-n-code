package eu.tyrano.blockncode.mod.common.entities.robot;

public enum RobotAction {
    standby,
    moving,
    placing,
    breaking,
    waiting,
    disassemble
}
