package eu.tyrano.blockncode.mod.common.blocks;

import eu.tyrano.blockncode.Block_n_code;
import eu.tyrano.blockncode.backend.variable.string.StringModification;
import eu.tyrano.blockncode.backend.variable.string.StringReader;
import eu.tyrano.blockncode.backend.variable.string.StringVariable;
import eu.tyrano.blockncode.libraries.VoiceChatPlugin;
import eu.tyrano.blockncode.libraries.VoiceRecognition;
import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorBase;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorType;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.network.NetworkHooks;

import javax.annotation.Nullable;
import java.util.UUID;

public class BlockModificationPlayerListen extends BlockValueStringInput {

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        if (!pLevel.isClientSide()) {
            BlockEntity entity = pLevel.getBlockEntity(pPos);
            if (entity instanceof BlockEntitySelectorBase<?, ?> && ((BlockEntitySelectorBase<?, ?>) entity).getSelectorType() == BlockEntitySelectorType.voiceToString) {
                NetworkHooks.openGui(((ServerPlayer) pPlayer), ((BlockEntitySelectorBase<?, ?>) entity), pPos);
            } else {
                throw new IllegalStateException("Our Container provider is missing!");
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    @Override
    public StringModification compileModification(Level worldIn, BlockPos currentBlockPos, CodeRuntime runtime) {

        BlockEntity blockEntity = worldIn.getBlockEntity(currentBlockPos);
        if (blockEntity instanceof BlockEntitySelectorBase<?, ?> blockEntitySelectorBase && blockEntitySelectorBase.getSelectorType() == BlockEntitySelectorType.voiceToString) {
            if (Block_n_code.isVoiceChatLoaded()) {
                return new StringReader(new StringVariable(
                        VoiceRecognition.getRecognitionString(runtime)));
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntities.modificationPlayerListen.get().create(pos, state);
    }
}
