package eu.tyrano.blockncode.mod.common.blockentities;

import eu.tyrano.blockncode.backend.variable.bool.ConditionStringType;
import eu.tyrano.blockncode.mod.client.screens.buttontypes.*;
import eu.tyrano.blockncode.mod.common.entities.robot.RobotAction;
import eu.tyrano.blockncode.mod.common.menus.MenuSelectorBase;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BlockEntitySelectorBase<E extends Enum<E>, F extends Enum<F> & ButtonType> extends BlockEntityValueProvider {

    private final String tagName = "type_tag";
    private E enumValue;
    private final MenuType<MenuSelectorBase<F, BlockEntitySelectorBase<E, F>>> menuType;
    private final BlockEntitySelectorType selectorType;

    public BlockEntitySelectorBase(BlockPos p_155229_, BlockState p_155230_, E baseValue, BlockEntityType<?> blockEntityType, MenuType<MenuSelectorBase<F, BlockEntitySelectorBase<E, F>>> menuType, BlockEntitySelectorType selectorType) {
        super(blockEntityType, p_155229_, p_155230_);
        this.enumValue = baseValue;
        this.menuType = menuType;
        this.selectorType = selectorType;
    }

    public E getEnumValue() {
        return enumValue;
    }

    public BlockEntitySelectorType getSelectorType() {
        return selectorType;
    }

    @Nullable
    @Override
    public AbstractContainerMenu createMenu(int p_39954_, Inventory p_39955_, Player p_39956_) {
        return new MenuSelectorBase<>(p_39954_, p_39955_, getBlockPos(), this.menuType, this.selectorType);
    }

    public void setValueType(E enumValue) {
        this.enumValue = enumValue;
        this.setChanged();
        level.sendBlockUpdated(getBlockPos(), level.getBlockState(getBlockPos()), level.getBlockState(getBlockPos()), 2);
    }

    @Override
    public void onLoad() {
        super.onLoad();
    }

    @Override
    public void invalidateCaps() {
        super.invalidateCaps();
    }

    @Override
    protected void saveAdditional(@NotNull CompoundTag tag) {
        tag.putString(tagName, enumValue.toString());
        super.saveAdditional(tag);
    }

    @Override
    public void load(@NotNull CompoundTag nbt) {
        super.load(nbt);
        switch (this.selectorType) {
            case unaryBoolean -> enumValue = (E) Enum.valueOf(ConditionUnaryBooleanType.class, nbt.getString(tagName));
            case binaryBoolean -> enumValue = (E) Enum.valueOf(ConditionBinaryBooleanType.class, nbt.getString(tagName));
            case mathBoolean -> enumValue = (E) Enum.valueOf(ConditionMathBooleanType.class, nbt.getString(tagName));
            case unaryMath -> enumValue = (E) Enum.valueOf(ExpressionUnaryMathType.class, nbt.getString(tagName));
            case binaryMath -> enumValue = (E) Enum.valueOf(ExpressionBinaryMathType.class, nbt.getString(tagName));
            case binaryString -> enumValue = (E) Enum.valueOf(ModificationStringType.class, nbt.getString(tagName));
            case numberToString -> enumValue = (E) Enum.valueOf(ModificationParseNumberType.class, nbt.getString(tagName));
            case booleanToString -> enumValue = (E) Enum.valueOf(ModificationParseBooleanType.class, nbt.getString(tagName));
            case chatToString -> enumValue = (E) Enum.valueOf(ModificationChatReadType.class, nbt.getString(tagName));
            case voiceToString -> enumValue = (E) Enum.valueOf(ModificationPlayerListenType.class, nbt.getString(tagName));
            case stringToBoolean -> enumValue = (E) Enum.valueOf(ConditionParseStringType.class, nbt.getString(tagName));
            case stringToNumber -> enumValue = (E) Enum.valueOf(ExpressionParseStringType.class, nbt.getString(tagName));
            case stringBoolean -> enumValue = (E) Enum.valueOf(ConditionStringType.class, nbt.getString(tagName));
            case robotInstruction -> enumValue = (E) Enum.valueOf(RobotAction.class, nbt.getString(tagName));
            case direction -> enumValue = (E) Enum.valueOf(DirectionType.class, nbt.getString(tagName));
        }
    }

    @Override
    public @NotNull CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        tag.putString(tagName, enumValue.toString());
        return tag;
    }

    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        // Will get tag from #getUpdateTag
        return ClientboundBlockEntityDataPacket.create(this);
    }
}
