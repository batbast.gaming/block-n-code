package eu.tyrano.blockncode.mod.common.menus.slots;

import eu.tyrano.blockncode.mod.common.items.ArrayVariableItem;
import eu.tyrano.blockncode.mod.common.items.StringVariableItem;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.IItemHandler;

public class SlotArrayVariableContainer extends SlotVariableContainer {
    public SlotArrayVariableContainer(IItemHandler itemHandler, int index, int xPosition, int yPosition) {
        super(itemHandler, index, xPosition, yPosition);
    }

    public boolean isRightType(ItemStack itemStack) {
        return itemStack.getItem() instanceof ArrayVariableItem;
    }
}
