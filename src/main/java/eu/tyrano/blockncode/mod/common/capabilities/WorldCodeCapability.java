package eu.tyrano.blockncode.mod.common.capabilities;

import net.minecraft.nbt.CompoundTag;

public class WorldCodeCapability {
    private int maxCodeLength = 128;
    private double maxCodePerTick = 400;
    private int maxRobotTeleportingRange = 10;
    private double robotDelayMultiplier = 1;

    public int getMaxCodeLength() {
        return maxCodeLength;
    }

    public double getMaxCodePerTick() {
        return maxCodePerTick;
    }

    public int getMaxRobotTeleportingRange() {
        return maxRobotTeleportingRange;
    }

    public double getRobotDelayMultiplier() {
        return robotDelayMultiplier;
    }

    public void setMaxCodeLength(int maxCodeLength) {
        this.maxCodeLength = maxCodeLength;
    }

    public void setMaxCodePerTick(double maxCodePerTick) {
        this.maxCodePerTick = maxCodePerTick;
    }

    public void setMaxRobotTeleportingRange(int maxRobotTeleportingRange) {
        this.maxRobotTeleportingRange = maxRobotTeleportingRange;
    }

    public void setRobotDelayMultiplier(double robotDelayMultiplier) {
        this.robotDelayMultiplier = robotDelayMultiplier;
    }

    public void saveNBTData(CompoundTag nbt) {
        nbt.putInt("length", maxCodeLength);
        nbt.putDouble("tick", maxCodePerTick);
        nbt.putInt("robot_range", maxRobotTeleportingRange);
        nbt.putDouble("robot_delay", robotDelayMultiplier);
    }

    public void loadNBTData(CompoundTag nbt) {
        maxCodeLength = nbt.getInt("length");
        maxCodePerTick = nbt.getDouble("tick");
        maxRobotTeleportingRange = nbt.getInt("robot_range");
        robotDelayMultiplier = nbt.getInt("robot_delay");
    }
}
