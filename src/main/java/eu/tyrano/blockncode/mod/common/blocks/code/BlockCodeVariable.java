package eu.tyrano.blockncode.mod.common.blocks.code;

import eu.tyrano.blockncode.backend.variable.Variable;
import eu.tyrano.blockncode.backend.variable.array.ArrayExpression;
import eu.tyrano.blockncode.backend.variable.array.ArrayExpressionResult;
import eu.tyrano.blockncode.backend.variable.array.ArrayVariable;
import eu.tyrano.blockncode.backend.variable.bool.BooleanVariable;
import eu.tyrano.blockncode.backend.variable.bool.Condition;
import eu.tyrano.blockncode.backend.variable.bool.ConditionResult;
import eu.tyrano.blockncode.backend.variable.number.Expression;
import eu.tyrano.blockncode.backend.variable.number.ExpressionResult;
import eu.tyrano.blockncode.backend.variable.number.NumberVariable;
import eu.tyrano.blockncode.backend.variable.string.ModificationResult;
import eu.tyrano.blockncode.backend.variable.string.StringModification;
import eu.tyrano.blockncode.backend.variable.string.StringVariable;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityVariableWriter;
import eu.tyrano.blockncode.mod.common.blocks.*;
import eu.tyrano.blockncode.mod.common.items.ArrayVariableItem;
import eu.tyrano.blockncode.mod.common.items.BooleanVariableItem;
import eu.tyrano.blockncode.mod.common.items.NumberVariableItem;
import eu.tyrano.blockncode.mod.common.items.StringVariableItem;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import eu.tyrano.blockncode.runtime.ExecutionResult;
import eu.tyrano.blockncode.runtime.Result;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;

public class BlockCodeVariable extends BlockCodeBlank {

    @Override
    public ExecutionResult execute(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {

        BlockPos valueWriterPos = currentPos.above();
        BlockEntity valueWriterEntity = worldIn.getBlockEntity(valueWriterPos);

        if (valueWriterEntity instanceof BlockEntityVariableWriter) {

            ItemStack variableItem = ((BlockEntityVariableWriter) valueWriterEntity).getItem();
            CompoundTag variableCompound = variableItem.getTag();

            if (variableCompound != null) {

                String variableName = variableCompound.getString("variable_name");

                BlockPos valueProviderPos = currentPos.above().above();
                Block valueInput = worldIn.getBlockState(valueProviderPos).getBlock();

                if (valueInput instanceof BlockValueProvider) {

                    if (variableItem.getItem() instanceof BooleanVariableItem && valueInput instanceof BlockValueBooleanInput) {

                        Condition condition = ((BlockValueBooleanInput) valueInput).compileCondition(worldIn, valueProviderPos, runtime);

                        if (condition == null) {
                            return new ExecutionResult(Result.error, "Condition generated at "
                                                                     + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ() + " is not valid", 0);
                        }

                        ConditionResult conditionResult = condition.execute();
                        if (conditionResult.error() == null) {
                            runtime.setVariable(variableName, new BooleanVariable(conditionResult.result()));
                            return new ExecutionResult(Result.ok, null, 0);
                        } else {
                            return new ExecutionResult(Result.error, conditionResult.error() + " at "
                                                                     + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                        }
                    } else if (variableItem.getItem() instanceof NumberVariableItem && valueInput instanceof BlockValueNumberInput valueNumberInput) {

                        Expression expression = valueNumberInput.compileExpression(worldIn, valueProviderPos, runtime);

                        if (expression == null) {
                            return new ExecutionResult(Result.error, "Expression generated at "
                                                                     + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ() + " is not valid", 0);
                        }

                        ExpressionResult expressionResult = expression.execute();
                        if (expressionResult.error() == null) {
                            NumberVariable result = expressionResult.variable();
                            runtime.setVariable(variableName, result);
                            return new ExecutionResult(Result.ok, null, 0);
                        } else {
                            return new ExecutionResult(Result.error, expressionResult.error() + " at "
                                                                     + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                        }
                    } else if (variableItem.getItem() instanceof StringVariableItem && valueInput instanceof BlockValueStringInput valueStringInput) {

                        StringModification modification = valueStringInput.compileModification(worldIn, valueProviderPos, runtime);

                        if (modification == null) {
                            return new ExecutionResult(Result.error, "Modification generated at "
                                                                     + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ() + " is not valid", 0);
                        }

                        ModificationResult modificationResult = modification.execute();

                        if (modificationResult.error() == null) {
                            runtime.setVariable(variableName, new StringVariable(modificationResult.value()));

                            return new ExecutionResult(Result.ok, null, 0);
                        } else {
                            return new ExecutionResult(Result.error, modificationResult.error() + " at "
                                                                     + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                        }
                    } else if (variableItem.getItem() instanceof ArrayVariableItem) {
                        Variable runtimeVariable = runtime.getVariable(variableName);
                        ArrayVariable<?> arrayVariable = null;
                        if (runtimeVariable != null) {
                            if (runtimeVariable instanceof ArrayVariable<?> runtimeArrayVariable) {
                                arrayVariable = runtimeArrayVariable;
                            } else {
                                return new ExecutionResult(Result.error, "Variable " + variableName + " is not an array variable", 0);
                            }
                        }
                        if (arrayVariable != null) {
                            arrayVariable.clear();
                        }

                        boolean action; // 0 = add, 1 = remove
                        BlockPos booleanPos = currentPos.above(3);
                        Block booleanValueInput = worldIn.getBlockState(booleanPos).getBlock();

                        if (booleanValueInput instanceof BlockValueBooleanInput blockValueProvider) {
                            Condition condition = blockValueProvider.compileCondition(worldIn, booleanPos, runtime);
                            if (condition == null) {
                                return new ExecutionResult(Result.error, "Condition generated at "
                                                                         + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ() + " is not valid", 0);
                            }
                            ConditionResult conditionResult = condition.execute();
                            if (conditionResult.error() != null) {
                                return new ExecutionResult(Result.error, conditionResult.error() + " at "
                                                                         + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                            }
                            action = conditionResult.result();
                        } else {
                            return new ExecutionResult(Result.error, "A boolean input is needed for arrays above "
                                                                     + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                        }

                        if (action) {
                            if (valueInput instanceof BlockValueArrayInput blockValueArrayInput) {
                                ArrayExpression<?> arrayExpression = blockValueArrayInput.compileExpression(worldIn, valueProviderPos, runtime);
                                if (arrayExpression == null) {
                                    return new ExecutionResult(Result.error, "Array expression generated at "
                                                                             + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ() + " is not valid", 0);
                                }
                                ArrayExpressionResult<?> arrayExpressionResult = arrayExpression.execute();
                                if (arrayExpressionResult.error() != null) {
                                    return new ExecutionResult(Result.error, arrayExpressionResult.error() + " at "
                                                                             + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                                }
                                if (arrayVariable == null) {
                                    arrayVariable = (ArrayVariable<?>) arrayExpressionResult.variable().copy();
                                } else {
                                    for (Variable variable : arrayExpressionResult.variable()) {
                                        arrayVariable.add(variable);
                                    }
                                }
                            } else if (valueInput instanceof BlockValueBooleanInput blockValueBooleanInput) {
                                Condition condition = blockValueBooleanInput.compileCondition(worldIn, valueProviderPos, runtime);
                                if (condition == null) {
                                    return new ExecutionResult(Result.error, "Condition generated at "
                                                                             + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ() + " is not valid", 0);
                                }
                                ConditionResult conditionResult = condition.execute();
                                if (conditionResult.error() != null) {
                                    return new ExecutionResult(Result.error, conditionResult.error() + " at "
                                                                             + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                                }
                                if (arrayVariable == null) {
                                    arrayVariable = new ArrayVariable<>(BooleanVariable.class);
                                }
                                arrayVariable.add(new BooleanVariable(conditionResult.result()));
                            } else if (valueInput instanceof BlockValueNumberInput blockValueNumberInput) {
                                Expression expression = blockValueNumberInput.compileExpression(worldIn, valueProviderPos, runtime);
                                if (expression == null) {
                                    return new ExecutionResult(Result.error, "Expression generated at "
                                                                             + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ() + " is not valid", 0);
                                }
                                ExpressionResult expressionResult = expression.execute();
                                if (expressionResult.error() != null) {
                                    return new ExecutionResult(Result.error, expressionResult.error() + " at "
                                                                             + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                                }
                                if (arrayVariable == null) {
                                    arrayVariable = new ArrayVariable<>(NumberVariable.class);
                                }
                                arrayVariable.add(expressionResult.variable());
                            } else if (valueInput instanceof BlockValueStringInput blockValueStringInput) {
                                StringModification modification = blockValueStringInput.compileModification(worldIn, valueProviderPos, runtime);
                                if (modification == null) {
                                    return new ExecutionResult(Result.error, "Modification generated at "
                                                                             + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ() + " is not valid", 0);
                                }
                                ModificationResult modificationResult = modification.execute();
                                if (modificationResult.error() != null) {
                                    return new ExecutionResult(Result.error, modificationResult.error() + " at "
                                                                             + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                                }
                                if (arrayVariable == null) {
                                    arrayVariable = new ArrayVariable<>(StringVariable.class);
                                }
                                arrayVariable.add(new StringVariable(modificationResult.value()));
                            } else {
                                return new ExecutionResult(Result.error, "Variable type not recognized", 0);
                            }
                        } else if (valueInput instanceof BlockValueNumberInput blockValueNumberInput) {
                            Expression expression = blockValueNumberInput.compileExpression(worldIn, valueProviderPos, runtime);
                            if (expression == null) {
                                return new ExecutionResult(Result.error, "Expression generated at "
                                                                         + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ() + " is not valid", 0);
                            }
                            ExpressionResult expressionResult = expression.execute();
                            if (expressionResult.error() != null) {
                                return new ExecutionResult(Result.error, expressionResult.error() + " at "
                                                                         + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                            }
                            if (arrayVariable == null) {
                                arrayVariable = new ArrayVariable<>(NumberVariable.class);
                            }
                            arrayVariable.remove((int) expressionResult.variable().getRealValue());
                        } else {
                            return new ExecutionResult(Result.error, "Arrays remove operation needs an index at "
                                                                     + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                        }
                        runtime.setVariable(variableName, arrayVariable);
                        return new ExecutionResult(Result.ok, null, 0);
                    } else {
                        return new ExecutionResult(Result.error, "Variable type does not match value provider type at "
                                                                 + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                    }
                } else {
                    if (variableItem.getItem() instanceof StringVariableItem && runtime.getVariable(variableName) instanceof ArrayVariable<?> arrayVariable) {
                        arrayVariable.clear();
                        return new ExecutionResult(Result.ok, null, 0);
                    } else {
                        return new ExecutionResult(Result.error, "No valid value provider above "
                                                                 + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                    }
                }
            } else {
                return new ExecutionResult(Result.error, "Variable does not has tag at "
                                                         + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
            }
        } else {
            return new ExecutionResult(Result.error, "No valid variable writer above "
                                                     + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
        }
    }
}