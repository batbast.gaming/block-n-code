package eu.tyrano.blockncode.mod.common.network;

import eu.tyrano.blockncode.mod.common.menus.MenuCodeRobot;
import io.netty.buffer.ByteBuf;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class RobotCoordinatesMessage {

    private int x;
    private int y;
    private int z;

    private final boolean failed;

    public RobotCoordinatesMessage(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.failed = false;
    }

    private RobotCoordinatesMessage(boolean failed) {
        this.failed = failed;
    }

    public static RobotCoordinatesMessage decode(ByteBuf buf) {
        try {
            int valX = buf.readInt();
            int valY = buf.readInt();
            int valZ = buf.readInt();
            return new RobotCoordinatesMessage(valX, valY, valZ);
        } catch (IndexOutOfBoundsException e) {
            return new RobotCoordinatesMessage(true);
        }
    }

    public static void messageConsumer(RobotCoordinatesMessage msg, Supplier<NetworkEvent.Context> ctx) {
        handle(msg, ctx);
        ctx.get().setPacketHandled(true);
    }

    public static void encode(RobotCoordinatesMessage msg, FriendlyByteBuf buf) {
        buf.writeInt(msg.x);
        buf.writeInt(msg.y);
        buf.writeInt(msg.z);
    }

    public static void handle(RobotCoordinatesMessage msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            if (!msg.failed && ctx.get().getSender() != null) {
                AbstractContainerMenu containerMenu = ctx.get().getSender().containerMenu;
                if (containerMenu instanceof MenuCodeRobot menuRobot) {
                    menuRobot.setRobotData(msg.x, msg.y, msg.z);
                    ctx.get().getSender().closeContainer();
                }
            }
        });
    }
}