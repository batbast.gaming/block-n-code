package eu.tyrano.blockncode.mod.common.entities.robot;

import net.minecraft.core.BlockPos;

public record RobotActionData(RobotAction action, RobotAction previousAction, BlockPos pos, int data) {
    public static RobotActionData waiting(int endTick, RobotAction previousAction) {
        return new RobotActionData(RobotAction.waiting, previousAction, BlockPos.ZERO, endTick);
    }

    public static RobotActionData standby(RobotAction previousAction) {
        return new RobotActionData(RobotAction.standby, previousAction, BlockPos.ZERO, 0);
    }

    public static RobotActionData moving(BlockPos pos) {
        return new RobotActionData(RobotAction.moving, null, pos, 0);
    }

    public static RobotActionData placing(BlockPos pos, int slot) {
        return new RobotActionData(RobotAction.placing, null, pos, slot);
    }

    public static RobotActionData breaking(BlockPos pos, int slot) {
        return new RobotActionData(RobotAction.breaking, null, pos, slot);
    }

    public static RobotActionData disassemble() {
        return new RobotActionData(RobotAction.disassemble, null, BlockPos.ZERO, 0);
    }
}
