package eu.tyrano.blockncode.mod.common.blocks;

import eu.tyrano.blockncode.backend.variable.Variable;
import eu.tyrano.blockncode.backend.variable.bool.BooleanConditionReader;
import eu.tyrano.blockncode.backend.variable.bool.BooleanVariable;
import eu.tyrano.blockncode.backend.variable.number.Expression;
import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityBooleanVariableReader;
import eu.tyrano.blockncode.mod.common.items.BooleanVariableItem;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.network.NetworkHooks;
import org.jetbrains.annotations.Nullable;

public class BlockVariableReaderArray extends BlockValueArrayInput {
    public BlockVariableReaderArray() {
        super();
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntities.variableArrayReader.get().create(pos, state);
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos,
                                 Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if (!pLevel.isClientSide()) {
            BlockEntity entity = pLevel.getBlockEntity(pPos);
            if (entity instanceof BlockEntityBooleanVariableReader) {
                NetworkHooks.openGui(((ServerPlayer) pPlayer), (BlockEntityArrayVariableReader) entity, pPos);
            } else {
                throw new IllegalStateException("Our Container provider is missing!");
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    @Override
    public Expression compileExpression(Level worldIn, BlockPos currentBlockPos, CodeRuntime runtime) {

        BlockEntity blockEntity = worldIn.getBlockEntity(currentBlockPos);
        if (blockEntity instanceof BlockEntityBooleanVariableReader) {
            ItemStack item = ((BlockEntityBooleanVariableReader) blockEntity).getItem();
            if (item.getItem() instanceof BooleanVariableItem) {
                BooleanVariable booleanVariable = null;
                if (item.getTag() != null) {
                    Variable variable = runtime.getVariable(item.getTag().getString("variable_name"));
                    if (variable instanceof BooleanVariable booleanVar) {
                        booleanVariable = booleanVar;
                    }
                }
                if (booleanVariable != null) {
                    return new BooleanConditionReader(new BooleanVariable(booleanVariable.getData()));
                } else {
                    return new BooleanConditionReader(new BooleanVariable(false));
                }
            } else {
                return new BooleanConditionReader(new BooleanVariable(false));
            }
        } else {
            return null;
        }
    }
}
