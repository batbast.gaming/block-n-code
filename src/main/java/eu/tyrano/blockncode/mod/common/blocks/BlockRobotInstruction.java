package eu.tyrano.blockncode.mod.common.blocks;

import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorBase;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorType;
import eu.tyrano.blockncode.mod.common.entities.robot.RobotAction;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.network.NetworkHooks;

public class BlockRobotInstruction extends Block implements EntityBlock {

    public BlockRobotInstruction() {
        super(BlockBehaviour.Properties.of(Material.METAL)
                .strength(3.0F, 3.0F));
    }

    public RobotAction getInstruction(Level worldIn, BlockPos currentPos) {
        BlockEntity blockEntity = worldIn.getBlockEntity(currentPos);
        if (blockEntity instanceof BlockEntitySelectorBase<?, ?> blockEntitySelectorBase
                && blockEntitySelectorBase.getSelectorType() == BlockEntitySelectorType.robotInstruction) {
            return (RobotAction) blockEntitySelectorBase.getEnumValue();
        } else {
            return null;
        }
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntities.robotInstruction.get().create(pos, state);
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos,
                                 Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        if (!pLevel.isClientSide()) {
            BlockEntity entity = pLevel.getBlockEntity(pPos);
            if (entity instanceof BlockEntitySelectorBase<?,?> selectorBase
                    && selectorBase.getSelectorType() == BlockEntitySelectorType.robotInstruction) {
                NetworkHooks.openGui(((ServerPlayer) pPlayer), selectorBase, pPos);
            } else {
                throw new IllegalStateException("Our Container provider is missing!");
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }
}