package eu.tyrano.blockncode.mod.common;

import eu.tyrano.blockncode.Block_n_code;
import eu.tyrano.blockncode.mod.common.blocks.*;
import eu.tyrano.blockncode.mod.common.blocks.code.*;
import eu.tyrano.blockncode.mod.common.blocks.sign.BlockStandingMetalSign;
import eu.tyrano.blockncode.mod.common.blocks.sign.BlockWallMetalSign;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Supplier;

public class ModBlocks {

    public static final DeferredRegister<Block> BLOCKS =
            DeferredRegister.create(ForgeRegistries.BLOCKS, Block_n_code.MOD_ID);


    public static final RegistryObject<Block> codeStartBlock = registerBlock("code_start",
            BlockCodeStart::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeDebugBlock = registerBlock("code_debug",
            BlockCodeDebug::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeBreak = registerBlock("code_break",
            BlockCodeBreak::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeIfBlock = registerBlock("code_if",
            BlockCodeIf::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeLoopBlock = registerBlock("code_loop",
            BlockCodeLoop::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeRobotWriter = registerBlock("code_robot_writer",
            BlockCodeRobotWriter::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeBaseBlock = registerBlock("code_base",
            BlockCodeBlank::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeRedstoneOutput = registerBlock("code_redstone_output",
            BlockCodeRedstoneOutput::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeExitBlock = registerBlock("code_exit",
            BlockCodeExit::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeVariableBlock = registerBlock("code_variable",
            BlockCodeVariable::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeRobotInstruction = registerBlock("code_robot_instruction",
            BlockCodeRobotInstruction::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeChatAsk = registerBlock("code_chat_ask",
            BlockCodeChatAsk::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeSoundCompute = registerBlock("code_sound_compute",
            BlockCodeSoundCompute::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeSoundInput = registerBlock("code_sound_input",
            BlockCodeSoundInput::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeTTS = registerBlock("code_tts",
            BlockCodeTTS::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> redstoneReaderBlock = registerBlock("redstone_reader",
            BlockRedstoneReader::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> monitorBlock = registerBlock("monitor",
            BlockMonitor::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> variableWriterBlock = registerBlock("code_variable_writer",
            BlockVariableWriter::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> variableNumberReaderBlock = registerBlock("code_number_variable_reader",
            BlockVariableReaderNumber::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> variableBooleanReaderBlock = registerBlock("code_boolean_variable_reader",
            BlockVariableReaderBoolean::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> variableStringReaderBlock = registerBlock("code_string_variable_reader",
            BlockVariableReaderString::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> valueBooleanInputBlock = registerBlock("input_boolean",
            BlockValueBooleanInput::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> valueNumberInputBlock = registerBlock("input_number",
            BlockValueNumberInput::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> robotInstructionValueBlock = registerBlock("input_robot_instruction",
            BlockRobotInstruction::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> directionValueBlock = registerBlock("input_direction",
            BlockDirectionValue::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> valueComplexInputBlock = registerBlock("input_complex",
            BlockValueComplexInput::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> valueStringInputBlock = registerBlock("input_string",
            BlockValueStringInput::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> conditionUnaryBoolean = registerBlock("condition_unary_boolean",
            BlockConditionUnaryBoolean::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> conditionBinaryBoolean = registerBlock("condition_binary_boolean",
            BlockConditionBinaryBoolean::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> conditionMathBoolean = registerBlock("condition_math_boolean",
            BlockConditionMathBoolean::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> conditionStringBoolean = registerBlock("condition_modification_boolean",
            BlockConditionStringBoolean::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> expressionUnaryMath = registerBlock("expression_unary_math",
            BlockExpressionUnaryMath::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> expressionBinaryMath = registerBlock("expression_binary_math",
            BlockExpressionBinaryMath::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> modificationBinaryString = registerBlock("modification_binary_string",
            BlockModificationBinaryString::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> modificationBooleanParser = registerBlock("modification_boolean_string",
            BlockModificationBooleanString::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> modificationNumberParser = registerBlock("modification_number_string",
            BlockModificationNumberString::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> modificationChatRead = registerBlock("modification_chat_read",
            BlockModificationChatRead::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> modificationPlayerListen = registerBlock("modification_player_listen",
            BlockModificationPlayerListen::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> operationStringToNumber = registerBlock("expression_string_number",
            BlockExpressionStringToNumber::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> conditionStringToBoolean = registerBlock("condition_string_boolean",
            BlockConditionStringToBoolean::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> modificationSignReader = registerBlock("modification_sign_reader",
            BlockSignReader::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> standingIronSign = registerBlockWithoutBlockItem("iron_sign",
            BlockStandingMetalSign::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> wallIronSign = registerBlockWithoutBlockItem("iron_wall_sign",
            BlockWallMetalSign::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> codeSignPrinter = registerBlock("code_sign_printer",
            BlockCodeSignPrinter::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> functionCall = registerBlock("code_function_call",
            BlockCodeFunctionCall::new,
            ModCreativeTab.BlockNCodeTab);


    public static final RegistryObject<Block> functionStart = registerBlock("code_function_start",
            BlockCodeFunctionStart::new,
            ModCreativeTab.BlockNCodeTab);


    private static <T extends Block> RegistryObject<T> registerBlockWithoutBlockItem(String name, Supplier<T> block, CreativeModeTab tab) {
        return BLOCKS.register(name, block);
    }

    private static <T extends Block> RegistryObject<T> registerBlock(String name, Supplier<T> block, CreativeModeTab tab) {
        RegistryObject<T> toReturn = BLOCKS.register(name, block);
        registerBlockItem(name, toReturn, tab);
        return toReturn;
    }

    private static <T extends Block> void registerBlockItem(String name, RegistryObject<T> block, CreativeModeTab tab) {
        ModItems.ITEMS.register(name, () -> new BlockItem(block.get(),
                new Item.Properties().tab(tab)));
    }

    public static void register(IEventBus eventBus) {
        BLOCKS.register(eventBus);
    }
}
