package eu.tyrano.blockncode.mod.common.blockentities;

import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.menus.MenuValueBooleanInput;
import eu.tyrano.blockncode.mod.common.menus.MenuValueComplexInput;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.level.block.state.BlockState;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BlockEntityValueComplexInput extends BlockEntityValueProvider {

    private double realValue = 0;
    private double imaginaryValue = 0;

    public BlockEntityValueComplexInput(BlockPos pos, BlockState state) {
        super(ModBlockEntities.valueComplexInput.get(), pos, state);
    }

    public double getRealValue() {
        return realValue;
    }

    @Nullable
    @Override
    public AbstractContainerMenu createMenu(int p_39954_, Inventory p_39955_, Player p_39956_) {
        return new MenuValueComplexInput(p_39954_, p_39955_, getBlockPos());
    }

    public double getImaginaryValue() {
        return imaginaryValue;
    }

    public void setRealValue(double realValue) {
        this.realValue = realValue;
        this.setChanged();
        level.sendBlockUpdated(getBlockPos(), level.getBlockState(getBlockPos()), level.getBlockState(getBlockPos()), 2);
    }

    public void setImaginaryValue(double imaginaryValue) {
        this.imaginaryValue = imaginaryValue;
        this.setChanged();
        level.sendBlockUpdated(getBlockPos(), level.getBlockState(getBlockPos()), level.getBlockState(getBlockPos()), 2);
    }

    @Override
    public void onLoad() {
        super.onLoad();
    }

    @Override
    public void invalidateCaps() {
        super.invalidateCaps();
    }

    @Override
    protected void saveAdditional(@NotNull CompoundTag tag) {
        tag.putDouble("real_value", realValue);
        tag.putDouble("imaginary_value", imaginaryValue);
        super.saveAdditional(tag);
    }

    @Override
    public void load(@NotNull CompoundTag nbt) {
        super.load(nbt);
        realValue = nbt.getDouble("real_value");
        imaginaryValue = nbt.getDouble("imaginary_value");
    }

    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        tag.putDouble("real_value", realValue);
        tag.putDouble("imaginary_value", imaginaryValue);
        return tag;
    }

    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        // Will get tag from #getUpdateTag
        return ClientboundBlockEntityDataPacket.create(this);
    }
}
