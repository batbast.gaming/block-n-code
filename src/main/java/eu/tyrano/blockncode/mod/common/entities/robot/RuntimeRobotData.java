package eu.tyrano.blockncode.mod.common.entities.robot;

import eu.tyrano.blockncode.backend.variable.Variable;
import eu.tyrano.blockncode.backend.variable.bool.BooleanVariable;
import eu.tyrano.blockncode.backend.variable.number.NumberVariable;
import eu.tyrano.blockncode.backend.variable.string.StringVariable;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;

import java.util.HashMap;

public record RuntimeRobotData(BlockPos robotPos, RobotAction previousAction, BlockState blockAbove,
                               BlockState blockBelow, HashMap<String, Variable> storedVariables,
                               RobotEntity robot) implements RuntimeDataProvider {

    @Override
    public RobotEntity getRobot() {
        return robot;
    }

    @Override
    public HashMap<String, Variable> getVariables() {
        HashMap<String, Variable> variables = new HashMap<>();
        variables.put("action_moving", new BooleanVariable(previousAction == RobotAction.moving));
        variables.put("action_standby", new BooleanVariable(previousAction == RobotAction.standby));
        variables.put("action_waiting", new BooleanVariable(previousAction == RobotAction.waiting));
        variables.put("action_breaking", new BooleanVariable(previousAction == RobotAction.breaking));
        variables.put("action_placing", new BooleanVariable(previousAction == RobotAction.placing));
        variables.put("robot_x", new NumberVariable(robotPos.getX()));
        variables.put("robot_y", new NumberVariable(robotPos.getY()));
        variables.put("robot_z", new NumberVariable(robotPos.getZ()));
        variables.put("robot_block_above", new StringVariable(blockAbove.getBlock().getName().getString().toLowerCase()));
        variables.put("robot_block_below", new StringVariable(blockBelow.getBlock().getName().getString().toLowerCase()));
        variables.putAll(storedVariables);
        return variables;
    }
}
