package eu.tyrano.blockncode.mod.common.items;

import eu.tyrano.blockncode.mod.common.ModEntities;
import eu.tyrano.blockncode.mod.common.blocks.code.BlockCodeStart;
import eu.tyrano.blockncode.mod.common.entities.robot.RobotEntity;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class RobotBottleItem extends Item {
    public RobotBottleItem(Properties p_41383_) {
        super(p_41383_);
    }

    public @NotNull InteractionResult useOn(UseOnContext p_43223_) {
        Level level = p_43223_.getLevel();
        if (!(level instanceof ServerLevel)) {
            return InteractionResult.SUCCESS;
        } else {
            ItemStack itemstack = p_43223_.getItemInHand();
            BlockPos blockPos = p_43223_.getClickedPos();
            Direction direction = p_43223_.getClickedFace();
            BlockState blockstate = level.getBlockState(blockPos);

            if (blockstate.getBlock() instanceof ChestBlock) {
                CompoundTag tag = itemstack.getTagElement("EntityTag");
                itemstack.addTagElement("EntityTag", setItemTag(tag, blockPos, "RobotCodeData", false));
                return InteractionResult.SUCCESS;
            } else if (blockstate.getBlock() instanceof BlockCodeStart) {
                CompoundTag tag = itemstack.getTagElement("EntityTag");
                itemstack.addTagElement("EntityTag", setItemTag(tag, blockPos, "RobotContainerData", true));
                return InteractionResult.SUCCESS;
            }

            BlockPos pos;
            if (blockstate.getCollisionShape(level, blockPos).isEmpty()) {
                pos = blockPos;
            } else {
                pos = blockPos.relative(direction);
            }

            if (ModEntities.robot.get().spawn((ServerLevel) level, itemstack,
                    p_43223_.getPlayer(), pos, MobSpawnType.SPAWN_EGG,
                    true, !Objects.equals(blockPos, pos)
                            && direction == Direction.UP) != null) {
                itemstack.shrink(1);
                level.gameEvent(p_43223_.getPlayer(), GameEvent.ENTITY_PLACE, blockPos);
            }

            return InteractionResult.CONSUME;
        }
    }

    private CompoundTag setItemTag(CompoundTag tag, BlockPos pos, String conserved, boolean first) {
        BlockPos codePos;

        if (tag != null && tag.contains(conserved)
                && tag.get(conserved) instanceof CompoundTag tagCode) {

            codePos = new BlockPos(
                    tagCode.getInt("x"),
                    tagCode.getInt("y"),
                    tagCode.getInt("z"));
        } else {
            codePos = BlockPos.ZERO;
        }

        CompoundTag finalTag = new CompoundTag();

        if (first) {
            RobotEntity.addRobotTag(finalTag, new BlockPos(
                    pos.getX(), pos.getY(), pos.getZ()), codePos);
        } else {
            RobotEntity.addRobotTag(finalTag, codePos, new BlockPos(
                    pos.getX(), pos.getY(), pos.getZ()));
        }

        return finalTag;
    }
}