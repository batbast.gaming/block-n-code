package eu.tyrano.blockncode.mod.common.entities.robot;

import eu.tyrano.blockncode.backend.variable.Variable;
import eu.tyrano.blockncode.backend.variable.bool.BooleanVariable;
import eu.tyrano.blockncode.backend.variable.number.NumberVariable;
import eu.tyrano.blockncode.backend.variable.string.StringVariable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public interface RuntimeDataProvider {
    RobotEntity getRobot();
    HashMap<String, Variable> getVariables();
}
