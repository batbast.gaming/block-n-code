package eu.tyrano.blockncode.mod.common.blocks.code;

import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityFunctionCall;
import eu.tyrano.blockncode.runtime.*;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.network.NetworkHooks;
import org.jetbrains.annotations.Nullable;

public class BlockCodeFunctionCall extends BlockCodeBlank implements EntityBlock {

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return new BlockEntityFunctionCall(pos, state);
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos,
                                 Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        if (!pLevel.isClientSide()) {
            BlockEntity entity = pLevel.getBlockEntity(pPos);
            if (entity instanceof BlockEntityFunctionCall functionCall) {
                NetworkHooks.openGui(((ServerPlayer) pPlayer), functionCall, pPos);
            } else {
                throw new IllegalStateException("Our Container provider is missing!");
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    public ExecutionResult execute(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {
        return new ExecutionResult(Result.ok, null, 0);
    }

    @Override
    public CodeRuntime getOptionalFork(Level world, BlockPos currentPos, CodeRuntime runtime) {
        BlockEntityFunctionCall blockEntity = ((BlockEntityFunctionCall) world.getBlockEntity(currentPos));
        if (blockEntity != null && world.isLoaded(blockEntity.getTargetPos())) {
            CodeRuntimeExtends compiler = new CodeRuntimeExtends(world, blockEntity.getTargetPos(), runtime);
            compiler.compile();
            return compiler;
        }
        return null;
    }
}