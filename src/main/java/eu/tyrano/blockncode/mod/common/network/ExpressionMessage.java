package eu.tyrano.blockncode.mod.common.network;

import eu.tyrano.blockncode.backend.variable.number.OperationType;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorBase;
import eu.tyrano.blockncode.mod.common.blocks.BlockExpressionBinaryMath;
import eu.tyrano.blockncode.mod.common.blocks.BlockExpressionUnaryMath;
import io.netty.buffer.ByteBuf;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.network.NetworkEvent;

import java.nio.charset.Charset;
import java.util.function.Supplier;

public class ExpressionMessage extends SelectorMessageBase{

    private final boolean failed;
    private OperationType type;

    public ExpressionMessage() {
        this.failed = false;
    }

    private ExpressionMessage(boolean failed) {
        this.failed = failed;
    }

    public static ExpressionMessage decode(ByteBuf buf) {
        try {
            int length = buf.readInt();
            OperationType type = OperationType.valueOf(buf.readCharSequence(length,Charset.defaultCharset()).toString());
            int x = buf.readInt();
            int y = buf.readInt();
            int z = buf.readInt();
            ExpressionMessage expressionMessage = new ExpressionMessage();
            expressionMessage.blockPos = new BlockPos(x, y, z);
            expressionMessage.type = type;
            expressionMessage.setReady();
            return expressionMessage;
        } catch (IndexOutOfBoundsException | IllegalArgumentException e) {
            return new ExpressionMessage(true);
        }
    }

    public static void messageConsumer(ExpressionMessage msg, Supplier<NetworkEvent.Context> ctx) {
        if (msg.ready) {
            handle(msg, ctx);
        }
        ctx.get().setPacketHandled(true);
    }

    public void finalizeEncode(FriendlyByteBuf buf) {
        buf.writeInt(super.value.toString().length());
        buf.writeCharSequence(super.value.toString(), Charset.defaultCharset());
        buf.writeInt(super.blockPos.getX());
        buf.writeInt(super.blockPos.getY());
        buf.writeInt(super.blockPos.getZ());
    }

    public static void handle(ExpressionMessage msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            if (!msg.failed) {
                if (ctx.get().getSender() != null && ctx.get().getSender().position().distanceTo(Vec3.atCenterOf(msg.blockPos)) <= 10) {
                    BlockEntity blockEntity = ctx.get().getSender().getLevel().getBlockEntity(msg.blockPos);
                    Block block = ctx.get().getSender().getLevel().getBlockState(msg.blockPos).getBlock();
                    if (block instanceof BlockExpressionBinaryMath && blockEntity instanceof BlockEntitySelectorBase expressionBinaryMath) {
                        expressionBinaryMath.setValueType(msg.type);
                    } else if (block instanceof BlockExpressionUnaryMath && blockEntity instanceof BlockEntitySelectorBase expressionUnaryMath) {
                        expressionUnaryMath.setValueType(msg.type);
                    }
                }
            }
        });
    }
}
