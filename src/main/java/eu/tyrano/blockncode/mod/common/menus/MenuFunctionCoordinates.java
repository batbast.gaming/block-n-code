package eu.tyrano.blockncode.mod.common.menus;

import eu.tyrano.blockncode.mod.common.ModMenus;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityFunctionCall;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;

public class MenuFunctionCoordinates extends AbstractContainerMenu {
    private final BlockEntityFunctionCall blockEntity;
    private final BlockPos blockPos;

    public MenuFunctionCoordinates(int pContainerId, Inventory inv, FriendlyByteBuf extraData) {
        this(pContainerId, inv, extraData.readBlockPos());
    }

    public MenuFunctionCoordinates(int pContainerId, Inventory inv, BlockPos blockPos) {
        super(ModMenus.menuFunctionCoordinates.get(), pContainerId);
        this.blockEntity = ((BlockEntityFunctionCall) inv.player.level.getBlockEntity(blockPos));
        this.blockPos = blockPos;
    }

    public BlockPos getBaseValue() {
        return blockEntity.getTargetPos();
    }

    public BlockPos getBlockPos() {
        return blockPos;
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }
}
