package eu.tyrano.blockncode.mod.common.blocks;

import eu.tyrano.blockncode.backend.variable.Variable;
import eu.tyrano.blockncode.backend.variable.number.ComplexVariable;
import eu.tyrano.blockncode.backend.variable.number.Expression;
import eu.tyrano.blockncode.backend.variable.number.MathExpressionReader;
import eu.tyrano.blockncode.backend.variable.number.NumberVariable;
import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityNumberVariableReader;
import eu.tyrano.blockncode.mod.common.items.NumberVariableItem;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.network.NetworkHooks;
import org.jetbrains.annotations.Nullable;

public class BlockVariableReaderNumber extends BlockValueNumberInput {
    public BlockVariableReaderNumber() {
        super();
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntities.variableNumberReader.get().create(pos, state);
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos,
                                 Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if (!pLevel.isClientSide()) {
            BlockEntity entity = pLevel.getBlockEntity(pPos);
            if (entity instanceof BlockEntityNumberVariableReader) {
                NetworkHooks.openGui(((ServerPlayer) pPlayer), (BlockEntityNumberVariableReader) entity, pPos);
            } else {
                throw new IllegalStateException("Our Container provider is missing!");
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    @Override
    public Expression compileExpression(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {

        BlockEntity blockEntity = worldIn.getBlockEntity(currentPos);
        if (blockEntity instanceof BlockEntityNumberVariableReader) {
            ItemStack item = ((BlockEntityNumberVariableReader) blockEntity).getItem();
            if (item.getItem() instanceof NumberVariableItem) {
                NumberVariable numberVariable = null;
                if (item.getTag() != null) {
                    Variable variable = runtime.getVariable(item.getTag().getString("variable_name"));
                    if (variable instanceof NumberVariable) {
                        numberVariable = (NumberVariable) variable;
                    }
                }
                if (numberVariable != null) {
                    if (numberVariable instanceof ComplexVariable) {
                        return new MathExpressionReader(new ComplexVariable(numberVariable));
                    } else {
                        return new MathExpressionReader(new NumberVariable(numberVariable));
                    }
                } else {
                    return new MathExpressionReader(new NumberVariable(0));
                }
            } else {
                return new MathExpressionReader(new NumberVariable(0));
            }
        } else {
            return null;
        }
    }
}
