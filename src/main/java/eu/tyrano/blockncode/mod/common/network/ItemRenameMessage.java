package eu.tyrano.blockncode.mod.common.network;

import eu.tyrano.blockncode.mod.common.items.VariableItemBase;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.StringTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.network.NetworkEvent;

import java.nio.charset.Charset;
import java.util.function.Supplier;

public class ItemRenameMessage {

    private String itemName;

    private final boolean failed;

    public ItemRenameMessage(String itemName) {
        this.itemName = itemName;
        this.failed = false;
    }

    private ItemRenameMessage(boolean failed) {
        this.failed = failed;
    }

    public static ItemRenameMessage decode(ByteBuf buf) {
        try {
            int length = buf.readInt();
            String itemName = buf.readCharSequence(length, Charset.defaultCharset()).toString();
            return new ItemRenameMessage(itemName);
        } catch (IndexOutOfBoundsException e) {
            return new ItemRenameMessage(true);
        }
    }

    public static void messageConsumer(ItemRenameMessage msg, Supplier<NetworkEvent.Context> ctx) {
        handle(msg, ctx);
        ctx.get().setPacketHandled(true);
    }

    public static void encode(ItemRenameMessage msg, FriendlyByteBuf buf) {
        buf.writeInt(msg.itemName.length());
        buf.writeCharSequence(msg.itemName, Charset.defaultCharset());
    }

    public static void handle(ItemRenameMessage msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            if (!msg.failed) {
                Player player = ctx.get().getSender();
                if (player != null) {
                    ItemStack itemStack = player.getMainHandItem();
                    if (itemStack.getItem() instanceof VariableItemBase) {
                        itemStack.addTagElement("variable_name", StringTag.valueOf(msg.itemName));
                    }
                }
            }
        });
    }
}