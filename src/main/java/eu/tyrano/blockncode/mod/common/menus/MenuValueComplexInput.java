package eu.tyrano.blockncode.mod.common.menus;

import eu.tyrano.blockncode.mod.common.ModMenus;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueComplexInput;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueNumberInput;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;

public class MenuValueComplexInput extends AbstractContainerMenu {

    private final BlockEntityValueComplexInput blockEntity;
    private final BlockPos blockPos;

    public MenuValueComplexInput(int pContainerId, Inventory inv, FriendlyByteBuf extraData) {
        this(pContainerId, inv, extraData.readBlockPos());
    }

    public MenuValueComplexInput(int pContainerId, Inventory inv, BlockPos blockPos) {
        super(ModMenus.menuValueComplexInput.get(), pContainerId);
        this.blockEntity = ((BlockEntityValueComplexInput) inv.player.level.getBlockEntity(blockPos));
        this.blockPos = blockPos;
    }

    public double getBaseRealValue() {
        return blockEntity.getRealValue();
    }

    public double getBaseImaginaryValue() {
        return blockEntity.getImaginaryValue();
    }

    public BlockPos getBlockPos() {
        return blockPos;
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }
}
