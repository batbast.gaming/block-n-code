package eu.tyrano.blockncode.mod.common.blocks.code;

import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityVariableWriter;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import eu.tyrano.blockncode.runtime.ExecutionResult;
import eu.tyrano.blockncode.runtime.Result;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;

public class BlockCodeRobotWriter extends BlockCodeBlank {
    @Override
    public ExecutionResult execute(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {
        BlockPos valueWriterPos = currentPos.above();
        BlockEntity valueWriterEntity = worldIn.getBlockEntity(valueWriterPos);

        if (valueWriterEntity instanceof BlockEntityVariableWriter) {

            ItemStack variableItem = ((BlockEntityVariableWriter) valueWriterEntity).getItem();
            CompoundTag variableCompound = variableItem.getTag();

            if (variableCompound != null) {
                String variableName = variableCompound.getString("variable_name");

                runtime.getRobot().addVariable(variableName, runtime.getVariable(variableName));
                return new ExecutionResult(Result.ok, null, 0);
            } else {
                return new ExecutionResult(Result.error, "Variable does not has tag at "
                                                         + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
            }
        } else {
            return new ExecutionResult(Result.error, "No valid variable writer above "
                                                     + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
        }
    }
}