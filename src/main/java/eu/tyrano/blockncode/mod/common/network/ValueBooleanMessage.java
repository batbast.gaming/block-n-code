package eu.tyrano.blockncode.mod.common.network;

import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueBooleanInput;
import io.netty.buffer.ByteBuf;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ValueBooleanMessage {

    private boolean value;
    private BlockPos blockPos;

    private final boolean failed;

    public ValueBooleanMessage(boolean value, BlockPos blockPos) {
        this.value = value;
        this.blockPos = blockPos;
        this.failed = false;
    }

    private ValueBooleanMessage(boolean failed) {
        this.failed = failed;
    }

    public static ValueBooleanMessage decode(ByteBuf buf) {
        try {
            boolean value = buf.readBoolean();
            int x = buf.readInt();
            int y = buf.readInt();
            int z = buf.readInt();
            return new ValueBooleanMessage(value, new BlockPos(x, y, z));
        } catch (IndexOutOfBoundsException e) {
            return new ValueBooleanMessage(true);
        }
    }

    public static void messageConsumer(ValueBooleanMessage msg, Supplier<NetworkEvent.Context> ctx) {
        handle(msg, ctx);
        ctx.get().setPacketHandled(true);
    }

    public static void encode(ValueBooleanMessage msg, FriendlyByteBuf buf) {
        buf.writeBoolean(msg.value);
        buf.writeInt(msg.blockPos.getX());
        buf.writeInt(msg.blockPos.getY());
        buf.writeInt(msg.blockPos.getZ());
    }

    public static void handle(ValueBooleanMessage msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            if (!msg.failed) {
                if (ctx.get().getSender() != null && ctx.get().getSender().position().distanceTo(Vec3.atCenterOf(msg.blockPos)) <= 10) {
                    BlockEntity blockEntity = ctx.get().getSender().getLevel().getBlockEntity(msg.blockPos);
                    if (blockEntity instanceof BlockEntityValueBooleanInput valueBooleanInput) {
                        valueBooleanInput.setValue(msg.value);
                    }
                }
            }
        });
    }
}
