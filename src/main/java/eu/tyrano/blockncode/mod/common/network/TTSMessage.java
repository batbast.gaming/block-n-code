package eu.tyrano.blockncode.mod.common.network;

import eu.tyrano.blockncode.mod.client.tts.ClientTTSPacketHandler;
import io.netty.buffer.ByteBuf;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.network.NetworkEvent;

import java.nio.charset.Charset;
import java.util.function.Supplier;

public class TTSMessage {

    private String value;

    private final boolean failed;

    public TTSMessage(String value) {
        this.value = value;
        this.failed = false;
    }

    private TTSMessage(boolean failed) {
        this.failed = failed;
    }

    public static TTSMessage decode(ByteBuf buf) {
        try {
            int length = buf.readInt();
            String newValue = buf.readCharSequence(length, Charset.defaultCharset()).toString();
            return new TTSMessage(newValue);
        } catch (IndexOutOfBoundsException e) {
            return new TTSMessage(true);
        }
    }

    public static void messageConsumer(TTSMessage msg, Supplier<NetworkEvent.Context> ctx) {
        handle(msg, ctx);
        ctx.get().setPacketHandled(true);
    }

    public static void encode(TTSMessage msg, FriendlyByteBuf buf) {
        buf.writeInt(msg.value.length());
        buf.writeCharSequence(msg.value, Charset.defaultCharset());
    }

    public static void handle(TTSMessage msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> {
            if (!msg.failed) {
                ClientTTSPacketHandler.handlePacket(msg.value);
            }
        }));
        ctx.get().setPacketHandled(true);
    }
}
