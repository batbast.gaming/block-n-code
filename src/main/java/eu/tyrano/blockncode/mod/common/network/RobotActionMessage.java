package eu.tyrano.blockncode.mod.common.network;

import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorBase;
import eu.tyrano.blockncode.mod.common.blocks.BlockRobotInstruction;
import eu.tyrano.blockncode.mod.common.entities.robot.RobotAction;
import io.netty.buffer.ByteBuf;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.network.NetworkEvent;

import java.nio.charset.Charset;
import java.util.function.Supplier;

public class RobotActionMessage extends SelectorMessageBase {
    private final boolean failed;
    private RobotAction type;

    public RobotActionMessage() {
        this.failed = false;
    }

    private RobotActionMessage(boolean failed) {
        this.failed = failed;
    }

    public static RobotActionMessage decode(ByteBuf buf) {
        try {
            int length = buf.readInt();
            RobotAction type = RobotAction.valueOf(buf.readCharSequence(length, Charset.defaultCharset()).toString());
            int x = buf.readInt();
            int y = buf.readInt();
            int z = buf.readInt();
            RobotActionMessage robotActionMessage = new RobotActionMessage();
            robotActionMessage.setBlockPos(new BlockPos(x, y, z));
            robotActionMessage.type = type;
            robotActionMessage.setReady();
            return robotActionMessage;
        } catch (IndexOutOfBoundsException | IllegalArgumentException e) {
            return new RobotActionMessage(true);
        }
    }

    public static void messageConsumer(RobotActionMessage msg, Supplier<NetworkEvent.Context> ctx) {
        if (msg.ready) {
            handle(msg, ctx);
        }
        ctx.get().setPacketHandled(true);
    }

    public void finalizeEncode(FriendlyByteBuf buf) {
        buf.writeInt(super.value.toString().length());
        buf.writeCharSequence(super.value.toString(), Charset.defaultCharset());
        buf.writeInt(super.blockPos.getX());
        buf.writeInt(super.blockPos.getY());
        buf.writeInt(super.blockPos.getZ());
    }

    public static void handle(RobotActionMessage msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            if (!msg.failed) {
                if (ctx.get().getSender() != null && ctx.get().getSender().position().distanceTo(Vec3.atCenterOf(msg.blockPos)) <= 10) {
                    BlockEntity blockEntity = ctx.get().getSender().getLevel().getBlockEntity(msg.blockPos);
                    Block block = ctx.get().getSender().getLevel().getBlockState(msg.blockPos).getBlock();
                    if (block instanceof BlockRobotInstruction && blockEntity instanceof BlockEntitySelectorBase robotInstruction) {
                        robotInstruction.setValueType(msg.type);
                    }
                }
            }
        });
    }
}
