package eu.tyrano.blockncode.mod.common.blocks.code;

import eu.tyrano.blockncode.backend.variable.bool.Condition;
import eu.tyrano.blockncode.backend.variable.bool.ConditionResult;
import eu.tyrano.blockncode.mod.common.blocks.BlockValueBooleanInput;
import eu.tyrano.blockncode.runtime.*;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;

public class BlockCodeIf extends BlockCodeBlank {

    @Override
    public CodeRuntime getOptionalFork(Level world, BlockPos currentPos, CodeRuntime runtime) {
        BlockPos altCodePos;
        switch (world.getBlockState(currentPos).getValue(FACING)) {
            case EAST -> altCodePos = new BlockPos(currentPos.north());
            case WEST -> altCodePos = new BlockPos(currentPos.south());
            case NORTH -> altCodePos = new BlockPos(currentPos.west());
            case SOUTH -> altCodePos = new BlockPos(currentPos.east());
            default -> {
                return null;
            }
        }

        CodeRuntimeExtends compiler = new CodeRuntimeExtends(world, altCodePos, runtime);
        compiler.compile();
        return compiler;
    }

    public ExecutionResult execute(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {

        BlockPos valueProviderPos = currentPos.above();
        Block valueInput = worldIn.getBlockState(valueProviderPos).getBlock();

        if (valueInput instanceof BlockValueBooleanInput valueBooleanInput) {
            Condition condition = valueBooleanInput.compileCondition(worldIn, valueProviderPos, runtime);

            if (condition == null) {
                return new ExecutionResult(Result.error, "Condition generated at "
                        + currentPos.getX() + ","
                        + currentPos.getY() + ","
                        + currentPos.getZ() + " is null", 0);
            }

            ConditionResult conditionResult = condition.execute();

            if (conditionResult.error() == null) {

                if (conditionResult.result()) {
                    return new ExecutionResult(Result.ok, null, 1);
                } else {
                    return new ExecutionResult(Result.ok, null, 0);
                }

            } else {
                return new ExecutionResult(Result.error, conditionResult.error() + " at "
                        + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
            }
        } else {
            return new ExecutionResult(Result.error, "No valid value provider above "
                    + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
        }
    }
}
