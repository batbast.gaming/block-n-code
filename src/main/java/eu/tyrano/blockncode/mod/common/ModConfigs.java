package eu.tyrano.blockncode.mod.common;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;
import eu.tyrano.blockncode.runtime.ChatBot;
import net.minecraftforge.common.ForgeConfigSpec;

import java.nio.file.Path;

public class ModConfigs {
    private static ChatBot chatGPT;
    public static final ForgeConfigSpec GENERAL_SPEC;
    public static ForgeConfigSpec.ConfigValue<String> chatGPTKey;

    static {
        ForgeConfigSpec.Builder configBuilder = new ForgeConfigSpec.Builder();
        setupConfig(configBuilder);
        GENERAL_SPEC = configBuilder.build();
    }

    public static ChatBot getChatGPT() {
        return chatGPT;
    }

    private static void setupConfig(ForgeConfigSpec.Builder builder) {
        chatGPTKey = builder.comment("Enter your ChatGPT AI Key here, generated at " +
                                     "https://platform.openai.com/account/api-keys. Keep it secret !\n" +
                        "Leave this field blank to disable chatGPT in block_n_code")
                .define("openai_key", "");
    }

    public static void initGPT() {
        if (chatGPTKey != null && !chatGPTKey.toString().equals("")) {
            chatGPT = new ChatBot(chatGPTKey.get());
        }
    }

    public static void init(Path file) {
        final CommentedFileConfig configData = CommentedFileConfig.builder(file)
                .sync()
                .autosave()
                .writingMode(WritingMode.REPLACE)
                .build();

        configData.load();
        GENERAL_SPEC.setConfig(configData);
    }
}
