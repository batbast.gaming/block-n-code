package eu.tyrano.blockncode.mod.common.menus;

import eu.tyrano.blockncode.mod.common.blocks.BlockVariableReaderArray;
import eu.tyrano.blockncode.mod.common.blocks.BlockVariableReaderBoolean;
import eu.tyrano.blockncode.mod.common.blocks.BlockVariableReaderNumber;
import eu.tyrano.blockncode.mod.common.blocks.BlockVariableReaderString;
import eu.tyrano.blockncode.mod.common.menus.slots.*;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.NotNull;

public class MenuVariableReader extends AbstractContainerMenu {

    private final BlockEntity blockEntity;
    private final Level level;
    private final RegistryObject<Block> block;

    public MenuVariableReader(int pContainerId, Inventory inv, FriendlyByteBuf extraData, RegistryObject<MenuType<MenuVariableReader>> menuType, RegistryObject<Block> block) {
        this(pContainerId, inv, inv.player.level.getBlockEntity(extraData.readBlockPos()), menuType, block);
    }

    public MenuVariableReader(int pContainerId, Inventory inv, BlockEntity entity, RegistryObject<MenuType<MenuVariableReader>> menuType, RegistryObject<Block> block) {
        super(menuType.get(), pContainerId);
        checkContainerSize(inv, 1);
        blockEntity = entity;
        this.level = inv.player.level;
        this.block = block;

        addPlayerInventory(inv);
        addPlayerHotbar(inv);

        if (block.get() instanceof BlockVariableReaderBoolean) {
            this.blockEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(handler ->
                    this.addSlot(new SlotBooleanVariableContainer(handler, 0, 62 + 22, 18 + 26)));
        } else if (block.get() instanceof BlockVariableReaderNumber) {
            this.blockEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(handler ->
                    this.addSlot(new SlotNumberVariableContainer(handler, 0, 62 + 22, 18 + 26)));
        } else if (block.get() instanceof BlockVariableReaderString) {
            this.blockEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(handler ->
                    this.addSlot(new SlotStringVariableContainer(handler, 0, 62 + 22, 18 + 26)));
        } else if (block.get() instanceof BlockVariableReaderArray) {
            this.blockEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(handler ->
                    this.addSlot(new SlotArrayVariableContainer(handler, 0, 62 + 22, 18 + 26)));
        }
    }

    @Override
    public boolean stillValid(@NotNull Player pPlayer) {
        return stillValid(ContainerLevelAccess.create(level, blockEntity.getBlockPos()),
                pPlayer, block.get());
    }

    private void addPlayerInventory(Inventory playerInventory) {
        for (int k = 0; k < 3; ++k) {
            for (int i = 0; i < 9; ++i) {
                this.addSlot(new Slot(playerInventory, i + k * 9 + 9, 12 + i * 18, 102 + k * 18));
            }
        }
    }

    private void addPlayerHotbar(Inventory playerInventory) {
        for (int l = 0; l < 9; ++l) {
            this.addSlot(new Slot(playerInventory, l, 12 + l * 18, 160));
        }
    }

    // ALL CREDITS TO : diesieben07
    private static final int HOTBAR_SLOT_COUNT = 9;
    private static final int PLAYER_INVENTORY_ROW_COUNT = 3;
    private static final int PLAYER_INVENTORY_COLUMN_COUNT = 9;
    private static final int PLAYER_INVENTORY_SLOT_COUNT = PLAYER_INVENTORY_COLUMN_COUNT * PLAYER_INVENTORY_ROW_COUNT;
    private static final int VANILLA_SLOT_COUNT = HOTBAR_SLOT_COUNT + PLAYER_INVENTORY_SLOT_COUNT;
    private static final int VANILLA_FIRST_SLOT_INDEX = 0;
    private static final int TE_INVENTORY_FIRST_SLOT_INDEX = VANILLA_FIRST_SLOT_INDEX + VANILLA_SLOT_COUNT;

    private static final int TE_INVENTORY_SLOT_COUNT = 1;  // must be the number of slots you have!

    @Override
    public @NotNull ItemStack quickMoveStack(@NotNull Player playerIn, int index) {

        Slot sourceSlot = slots.get(index);
        if (!sourceSlot.hasItem()) return ItemStack.EMPTY;  //EMPTY_ITEM
        ItemStack sourceStack = sourceSlot.getItem();
        ItemStack copyOfSourceStack = sourceStack.copy();

        // Check if the slot clicked is one of the vanilla container slots
        if (index < VANILLA_FIRST_SLOT_INDEX + VANILLA_SLOT_COUNT) {
            // This is a vanilla container slot so merge the stack into the tile inventory
            if (!moveItemStackTo(sourceStack, TE_INVENTORY_FIRST_SLOT_INDEX, TE_INVENTORY_FIRST_SLOT_INDEX
                    + TE_INVENTORY_SLOT_COUNT, false)) {
                return ItemStack.EMPTY;  // EMPTY_ITEM
            }
        } else if (index < TE_INVENTORY_FIRST_SLOT_INDEX + TE_INVENTORY_SLOT_COUNT) {
            // This is a TE slot so merge the stack into the players inventory
            if (!moveItemStackTo(sourceStack, VANILLA_FIRST_SLOT_INDEX, VANILLA_FIRST_SLOT_INDEX + VANILLA_SLOT_COUNT, false)) {
                return ItemStack.EMPTY;
            }
        } else {
            System.out.println("Invalid slotIndex:" + index);
            return ItemStack.EMPTY;
        }
        // If stack size == 0 (the entire stack was moved) set slot contents to null
        if (sourceStack.getCount() == 0) {
            sourceSlot.set(ItemStack.EMPTY);
        } else {
            sourceSlot.setChanged();
        }
        sourceSlot.onTake(playerIn, sourceStack);
        return copyOfSourceStack;
    }
}
