package eu.tyrano.blockncode.mod.common.menus.robot;

import net.minecraft.core.BlockPos;

public record RobotData(BlockPos pos) {
}
