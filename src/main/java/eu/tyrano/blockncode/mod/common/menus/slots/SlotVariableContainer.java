package eu.tyrano.blockncode.mod.common.menus.slots;

import eu.tyrano.blockncode.mod.common.items.VariableItemBase;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class SlotVariableContainer extends SlotItemHandler {
    public SlotVariableContainer(IItemHandler itemHandler, int index, int xPosition, int yPosition) {
        super(itemHandler, index, xPosition, yPosition);
    }

    @Override
    public boolean mayPlace(ItemStack itemStack) {
        return itemStack.isEmpty() || (itemStack.getItem() instanceof VariableItemBase && itemStack.getTag() != null
                && !itemStack.getTag().getString("variable_name").equals("") && isRightType(itemStack));
    }

    public boolean isRightType(ItemStack itemStack) {
        return true;
    };
}
