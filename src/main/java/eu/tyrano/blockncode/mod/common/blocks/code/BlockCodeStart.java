package eu.tyrano.blockncode.mod.common.blocks.code;

import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityCodeStart;
import eu.tyrano.blockncode.mod.common.entities.robot.RuntimeDataProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.BlockHitResult;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

public class BlockCodeStart extends BlockCodeBlank implements EntityBlock {

    public static final BooleanProperty powered = BlockStateProperties.POWERED;

    public BlockCodeStart() {
        super();
        this.registerDefaultState(this.getStateDefinition().any().setValue(FACING, Direction.NORTH).setValue(powered, false));
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(FACING, powered);
    }

    @Override
    @ParametersAreNonnullByDefault
    public void neighborChanged(BlockState state, Level worldIn, BlockPos pos, Block blockIn, BlockPos fromPos,
                                boolean isMoving) {
        if (worldIn.isClientSide) {
            return;
        }

        if (worldIn.hasNeighborSignal(pos)) {
            activate(pos, worldIn, null);
        } else {
            worldIn.setBlockAndUpdate(pos, state.setValue(powered, false));
        }
    }

    public void activate(BlockPos pos, Level worldIn, RuntimeDataProvider initData) {
        BlockEntity blockEntity = worldIn.getBlockEntity(pos);
        if (blockEntity instanceof BlockEntityCodeStart codeStart) {
            codeStart.startRuntime(worldIn, pos, initData);
        }
    }

    @Override
    @ParametersAreNonnullByDefault
    public @NotNull InteractionResult use(BlockState p_60503_, Level world, BlockPos pos, Player player, InteractionHand p_60507_, BlockHitResult p_60508_) {
        BlockEntity blockEntity = world.getBlockEntity(pos);
        if (blockEntity instanceof BlockEntityCodeStart codeStart) {
            if (player.getMainHandItem().is(Items.STICK)) {
                codeStart.stopRuntime();
                return InteractionResult.sidedSuccess(world.isClientSide);
            }
        }
        return InteractionResult.FAIL;
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(@NotNull BlockPos pos, @NotNull BlockState state) {
        return new BlockEntityCodeStart(pos, state);
    }
}
