package eu.tyrano.blockncode.mod.common.blocks.code;

import eu.tyrano.blockncode.Block_n_code;
import eu.tyrano.blockncode.libraries.VoiceRecognition;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import eu.tyrano.blockncode.runtime.ExecutionResult;
import eu.tyrano.blockncode.runtime.Result;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;

public class BlockCodeSoundInput extends BlockCodeBlank {

    public ExecutionResult execute(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {

        if (Block_n_code.isVoiceChatLoaded()) {
            Player player = worldIn.getNearestPlayer(currentPos.getX(),
                    currentPos.getY(), currentPos.getZ(), -1, false);
            if (player != null) {
                runtime.addListeningPlayer(player.getUUID());
                VoiceRecognition.listen(player.getUUID());
                return new ExecutionResult(Result.ok, null, 0);
                // TODO: 12/03/2023 is -1 the range ?
            } else {
                return new ExecutionResult(Result.error, "No player found", 0);
            }
        } else {
            return new ExecutionResult(Result.error,
                    "Voice Chat is not installed", 0);
        }
    }
}
