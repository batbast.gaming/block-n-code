package eu.tyrano.blockncode.mod.common.blocks;

import eu.tyrano.blockncode.backend.variable.string.StringModification;
import eu.tyrano.blockncode.backend.variable.string.StringReader;
import eu.tyrano.blockncode.backend.variable.string.StringVariable;
import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueStringInput;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.network.NetworkHooks;

import javax.annotation.Nullable;
import java.util.UUID;

public class BlockValueStringInput extends BlockValueProvider {
    public BlockValueStringInput() {
        super();
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos,
                                 Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        if (!pLevel.isClientSide()) {
            BlockEntity entity = pLevel.getBlockEntity(pPos);
            if (entity instanceof BlockEntityValueStringInput) {
                NetworkHooks.openGui(((ServerPlayer) pPlayer), (BlockEntityValueStringInput) entity, pPos);
            } else {
                throw new IllegalStateException("Our Container provider is missing!");
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    public StringModification compileModification(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {

        BlockEntity blockEntity = worldIn.getBlockEntity(currentPos);
        if (blockEntity instanceof BlockEntityValueStringInput) {
            return new StringReader(new StringVariable(((BlockEntityValueStringInput) blockEntity).getValue()));
        } else {
            return null;
        }
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntities.valueStringInput.get().create(pos, state);
    }
}
