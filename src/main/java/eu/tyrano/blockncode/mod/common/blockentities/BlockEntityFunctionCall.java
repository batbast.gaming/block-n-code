package eu.tyrano.blockncode.mod.common.blockentities;

import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.menus.MenuFunctionCoordinates;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BlockEntityFunctionCall extends BlockEntity implements MenuProvider {

    private BlockPos pos;

    public BlockEntityFunctionCall(BlockPos pos, BlockState state) {
        super(ModBlockEntities.functionCall.get(), pos, state);
    }

    @Nullable
    @Override
    public AbstractContainerMenu createMenu(int p_39954_, Inventory p_39955_, Player p_39956_) {
        return new MenuFunctionCoordinates(p_39954_, p_39955_, getBlockPos());
    }

    public BlockPos getTargetPos() {
        return pos;
    }

    public void setValue(int x, int y, int z) {
        this.pos = new BlockPos(x, y, z);
        if (level.isLoaded(pos)) {
            this.setChanged();
            level.sendBlockUpdated(getBlockPos(), level.getBlockState(getBlockPos()),
                    level.getBlockState(getBlockPos()), 2);
        }
    }

    @Override
    public void onLoad() {
        super.onLoad();
    }

    @Override
    public void invalidateCaps() {
        super.invalidateCaps();
    }

    @Override
    protected void saveAdditional(@NotNull CompoundTag tag) {
        if (pos != null) {
            tag.putInt("destination_x", this.pos.getX());
            tag.putInt("destination_y", this.pos.getY());
            tag.putInt("destination_z", this.pos.getZ());
        }
        super.saveAdditional(tag);
    }

    @Override
    public void load(@NotNull CompoundTag nbt) {
        super.load(nbt);
        int boundX = nbt.getInt("destination_x");
        int boundY = nbt.getInt("destination_y");
        int boundZ = nbt.getInt("destination_z");
        this.pos = new BlockPos(boundX, boundY, boundZ);
    }

    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        if (pos != null) {
            tag.putInt("destination_x", this.pos.getX());
            tag.putInt("destination_y", this.pos.getY());
            tag.putInt("destination_z", this.pos.getZ());
        }
        return tag;
    }

    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        // Will get tag from #getUpdateTag
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public Component getDisplayName() {
        return new TextComponent("Function coordinates selection");
    }
}
