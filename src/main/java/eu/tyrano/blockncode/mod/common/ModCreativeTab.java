package eu.tyrano.blockncode.mod.common;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import org.jetbrains.annotations.NotNull;

public class ModCreativeTab {
    public static final CreativeModeTab BlockNCodeTab = new CreativeModeTab("block_n_code_tab") {
        @Override
        public @NotNull ItemStack makeIcon() {
            return new ItemStack(ModBlocks.codeStartBlock.get().asItem());
        }
    };
}