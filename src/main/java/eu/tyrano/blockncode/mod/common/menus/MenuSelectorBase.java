package eu.tyrano.blockncode.mod.common.menus;

import eu.tyrano.blockncode.mod.client.screens.buttontypes.*;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorBase;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorType;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.level.block.entity.BlockEntity;

public class MenuSelectorBase<E extends Enum<E> & ButtonType, B extends BlockEntitySelectorBase<?, E>> extends AbstractContainerMenu {
    private final B blockEntity;
    private final BlockPos blockPos;
    private final BlockEntitySelectorType type;

    public MenuSelectorBase(int pContainerId, Inventory inv, FriendlyByteBuf extraData, MenuType<MenuSelectorBase<E, B>> menu, BlockEntitySelectorType type) {
        this(pContainerId, inv, extraData.readBlockPos(), menu, type);
    }

    public MenuSelectorBase(int pContainerId, Inventory inventory, BlockPos blockPos, MenuType<MenuSelectorBase<E, B>> menu, BlockEntitySelectorType type) {
        super(menu, pContainerId);
        BlockEntity uncheckedBlockEntity = inventory.player.level.getBlockEntity(blockPos);

        if (uncheckedBlockEntity instanceof BlockEntitySelectorBase<?, ?> blockEntitySelectorBase) {
            this.blockEntity = ((B) blockEntitySelectorBase);
        } else {
            this.blockEntity = null;
        }
        this.type = type;

        this.blockPos = blockPos;
    }

    public BlockEntitySelectorType getSelectorType() {
        return this.type;
    }

    public E getBaseType() {
        switch (type) {
            case unaryBoolean -> {
                return (E) Enum.valueOf(ConditionUnaryBooleanType.class, blockEntity.getEnumValue().toString());
            }
            case binaryBoolean -> {
                return (E) Enum.valueOf(ConditionBinaryBooleanType.class, blockEntity.getEnumValue().toString());
            }
            case mathBoolean -> {
                return (E) Enum.valueOf(ConditionMathBooleanType.class, blockEntity.getEnumValue().toString());
            }
            case unaryMath -> {
                return (E) Enum.valueOf(ExpressionUnaryMathType.class, blockEntity.getEnumValue().toString());
            }
            case binaryMath -> {
                return (E) Enum.valueOf(ExpressionBinaryMathType.class, blockEntity.getEnumValue().toString());
            }
            case binaryString -> {
                return (E) Enum.valueOf(ModificationStringType.class, blockEntity.getEnumValue().toString());
            }
            case numberToString -> {
                return (E) Enum.valueOf(ModificationParseNumberType.class, blockEntity.getEnumValue().toString());
            }
            case booleanToString -> {
                return (E) Enum.valueOf(ModificationParseBooleanType.class, blockEntity.getEnumValue().toString());
            }
            case chatToString -> {
                return (E) Enum.valueOf(ModificationChatReadType.class, blockEntity.getEnumValue().toString());
            }
            case voiceToString -> {
                return (E) Enum.valueOf(ModificationPlayerListenType.class, blockEntity.getEnumValue().toString());
            }
            case stringToNumber -> {
                return (E) Enum.valueOf(ExpressionParseStringType.class, blockEntity.getEnumValue().toString());
            }
            case stringToBoolean -> {
                return (E) Enum.valueOf(ConditionParseStringType.class, blockEntity.getEnumValue().toString());
            }
            case stringBoolean -> {
                return (E) Enum.valueOf(ConditionStringBooleanType.class, blockEntity.getEnumValue().toString());
            }
            case robotInstruction -> {
                return (E) Enum.valueOf(RobotActionType.class, blockEntity.getEnumValue().toString());
            }
            case direction -> {
                return (E) Enum.valueOf(DirectionType.class, blockEntity.getEnumValue().toString());
            }
        }
        return null;
    }

    public BlockPos getBlockPos() {
        return blockPos;
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }
}
