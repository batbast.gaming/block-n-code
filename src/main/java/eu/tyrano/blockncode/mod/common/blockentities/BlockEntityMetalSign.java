package eu.tyrano.blockncode.mod.common.blockentities;

import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.SignBlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class BlockEntityMetalSign extends SignBlockEntity  {
    public BlockEntityMetalSign(BlockPos p_155700_, BlockState p_155701_) {
        super(p_155700_, p_155701_);
    }

    @Override
    public BlockEntityType<?> getType() {
        return ModBlockEntities.metalSign.get();
    }
}
