package eu.tyrano.blockncode.mod.common.network;

import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueStringInput;
import io.netty.buffer.ByteBuf;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.network.NetworkEvent;

import java.nio.charset.Charset;
import java.util.function.Supplier;

public class ValueStringMessage {

    private String value;
    private BlockPos blockPos;

    private final boolean failed;

    public ValueStringMessage(String value, BlockPos blockPos) {
        this.value = value;
        this.blockPos = blockPos;
        this.failed = false;
    }

    private ValueStringMessage(boolean failed) {
        this.failed = failed;
    }

    public static ValueStringMessage decode(ByteBuf buf) {
        try {
            int length = buf.readInt();
            String newValue = buf.readCharSequence(length, Charset.defaultCharset()).toString();
            int x = buf.readInt();
            int y = buf.readInt();
            int z = buf.readInt();
            return new ValueStringMessage(newValue, new BlockPos(x, y, z));
        } catch (IndexOutOfBoundsException e) {
            return new ValueStringMessage(true);
        }
    }

    public static void messageConsumer(ValueStringMessage msg, Supplier<NetworkEvent.Context> ctx) {
        handle(msg, ctx);
        ctx.get().setPacketHandled(true);
    }

    public static void encode(ValueStringMessage msg, FriendlyByteBuf buf) {
        buf.writeInt(msg.value.length());
        buf.writeCharSequence(msg.value, Charset.defaultCharset());
        buf.writeInt(msg.blockPos.getX());
        buf.writeInt(msg.blockPos.getY());
        buf.writeInt(msg.blockPos.getZ());
    }

    public static void handle(ValueStringMessage msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            if (!msg.failed) {
                if (ctx.get().getSender() != null && ctx.get().getSender().position().distanceTo(Vec3.atCenterOf(msg.blockPos)) <= 10) {
                    BlockEntity blockEntity = ctx.get().getSender().getLevel().getBlockEntity(msg.blockPos);
                    if (blockEntity instanceof BlockEntityValueStringInput valueStringInput) {
                        valueStringInput.setValue(msg.value);
                    }
                }
            }
        });
    }
}
