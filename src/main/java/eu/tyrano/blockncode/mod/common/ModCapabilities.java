package eu.tyrano.blockncode.mod.common;

import eu.tyrano.blockncode.mod.common.capabilities.WorldCodeCapability;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;


public class ModCapabilities {
    public static void register(RegisterCapabilitiesEvent event) {
        event.register(WorldCodeCapability.class);
    }
}
