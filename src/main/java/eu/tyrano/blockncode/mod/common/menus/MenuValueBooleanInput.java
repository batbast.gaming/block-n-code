package eu.tyrano.blockncode.mod.common.menus;

import eu.tyrano.blockncode.mod.common.ModMenus;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueBooleanInput;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;

public class MenuValueBooleanInput extends AbstractContainerMenu {

    private final BlockEntityValueBooleanInput blockEntity;
    private final BlockPos blockPos;

    public MenuValueBooleanInput(int pContainerId, Inventory inv, FriendlyByteBuf extraData) {
        this(pContainerId, inv, extraData.readBlockPos());
    }

    public MenuValueBooleanInput(int pContainerId, Inventory inv, BlockPos blockPos) {
        super(ModMenus.menuValueBooleanInput.get(), pContainerId);
        this.blockEntity = ((BlockEntityValueBooleanInput) inv.player.level.getBlockEntity(blockPos));
        this.blockPos = blockPos;
    }

    public boolean getBaseValue() {
        return blockEntity.getValue();
    }

    public BlockPos getBlockPos() {
        return blockPos;
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }
}
