package eu.tyrano.blockncode.mod.common.blocks.code;

import eu.tyrano.blockncode.runtime.CodeRuntime;
import eu.tyrano.blockncode.runtime.CompiledBlock;
import eu.tyrano.blockncode.runtime.ExecutionResult;
import eu.tyrano.blockncode.runtime.Result;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.material.Material;

public class BlockCodeBlank extends HorizontalDirectionalBlock {

    public BlockCodeBlank() {
        super(BlockBehaviour.Properties.of(Material.METAL)
                .strength(3.0F, 3.0F));
        this.registerDefaultState(this.getStateDefinition().any().setValue(FACING, Direction.NORTH));
    }

    @Override
    public BlockState rotate(BlockState state, LevelAccessor world, BlockPos pos, Rotation direction) {
        return super.rotate(state, world, pos, direction);
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext p_54779_) {
        return this.defaultBlockState().setValue(FACING, p_54779_.getHorizontalDirection().getOpposite());
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> p_54794_) {
        p_54794_.add(FACING);
    }

    public CompiledBlock getNext(Level worldIn, BlockPos currentPos) {
        BlockState blockState = worldIn.getBlockState(currentPos);
        BlockPos nextCodePos = currentPos.relative(blockState.getValue(FACING));
        Block nextCodeBlock = worldIn.getBlockState(nextCodePos).getBlock();

        if (nextCodeBlock instanceof BlockCodeBlank blockCodeBase) {
            return new CompiledBlock(blockCodeBase, nextCodePos, null);
        }
        return null;
    }

    public ExecutionResult execute(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {
        return new ExecutionResult(Result.ok, null, 0);
    }

    public CodeRuntime getOptionalFork(Level world, BlockPos currentPos, CodeRuntime runtime) {
        return null;
    }
}
