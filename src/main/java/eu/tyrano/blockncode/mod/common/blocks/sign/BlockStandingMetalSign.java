package eu.tyrano.blockncode.mod.common.blocks.sign;

import eu.tyrano.blockncode.mod.common.ModWoodTypes;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityMetalSign;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.StandingSignBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

public class BlockStandingMetalSign extends StandingSignBlock {
    public BlockStandingMetalSign() {
        super(BlockBehaviour.Properties.of(Material.METAL).strength(3.0F, 3.0F).noCollission(), ModWoodTypes.metal);
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return new BlockEntityMetalSign(pos, state);
    }
}
