package eu.tyrano.blockncode.mod.common.blocks.code;

import eu.tyrano.blockncode.backend.variable.string.ModificationResult;
import eu.tyrano.blockncode.backend.variable.string.StringModification;
import eu.tyrano.blockncode.mod.common.ModPacketHandler;
import eu.tyrano.blockncode.mod.common.blocks.BlockValueStringInput;
import eu.tyrano.blockncode.mod.common.network.TTSMessage;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import eu.tyrano.blockncode.runtime.ExecutionResult;
import eu.tyrano.blockncode.runtime.Result;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.network.PacketDistributor;

public class BlockCodeTTS extends BlockCodeBlank {

    public ExecutionResult execute(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {

        BlockPos valueProviderPos = currentPos.above();
        Block valueInput = worldIn.getBlockState(valueProviderPos).getBlock();

        if (valueInput instanceof BlockValueStringInput valueStringInput) {
            StringModification modification = valueStringInput.compileModification(worldIn, valueProviderPos, runtime);

            if (modification == null) {
                return new ExecutionResult(Result.error, "String generated at "
                                                         + currentPos.getX() + ","
                                                         + currentPos.getY() + ","
                                                         + currentPos.getZ() + " is null", 0);
            }

            ModificationResult modificationResult = modification.execute();

            if (modificationResult.error() == null) {

                ModPacketHandler.INSTANCE.send(PacketDistributor.NEAR
                                .with(PacketDistributor.TargetPoint.p(
                                        currentPos.getX(),
                                        currentPos.getY(),
                                        currentPos.getZ(),
                                        10,
                                        worldIn.dimension())),
                        new TTSMessage(modificationResult.value()));

                return new ExecutionResult(Result.ok, null, 0);
            } else {
                return new ExecutionResult(Result.error, modificationResult.error() + " at "
                                                         + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
            }
        } else {
            return new ExecutionResult(Result.error, "No valid value provider above "
                                                     + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
        }
    }
}
