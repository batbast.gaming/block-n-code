package eu.tyrano.blockncode.mod.common.blocks.code;

import eu.tyrano.blockncode.backend.variable.string.ModificationResult;
import eu.tyrano.blockncode.backend.variable.string.StringModification;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityMetalSign;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityMonitor;
import eu.tyrano.blockncode.mod.common.blocks.BlockValueStringInput;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import eu.tyrano.blockncode.runtime.ExecutionResult;
import eu.tyrano.blockncode.runtime.Result;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;

public class BlockCodeSignPrinter extends BlockCodeBlank {

    public BlockCodeSignPrinter() {
        super();
    }

    private BlockPos getSignPos(Level world, BlockPos currentPos) {

        BlockPos signPos;

        switch (world.getBlockState(currentPos).getValue(FACING)) {
            case EAST -> signPos = new BlockPos(currentPos.north());
            case WEST -> signPos = new BlockPos(currentPos.south());
            case NORTH -> signPos = new BlockPos(currentPos.west());
            case SOUTH -> signPos = new BlockPos(currentPos.east());
            default -> {
                return currentPos;
            }
        }

        return signPos;
    }

    public ExecutionResult execute(Level worldIn, BlockPos currentPos, CodeRuntime runtime) {

        BlockPos valueProviderPos = currentPos.above();
        Block valueInput = worldIn.getBlockState(valueProviderPos).getBlock();

        if (valueInput instanceof BlockValueStringInput valueStringInput) {
            StringModification modification = valueStringInput.compileModification(worldIn, valueProviderPos, runtime);

            if (modification == null) {
                return new ExecutionResult(Result.error, "Modification generated at "
                        + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ() + " is null", 0);
            }

            ModificationResult modificationResult = modification.execute();

            if (modificationResult.error() == null) {

                String result = modificationResult.value();

                BlockPos signPos = getSignPos(worldIn, currentPos);
                BlockEntity sign = worldIn.getBlockEntity(signPos);

                if (sign instanceof BlockEntityMetalSign metalSign) {

                    int maxCharPerLine = 15;
                    int characterIndex = 0;

                    int line = 0;
                    StringBuilder text = new StringBuilder();
                    for (char character : result.toCharArray()){
                        if (characterIndex < maxCharPerLine) {
                            text.append(character);
                            characterIndex++;
                        } else {
                            metalSign.setMessage(line, Component.nullToEmpty(text.toString()));
                            line++;
                            text = new StringBuilder();
                            characterIndex = 0;
                        }
                        if (line >= 4) {
                            break;
                        }
                    }
                    if (line < 4) {
                        metalSign.setMessage(line, Component.nullToEmpty(text.toString()));
                    }

                    worldIn.sendBlockUpdated(signPos,
                            worldIn.getBlockState(signPos),
                            worldIn.getBlockState(signPos), 2);

                    return new ExecutionResult(Result.ok, null, 0);
                } else if (sign instanceof BlockEntityMonitor monitor) {
                    monitor.setValue(result);

                    worldIn.sendBlockUpdated(signPos,
                            worldIn.getBlockState(signPos),
                            worldIn.getBlockState(signPos), 2);
                    return new ExecutionResult(Result.ok, null, 0);
                } else {
                    return new ExecutionResult(Result.error, "No sign found next to "
                            + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
                }
            } else {
                return new ExecutionResult(Result.error, modificationResult.error() + " at "
                        + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
            }
        } else {
            return new ExecutionResult(Result.error, "No valid value provider above "
                    + currentPos.getX() + "," + currentPos.getY() + "," + currentPos.getZ(), 0);
        }
    }
}
