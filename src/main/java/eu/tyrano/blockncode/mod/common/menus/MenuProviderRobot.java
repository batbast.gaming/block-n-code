package eu.tyrano.blockncode.mod.common.menus;

import eu.tyrano.blockncode.mod.common.entities.robot.RobotEntity;
import eu.tyrano.blockncode.mod.common.menus.robot.RobotData;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraftforge.network.NetworkHooks;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;

public class MenuProviderRobot implements MenuProvider {
    private final RobotData robotData;
    private final RobotEntity robot;
    private final boolean isShifting;

    public MenuProviderRobot(RobotData robotData, RobotEntity robot, boolean isShifting) {
        this.robotData = robotData;
        this.robot = robot;
        this.isShifting = isShifting;
    }

    @Nullable
    @Override
    public AbstractContainerMenu createMenu(int p_39954_, @NotNull Inventory p_39955_,
                                            @NotNull Player p_39956_) {
        if (isShifting) {
            return new MenuCodeRobot(p_39954_, this, robot);
        } else {
            return new MenuContainerRobot(p_39954_, this, robot);
        }
    }

    @Override
    public @NotNull Component getDisplayName() {
        return new TextComponent("Robot");
    }

    public RobotData getRobotData() {
        return robotData;
    }
}
