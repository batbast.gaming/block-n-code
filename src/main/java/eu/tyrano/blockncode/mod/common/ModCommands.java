package eu.tyrano.blockncode.mod.common;

import eu.tyrano.blockncode.Block_n_code;
import eu.tyrano.blockncode.mod.common.commands.CommandCodeRule;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = Block_n_code.MOD_ID)
public class ModCommands {
    static CommandCodeRule commandCodeRule;

    @SubscribeEvent
    public static void onCommandRegister(RegisterCommandsEvent event) {
        commandCodeRule = new CommandCodeRule(event.getDispatcher());
    }
}