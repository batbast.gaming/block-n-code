package eu.tyrano.blockncode.mod.common.capabilities;

import com.google.common.util.concurrent.AtomicDouble;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import java.util.concurrent.atomic.AtomicInteger;

public class WorldCodeCapabilityProvider implements ICapabilityProvider, INBTSerializable<CompoundTag> {
    public static Capability<WorldCodeCapability> codeCapability = CapabilityManager.get(new CapabilityToken<>() {
    });

    private WorldCodeCapability capability = null;

    private final LazyOptional<WorldCodeCapability> optional = LazyOptional.of(this::createWorldCapability);

    private WorldCodeCapability createWorldCapability() {
        if (this.capability == null) {
            this.capability = new WorldCodeCapability();
        }

        return this.capability;
    }

    @Override
    public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
        if (cap == codeCapability) {
            return optional.cast();
        }

        return LazyOptional.empty();
    }

    public static int getMaxCodeLength(Level world) {
        AtomicInteger value = new AtomicInteger();
        world.getCapability(codeCapability).ifPresent(worldCodeCapability ->
                value.set(worldCodeCapability.getMaxCodeLength()));
        return value.get();
    }

    public static double getMaxCodePerTick(Level world) {
        AtomicDouble value = new AtomicDouble();
        world.getCapability(codeCapability).ifPresent(worldCodeCapability ->
                value.set(worldCodeCapability.getMaxCodePerTick()));
        return value.get();
    }

    public static int getRobotTeleportRange(Level world) {
        AtomicInteger value = new AtomicInteger();
        world.getCapability(codeCapability).ifPresent(worldCodeCapability ->
                value.set(worldCodeCapability.getMaxRobotTeleportingRange()));
        return value.get();
    }

    public static double getRobotDelayMultiplier(Level world) {
        AtomicDouble value = new AtomicDouble();
        world.getCapability(codeCapability).ifPresent(worldCodeCapability ->
                value.set(worldCodeCapability.getRobotDelayMultiplier()));
        return value.get();
    }

    public static void setMaxCodeLength(Level world, int value) {
        world.getCapability(codeCapability).ifPresent(worldCodeCapability ->
                worldCodeCapability.setMaxCodeLength(value));
    }

    public static void setMaxCodePerTick(Level world, double value) {
        world.getCapability(codeCapability).ifPresent(worldCodeCapability ->
                worldCodeCapability.setMaxCodePerTick(value));
    }

    public static void setRobotTeleportRange(Level world, int value) {
        world.getCapability(codeCapability).ifPresent(worldCodeCapability ->
                worldCodeCapability.setMaxRobotTeleportingRange(value));
    }

    public static void setRobotDelayMultiplier(Level world, double value) {
        world.getCapability(codeCapability).ifPresent(worldCodeCapability ->
                worldCodeCapability.setRobotDelayMultiplier(value));
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag nbt = new CompoundTag();
        createWorldCapability().saveNBTData(nbt);
        return nbt;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        createWorldCapability().loadNBTData(nbt);
    }
}
