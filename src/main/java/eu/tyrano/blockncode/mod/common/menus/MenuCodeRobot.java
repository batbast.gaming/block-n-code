package eu.tyrano.blockncode.mod.common.menus;

import eu.tyrano.blockncode.mod.common.ModMenus;
import eu.tyrano.blockncode.mod.common.entities.robot.RobotEntity;
import eu.tyrano.blockncode.mod.common.menus.robot.RobotData;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import org.jetbrains.annotations.NotNull;

public class MenuCodeRobot extends AbstractContainerMenu {

    private RobotData robotData;
    private final RobotEntity robot;

    public MenuCodeRobot(int pContainerId, Inventory ignoredInventory, FriendlyByteBuf buf) {
        this(pContainerId, createInventory(), null);
        this.robotData = new RobotData(buf.readBlockPos());
    }

    public MenuCodeRobot(int pContainerId, MenuProviderRobot menuProviderRobot, RobotEntity robot) {
        super(ModMenus.menuCodeRobot.get(), pContainerId);
        this.robotData = menuProviderRobot.getRobotData();
        this.robot = robot;
    }

    private static MenuProviderRobot createInventory() {
        return new MenuProviderRobot(null, null, false);
    }

    public RobotData getRobotData() {
        return robotData;
    }

    @Override
    public boolean stillValid(@NotNull Player pPlayer) {
        return true;
    }

    public void setRobotData(int x, int y, int z) {
        if (robot != null) {
            robot.setRobotStandByPos(x, y, z);
        }
    }
}
