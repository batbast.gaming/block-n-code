package eu.tyrano.blockncode.mod.common.network;

import eu.tyrano.blockncode.mod.client.screens.buttontypes.ButtonType;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;

public abstract class SelectorMessageBase {

    protected BlockPos blockPos;
    protected ButtonType value;
    protected boolean ready = false;

    public void setBlockPos(BlockPos blockPos) {
        this.blockPos = blockPos;
    }

    public void setValue(ButtonType value) {
        this.value = value;
    }

    public void setReady() {
        this.ready = true;
    }

    public static void encode(SelectorMessageBase msg, FriendlyByteBuf buf) {
        if (msg.ready) {
            msg.finalizeEncode(buf);
        }
    }

    protected abstract void finalizeEncode(FriendlyByteBuf buf);
}
