package eu.tyrano.blockncode.mod.common.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.DoubleArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import eu.tyrano.blockncode.mod.common.TickRuntimeHandler;
import eu.tyrano.blockncode.mod.common.capabilities.WorldCodeCapabilityProvider;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import net.minecraft.ChatFormatting;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.*;

import java.util.HashMap;
import java.util.UUID;

public class CommandCodeRule {
    public CommandCodeRule(CommandDispatcher<CommandSourceStack> dispatcher) {
        final LiteralArgumentBuilder<CommandSourceStack> literalArgumentBuilder = Commands.literal("backend").requires((p_137750_) -> p_137750_.hasPermission(2));
        literalArgumentBuilder
                .then(Commands.literal("set-rule")
                        .then(Commands.literal("codeLength").then(Commands.argument("value", IntegerArgumentType.integer())
                                .executes((context) -> setMaxCodeLength(context.getSource(), IntegerArgumentType.getInteger(context, "value")))))
                        .then(Commands.literal("codePerTick").then(Commands.argument("value", DoubleArgumentType.doubleArg())
                                .executes((context) -> setMaxCodePerTick(context.getSource(), DoubleArgumentType.getDouble(context, "value")))))
                        .then(Commands.literal("robotRange").then(Commands.argument("value", IntegerArgumentType.integer())
                                .executes((context -> setRobotTeleportRange(context.getSource(), IntegerArgumentType.getInteger(context, "value"))))))
                        .then(Commands.literal("robotDelay").then(Commands.argument("value", DoubleArgumentType.doubleArg())
                                .executes((context) -> setRobotDelayMultiplier(context.getSource(), DoubleArgumentType.getDouble(context, "value"))))))
                .then(Commands.literal("get-rule")
                        .executes((context) -> getRules(context.getSource())))
                .then(Commands.literal("get-sources")
                        .executes((context) -> getSources(context.getSource())))
                .then(Commands.literal("clear-all")
                        .executes((context) -> clearAll(context.getSource())))
                .then(Commands.literal("clear-with-uuid").then(Commands.argument("UUID", StringArgumentType.word())
                        .executes((context) -> clearWithUUID(context.getSource(), StringArgumentType.getString(context, "UUID")))));
        dispatcher.register(literalArgumentBuilder);
    }

    private int getSources(CommandSourceStack source) {
        TextComponent builder = new TextComponent("");
        HashMap<UUID, CodeRuntime> runtimes = TickRuntimeHandler.instance.getRuntimes();

        Component arrow = new TextComponent(" -> ").setStyle(Style.EMPTY
                .withColor(ChatFormatting.GREEN)
                .withBold(true));

        for (UUID uuid : runtimes.keySet()) {
            CodeRuntime runtime = runtimes.get(uuid);
            BlockPos starterPos = runtime.getStarterPos();

            Component coordinateComponent = new TextComponent("[" + starterPos.getX() + " ," + starterPos.getY() + " ," + starterPos.getZ() + "]")
                    .setStyle(Style.EMPTY
                            .withHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent("Click to teleport")))
                            .withClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/teleport @s " + starterPos.getX() + " " + starterPos.getY() + " " + starterPos.getZ()))
                            .withColor(ChatFormatting.AQUA));

            Component copyComponent = new TextComponent("[" + uuid.toString() + "]")
                    .setStyle(Style.EMPTY
                            .withHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent("Click to copy UUID")))
                            .withClickEvent(new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD, uuid.toString()))
                            .withColor(ChatFormatting.AQUA)
                            .withBold(true));

            builder.append(coordinateComponent).append(arrow).append(copyComponent).append("\n");
        }
        source.sendSuccess(builder, false);
        return 0;
    }

    private int clearAll(CommandSourceStack source) {
        int count = TickRuntimeHandler.instance.clearRuntimes();
        source.sendSuccess(new TextComponent(count + " runtime(s) were cleared"), true);
        return 0;
    }

    private int clearWithUUID(CommandSourceStack source, String uuid) {
        TickRuntimeHandler.instance.removeRuntime(UUID.fromString(uuid));
        source.sendSuccess(new TextComponent("runtime with UUID " + uuid + " was cleared"), true);
        return 0;
    }

    private int getRules(CommandSourceStack source) {
        source.sendSuccess(new TextComponent(
                "Max backend length for DIM " + getDimName(source) + " : " + WorldCodeCapabilityProvider.getMaxCodeLength(source.getLevel())
                        + "\nMax backend per tick for DIM " + getDimName(source) + " : " + WorldCodeCapabilityProvider.getMaxCodePerTick(source.getLevel())
                        + "\nMax robot teleportation range for DIM " + getDimName(source) + " : " + WorldCodeCapabilityProvider.getRobotTeleportRange(source.getLevel())
                        + "\nRobot delay multiplier for DIM " + getDimName(source) + " : " + WorldCodeCapabilityProvider.getRobotDelayMultiplier(source.getLevel())), false);
        return 0;
    }

    private int setMaxCodeLength(CommandSourceStack source, int value) {
        source.sendSuccess(new TextComponent("Max backend length for DIM " + getDimName(source) + " set to " + value), true);
        WorldCodeCapabilityProvider.setMaxCodeLength(source.getLevel(), value);
        return 0;
    }

    private int setMaxCodePerTick(CommandSourceStack source, double value) {
        source.sendSuccess(new TextComponent("Max backend per tick for DIM " + getDimName(source) + " set to " + value), true);
        WorldCodeCapabilityProvider.setMaxCodePerTick(source.getLevel(), value);
        return 0;
    }

    private int setRobotTeleportRange(CommandSourceStack source, int value) {
        source.sendSuccess(new TextComponent("Max robot teleport range for DIM " + getDimName(source) + " set to " + value), true);
        WorldCodeCapabilityProvider.setRobotTeleportRange(source.getLevel(), value);
        return 0;
    }

    private int setRobotDelayMultiplier(CommandSourceStack source, double value) {
        source.sendSuccess(new TextComponent("Robot delay multiplier for DIM " + getDimName(source) + " set to " + value), true);
        WorldCodeCapabilityProvider.setRobotDelayMultiplier(source.getLevel(), value);
        return 0;
    }

    private String getDimName(CommandSourceStack source) {
        return source.getLevel().dimension().getRegistryName().toString();
    }
}