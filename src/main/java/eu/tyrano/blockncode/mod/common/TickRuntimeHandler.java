package eu.tyrano.blockncode.mod.common;

import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityMonitor;
import eu.tyrano.blockncode.mod.common.capabilities.WorldCodeCapabilityProvider;
import eu.tyrano.blockncode.mod.common.entities.robot.RuntimeDataProvider;
import eu.tyrano.blockncode.runtime.CodeRuntime;
import eu.tyrano.blockncode.runtime.Result;
import eu.tyrano.blockncode.runtime.RuntimeResult;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class TickRuntimeHandler {

    public static TickRuntimeHandler instance;

    public int tick = 0;
    private final HashMap<UUID, CodeRuntime> runtimes = new HashMap<>();

    @SubscribeEvent
    public void tick(TickEvent.ServerTickEvent event) {
        ArrayList<UUID> toRemoveUUID = new ArrayList<>();
        for (UUID uuid : runtimes.keySet()) {
            CodeRuntime runtime = runtimes.get(uuid);
            RuntimeResult runtimeResult = runtime.runBunchOfBlocksBase(this.tick++);
            if (runtimeResult.executionResult().result() == Result.error) {
                BlockEntity monitorBlock = runtime.getWorld().getBlockEntity(runtime.getStarterPos().below());
                if (monitorBlock instanceof BlockEntityMonitor monitor) {
                    monitor.setValue(runtimeResult.executionResult().error());
                }
                toRemoveUUID.add(uuid);
            } else if (runtimeResult.executionResult().result() == Result.finished
                       || runtimeResult.executionResult().result() == Result.terminated) {
                toRemoveUUID.add(uuid);
            }
        }

        for (UUID uuid : toRemoveUUID) {
            removeRuntime(uuid);
        }
    }

    public HashMap<UUID, CodeRuntime> getRuntimes() {
        return runtimes;
    }

    public void removeRuntime(UUID runtimeUUID) {
        CodeRuntime runtime = runtimes.get(runtimeUUID);
        if (runtime != null) {
            runtime.shutdown();
            runtimes.remove(runtimeUUID);
        }
    }

    public int clearRuntimes() {
        for (UUID uuid : runtimes.keySet()) {
            CodeRuntime runtime = runtimes.get(uuid);
            runtime.shutdown();
        }
        int count = runtimes.size();
        runtimes.clear();
        return count;
    }

    public interface OnRuntimeFinished {
        void action();
    }

    public UUID registerRuntime(Level world, BlockPos pos, OnRuntimeFinished onRuntimeFinished,
                                RuntimeDataProvider initData) {
        UUID runtimeUUID = UUID.randomUUID();
        CodeRuntime runtime;
        runtime = new CodeRuntime(world, pos, WorldCodeCapabilityProvider.getMaxCodeLength(world),
                WorldCodeCapabilityProvider.getMaxCodePerTick(world), onRuntimeFinished, initData);
        runtime.compile();
        runtimes.put(runtimeUUID, runtime);
        return runtimeUUID;
    }
}
