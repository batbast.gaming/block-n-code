package eu.tyrano.blockncode.mod.common;

import eu.tyrano.blockncode.Block_n_code;
import eu.tyrano.blockncode.mod.common.network.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.network.NetworkRegistry;
import net.minecraftforge.network.simple.SimpleChannel;

public class ModPacketHandler {
    private static int id;

    private static final String PROTOCOL_VERSION = "1.8";
    public static final SimpleChannel INSTANCE = NetworkRegistry.ChannelBuilder
            .named(new ResourceLocation(Block_n_code.MOD_ID, "main_channel"))
            .networkProtocolVersion(() -> PROTOCOL_VERSION)
            .clientAcceptedVersions(PROTOCOL_VERSION::equals)
            .serverAcceptedVersions(PROTOCOL_VERSION::equals)
            .simpleChannel();

    public static void init() {
        INSTANCE.registerMessage(id++, ItemRenameMessage.class, ItemRenameMessage::encode, ItemRenameMessage::decode, ItemRenameMessage::messageConsumer);

        INSTANCE.registerMessage(id++, ConditionBooleanMessage.class, ConditionBooleanMessage::encode, ConditionBooleanMessage::decode, ConditionBooleanMessage::messageConsumer);
        INSTANCE.registerMessage(id++, ConditionMathMessage.class, ConditionMathMessage::encode, ConditionMathMessage::decode, ConditionMathMessage::messageConsumer);
        INSTANCE.registerMessage(id++, ConditionStringMessage.class, ConditionStringMessage::encode, ConditionStringMessage::decode, ConditionStringMessage::messageConsumer);
        INSTANCE.registerMessage(id++, ExpressionMessage.class, ExpressionMessage::encode, ExpressionMessage::decode, ExpressionMessage::messageConsumer);
        INSTANCE.registerMessage(id++, ModificationMessage.class, ModificationMessage::encode, ModificationMessage::decode, ModificationMessage::messageConsumer);
        INSTANCE.registerMessage(id++, RobotActionMessage.class, RobotActionMessage::encode, RobotActionMessage::decode, RobotActionMessage::messageConsumer);
        INSTANCE.registerMessage(id++, DirectionMessage.class, DirectionMessage::encode, DirectionMessage::decode, DirectionMessage::messageConsumer);

        INSTANCE.registerMessage(id++, ValueBooleanMessage.class, ValueBooleanMessage::encode, ValueBooleanMessage::decode, ValueBooleanMessage::messageConsumer);
        INSTANCE.registerMessage(id++, ValueNumberMessage.class, ValueNumberMessage::encode, ValueNumberMessage::decode, ValueNumberMessage::messageConsumer);
        INSTANCE.registerMessage(id++, ValueComplexMessage.class, ValueComplexMessage::encode, ValueComplexMessage::decode, ValueComplexMessage::messageConsumer);
        INSTANCE.registerMessage(id++, ValueStringMessage.class, ValueStringMessage::encode, ValueStringMessage::decode, ValueStringMessage::messageConsumer);

        INSTANCE.registerMessage(id++, FunctionCoordinatesMessage.class, FunctionCoordinatesMessage::encode, FunctionCoordinatesMessage::decode, FunctionCoordinatesMessage::messageConsumer);
        INSTANCE.registerMessage(id++, RobotCoordinatesMessage.class, RobotCoordinatesMessage::encode, RobotCoordinatesMessage::decode, RobotCoordinatesMessage::messageConsumer);
        INSTANCE.registerMessage(id++, RobotContainerMessage.class, RobotContainerMessage::encode, RobotContainerMessage::decode, RobotContainerMessage::messageConsumer);

        INSTANCE.registerMessage(id++, TTSMessage.class, TTSMessage::encode, TTSMessage::decode, TTSMessage::messageConsumer);
    }
}
