package eu.tyrano.blockncode.mod.common.entities.robot;

import net.minecraft.core.Direction;

public record RobotControl(RobotAction action, Direction direction, int data) {
}