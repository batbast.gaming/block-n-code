package eu.tyrano.blockncode.mod.common;

import eu.tyrano.blockncode.Block_n_code;
import eu.tyrano.blockncode.backend.variable.bool.ConditionBooleanType;
import eu.tyrano.blockncode.backend.variable.bool.ConditionMathType;
import eu.tyrano.blockncode.backend.variable.bool.ConditionStringType;
import eu.tyrano.blockncode.backend.variable.number.OperationType;
import eu.tyrano.blockncode.backend.variable.string.ModificationType;
import eu.tyrano.blockncode.mod.client.screens.buttontypes.*;
import eu.tyrano.blockncode.mod.common.blockentities.*;
import eu.tyrano.blockncode.mod.common.entities.robot.RobotAction;
import net.minecraft.core.Direction;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ModBlockEntities {
    public static final DeferredRegister<BlockEntityType<?>> TILE_ENTITIES =
            DeferredRegister.create(ForgeRegistries.BLOCK_ENTITIES, Block_n_code.MOD_ID);


    public static final RegistryObject<BlockEntityType<BlockEntityVariableWriter>> variableWriter =
            TILE_ENTITIES.register("variable_writer",
                    () -> BlockEntityType.Builder.of(BlockEntityVariableWriter::new, ModBlocks.variableWriterBlock.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntityNumberVariableReader>> variableNumberReader =
            TILE_ENTITIES.register("variable_number_reader",
                    () -> BlockEntityType.Builder.of(BlockEntityNumberVariableReader::new, ModBlocks.variableNumberReaderBlock.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntityBooleanVariableReader>> variableBooleanReader =
            TILE_ENTITIES.register("variable_boolean_reader",
                    () -> BlockEntityType.Builder.of(BlockEntityBooleanVariableReader::new, ModBlocks.variableBooleanReaderBlock.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntityStringVariableReader>> variableStringReader =
            TILE_ENTITIES.register("variable_string_reader",
                    () -> BlockEntityType.Builder.of(BlockEntityStringVariableReader::new, ModBlocks.variableStringReaderBlock.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntityValueBooleanInput>> valueBooleanInput =
            TILE_ENTITIES.register("value_boolean_input",
                    () -> BlockEntityType.Builder.of(BlockEntityValueBooleanInput::new, ModBlocks.valueBooleanInputBlock.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntityValueNumberInput>> valueNumberInput =
            TILE_ENTITIES.register("value_number_input",
                    () -> BlockEntityType.Builder.of(BlockEntityValueNumberInput::new, ModBlocks.valueNumberInputBlock.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntityValueComplexInput>> valueComplexInput =
            TILE_ENTITIES.register("value_complex_input",
                    () -> BlockEntityType.Builder.of(BlockEntityValueComplexInput::new, ModBlocks.valueComplexInputBlock.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntityValueStringInput>> valueStringInput =
            TILE_ENTITIES.register("value_string_input",
                    () -> BlockEntityType.Builder.of(BlockEntityValueStringInput::new, ModBlocks.valueStringInputBlock.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<ConditionBooleanType, ConditionUnaryBooleanType>>> conditionUnaryBoolean =
            TILE_ENTITIES.register("condition_unary_boolean",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, ConditionBooleanType.no,
                                            ModBlockEntities.conditionUnaryBoolean.get(), ModMenus.menuConditionUnaryBoolean.get(), BlockEntitySelectorType.unaryBoolean),
                            ModBlocks.conditionUnaryBoolean.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<ConditionBooleanType, ConditionBinaryBooleanType>>> conditionBinaryBoolean =
            TILE_ENTITIES.register("condition_binary_boolean",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, ConditionBooleanType.and,
                                            ModBlockEntities.conditionBinaryBoolean.get(), ModMenus.menuConditionBinaryBoolean.get(), BlockEntitySelectorType.binaryBoolean),
                            ModBlocks.conditionBinaryBoolean.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<ConditionMathType, ConditionMathBooleanType>>> conditionMathBoolean =
            TILE_ENTITIES.register("condition_math_boolean",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, ConditionMathType.equal,
                                            ModBlockEntities.conditionMathBoolean.get(), ModMenus.menuConditionMathBoolean.get(), BlockEntitySelectorType.mathBoolean),
                            ModBlocks.conditionMathBoolean.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<OperationType, ExpressionUnaryMathType>>> expressionMathUnary =
            TILE_ENTITIES.register("expression_unary_math",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, OperationType.absolute,
                                            ModBlockEntities.expressionMathUnary.get(), ModMenus.menuOperationUnaryMath.get(), BlockEntitySelectorType.unaryMath),
                            ModBlocks.expressionUnaryMath.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<RobotAction, RobotActionType>>> robotInstruction =
            TILE_ENTITIES.register("robot_instruction",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, RobotAction.waiting,
                                            ModBlockEntities.robotInstruction.get(), ModMenus.menuRobotInstruction.get(), BlockEntitySelectorType.robotInstruction),
                            ModBlocks.robotInstructionValueBlock.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<Direction, DirectionType>>> directionValue =
            TILE_ENTITIES.register("direction_value",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, Direction.NORTH,
                                            ModBlockEntities.directionValue.get(), ModMenus.menuDirection.get(), BlockEntitySelectorType.direction),
                            ModBlocks.directionValueBlock.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<OperationType, ExpressionBinaryMathType>>> expressionMathBinary =
            TILE_ENTITIES.register("expression_binary_math",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, OperationType.add,
                                            ModBlockEntities.expressionMathBinary.get(), ModMenus.menuOperationBinaryMath.get(), BlockEntitySelectorType.binaryMath),
                            ModBlocks.expressionBinaryMath.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<ModificationType, ModificationStringType>>> modificationStringBinary =
            TILE_ENTITIES.register("modification_binary_string",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, ModificationType.append,
                                            ModBlockEntities.modificationStringBinary.get(), ModMenus.menuModificationBinaryString.get(), BlockEntitySelectorType.binaryString),
                            ModBlocks.modificationBinaryString.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<ModificationType, ModificationParseBooleanType>>> modificationStringBoolean =
            TILE_ENTITIES.register("modification_boolean_string",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, ModificationType.parseBoolean,
                                            ModBlockEntities.modificationStringBoolean.get(), ModMenus.menuModificationBooleanString.get(), BlockEntitySelectorType.booleanToString),
                            ModBlocks.modificationBooleanParser.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<ModificationType, ModificationParseNumberType>>> modificationStringNumber =
            TILE_ENTITIES.register("modification_number_string",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, ModificationType.parseNumber,
                                            ModBlockEntities.modificationStringNumber.get(), ModMenus.menuModificationNumberString.get(), BlockEntitySelectorType.numberToString),
                            ModBlocks.modificationNumberParser.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<ModificationType, ModificationChatReadType>>> modificationChatRead =
            TILE_ENTITIES.register("modification_chat_read",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, ModificationType.readChat,
                                            ModBlockEntities.modificationChatRead.get(), ModMenus.menuModificationChatRead.get(), BlockEntitySelectorType.chatToString),
                            ModBlocks.modificationChatRead.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<ModificationType, ModificationPlayerListenType>>> modificationPlayerListen =
            TILE_ENTITIES.register("modification_player_listen",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, ModificationType.playerListen,
                                            ModBlockEntities.modificationPlayerListen.get(), ModMenus.menuModificationPlayerListen.get(), BlockEntitySelectorType.voiceToString),
                            ModBlocks.modificationPlayerListen.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<OperationType, ExpressionParseStringType>>> operationStringToNumber =
            TILE_ENTITIES.register("operation_string_to_number",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, OperationType.parseString,
                                            ModBlockEntities.operationStringToNumber.get(), ModMenus.menuOperationStringToNumber.get(), BlockEntitySelectorType.stringToNumber),
                            ModBlocks.operationStringToNumber.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<ConditionBooleanType, ConditionParseStringType>>> conditionStringToBoolean =
            TILE_ENTITIES.register("condition_string_to_boolean",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, ConditionBooleanType.parseString,
                                            ModBlockEntities.conditionStringToBoolean.get(), ModMenus.menuConditionStringToBoolean.get(), BlockEntitySelectorType.stringToBoolean),
                            ModBlocks.conditionStringToBoolean.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntitySelectorBase<ConditionStringType, ConditionStringBooleanType>>> conditionStringBoolean =
            TILE_ENTITIES.register("condition_string_boolean",
                    () -> BlockEntityType.Builder.of((pos, state) ->
                                    new BlockEntitySelectorBase<>(pos, state, ConditionStringType.isSame,
                                            ModBlockEntities.conditionStringBoolean.get(), ModMenus.menuConditionStringBoolean.get(), BlockEntitySelectorType.stringBoolean),
                            ModBlocks.conditionStringBoolean.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntityMonitor>> monitor =
            TILE_ENTITIES.register("monitor",
                    () -> BlockEntityType.Builder.of(BlockEntityMonitor::new, ModBlocks.monitorBlock.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntityFunctionCall>> functionCall =
            TILE_ENTITIES.register("block_call",
                    () -> BlockEntityType.Builder.of(BlockEntityFunctionCall::new, ModBlocks.functionCall.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntityMetalSign>> metalSign =
            TILE_ENTITIES.register("metal_sign",
                    () -> BlockEntityType.Builder.of(BlockEntityMetalSign::new, ModBlocks.wallIronSign.get(), ModBlocks.standingIronSign.get()).build(null));


    public static final RegistryObject<BlockEntityType<BlockEntityCodeStart>> codeStart =
            TILE_ENTITIES.register("code_start",
                    () -> BlockEntityType.Builder.of(BlockEntityCodeStart::new, ModBlocks.codeStartBlock.get()).build(null));


    public static void register(IEventBus eventBus) {
        TILE_ENTITIES.register(eventBus);
    }
}
