package eu.tyrano.blockncode.mod.common.blockentities;

public enum BlockEntitySelectorType {
    unaryBoolean,
    binaryBoolean,
    mathBoolean,
    unaryMath,
    binaryMath,
    binaryString,
    numberToString,
    chatToString,
    booleanToString,
    stringToNumber,
    stringToBoolean,
    stringBoolean,
    robotInstruction,
    voiceToString, direction
}
