package eu.tyrano.blockncode.mod.common.entities.robot;

import eu.tyrano.blockncode.backend.variable.Variable;
import eu.tyrano.blockncode.backend.variable.bool.BooleanVariable;
import eu.tyrano.blockncode.backend.variable.number.ComplexVariable;
import eu.tyrano.blockncode.backend.variable.number.NumberVariable;
import eu.tyrano.blockncode.backend.variable.string.StringVariable;
import eu.tyrano.blockncode.mod.common.ModItems;
import eu.tyrano.blockncode.mod.common.blocks.code.BlockCodeStart;
import eu.tyrano.blockncode.mod.common.capabilities.WorldCodeCapabilityProvider;
import eu.tyrano.blockncode.mod.common.menus.MenuProviderRobot;
import eu.tyrano.blockncode.mod.common.menus.robot.RobotData;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.BlockSnapshot;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.network.NetworkHooks;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class RobotEntity extends Mob {

    private RobotActionData actionData = RobotActionData.standby(RobotAction.standby);
    private final ArrayList<Pair<String, Variable>> storedVariables = new ArrayList<>(10);
    private static final EntityDataAccessor<BlockPos> robotStandByPos = SynchedEntityData
            .defineId(RobotEntity.class, EntityDataSerializers.BLOCK_POS);
    private static final EntityDataAccessor<BlockPos> robotContainerPos = SynchedEntityData
            .defineId(RobotEntity.class, EntityDataSerializers.BLOCK_POS);

    public RobotEntity(EntityType<? extends Mob> p_27508_, Level p_27509_) {
        super(p_27508_, p_27509_);
    }

    private void teleport(BlockPos pos) {
        super.teleportTo(pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5);
        this.playSound(SoundEvents.ENDERMAN_TELEPORT, 1.0F, 1.0F);
        ((ServerLevel) this.level).sendParticles(ParticleTypes.CAMPFIRE_COSY_SMOKE,
                this.xo, this.yo, this.zo,
                0,
                this.xo, this.yo, this.zo, 0);
    }

    public void setActionData(RobotControl robotControl) {
        RobotAction previousAction = actionData.previousAction();
        int maxTeleportRange = WorldCodeCapabilityProvider.getRobotTeleportRange(this.level);
        switch (robotControl.action()) {
            case waiting -> {
                int minimumDelay = (int) (20 * WorldCodeCapabilityProvider.getRobotDelayMultiplier(this.level));
                if (robotControl.data() < minimumDelay) {
                    this.actionData = RobotActionData.waiting(
                            minimumDelay, previousAction);
                }
                this.actionData = RobotActionData.waiting(
                        robotControl.data(), previousAction);
            }
            case moving -> this.actionData = RobotActionData.moving(
                    new BlockPos(this.position()).relative(robotControl.direction(),
                            Math.max(Math.min(robotControl.data(), maxTeleportRange), 0)));
            case placing -> this.actionData = RobotActionData.placing(
                    new BlockPos(this.position()).relative(robotControl.direction()), robotControl.data());
            case breaking -> this.actionData = RobotActionData.breaking(
                    new BlockPos(this.position()).relative(robotControl.direction()), robotControl.data());
            case disassemble -> this.actionData = RobotActionData.disassemble();
        }
    }

    @Override
    public void tick() {
        super.tick();
        int delay = (int) (20 * WorldCodeCapabilityProvider.getRobotDelayMultiplier(this.level));
        if (delay <= 0) {
            delay = 1;
        }
        if (this.tickCount % delay == 0) {
            if (!(this.level instanceof ServerLevel) || this.isDeadOrDying()) {
                return;
            }
            RobotAction action = actionData.action();
            switch (actionData.action()) {
                case moving -> {
                    if (level.isLoaded(actionData.pos())) {
                        if (level.getBlockState(actionData.pos()).isAir()) {
                            teleport(actionData.pos());
                        }
                    }
                    actionData = RobotActionData.waiting(this.tickCount + delay, action);
                }
                case standby -> {
                    BlockPos pos = new BlockPos(
                            this.entityData.get(robotStandByPos).getX(),
                            this.entityData.get(robotStandByPos).getY(),
                            this.entityData.get(robotStandByPos).getZ());
                    if (level.isLoaded(pos)) {
                        BlockState blockState = level.getBlockState(pos);
                        if (blockState.getBlock() instanceof BlockCodeStart blockCodeStart) {
                            HashMap<String, Variable> variableHashMap = new HashMap<>();
                            storedVariables.forEach((pair) -> variableHashMap.put(pair.getKey(), pair.getValue()));
                            blockCodeStart.activate(pos, level, new RuntimeRobotData(
                                    new BlockPos(this.position()),
                                    actionData.previousAction(),
                                    level.getBlockState(new BlockPos(this.position()).above()),
                                    level.getBlockState(new BlockPos(this.position()).below()),
                                    variableHashMap,
                                    this
                            ));
                        }
                    }
                }
                case waiting -> {
                    if (actionData.data() >= this.tickCount) {
                        actionData = RobotActionData.standby(RobotAction.waiting);
                    }
                }
                case placing -> {
                    BlockPos chestPos = this.entityData.get(robotContainerPos);
                    if (level.isLoaded(actionData.pos()) && level.isLoaded(chestPos)) {
                        if (level.getBlockState(actionData.pos()).isAir()) {
                            if (level.getBlockEntity(chestPos) instanceof ChestBlockEntity chest) {
                                int slot = actionData.data();
                                Optional<IItemHandler> capability = chest.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).resolve();
                                if (capability.isPresent()) {
                                    IItemHandler itemHandler = capability.get();
                                    int maxSlots = itemHandler.getSlots();
                                    if (slot >= 0 && slot < maxSlots) {
                                        ItemStack stack = itemHandler.getStackInSlot(slot);
                                        if (!stack.isEmpty() && stack.getItem() instanceof BlockItem blockItem) {
                                            itemHandler.extractItem(slot, 1, false);

                                            if (!ForgeEventFactory.onBlockPlace(this, BlockSnapshot.create(level.dimension(), level, actionData.pos()), net.minecraft.core.Direction.UP)) {
                                                this.level.setBlock(this.actionData.pos(), blockItem.getBlock().defaultBlockState(), 3);
                                                level.gameEvent(this, GameEvent.BLOCK_PLACE, this.actionData.pos());
                                            }
                                        }
                                    } else {
                                        for (int i = 0; i < maxSlots; i++) {

                                            ItemStack stack = itemHandler.getStackInSlot(i);

                                            if (!stack.isEmpty() && stack.getItem() instanceof BlockItem blockItem) {
                                                itemHandler.extractItem(i, 1, false);
                                                if (!ForgeEventFactory.onBlockPlace(this, BlockSnapshot.create(level.dimension(), level, actionData.pos()), net.minecraft.core.Direction.UP)) {
                                                    this.level.setBlock(this.actionData.pos(), blockItem.getBlock().defaultBlockState(), 3);
                                                    level.gameEvent(this, GameEvent.BLOCK_PLACE, this.actionData.pos());
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    actionData = RobotActionData.waiting(this.tickCount + delay, action);
                }
                case breaking -> {
                    BlockPos chestPos = this.entityData.get(robotContainerPos);
                    if (level.isLoaded(actionData.pos()) && level.isLoaded(chestPos)) {
                        BlockState toBreak = this.level.getBlockState(actionData.pos());
                        if (!toBreak.isAir() && toBreak.getDestroySpeed(this.level, actionData.pos()) != -1) {
                            if (level.getBlockEntity(chestPos) instanceof ChestBlockEntity chest) {
                                Optional<IItemHandler> capability = chest.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).resolve();
                                if (capability.isPresent()) {

                                    IItemHandler itemHandler = capability.get();

                                    ItemStack tool = itemHandler.getStackInSlot(0);

                                    LootContext.Builder lootContextBuilder =
                                            new LootContext.Builder((ServerLevel) this.level)
                                                    .withRandom(this.random)
                                                    .withParameter(LootContextParams.THIS_ENTITY, this)
                                                    .withParameter(LootContextParams.ORIGIN,
                                                            new Vec3(
                                                                    actionData.pos().getX(),
                                                                    actionData.pos().getY(),
                                                                    actionData.pos().getZ()))
                                                    .withParameter(LootContextParams.TOOL, tool);

                                    if (tool.hurt(1, this.random, null)) {
                                        itemHandler.extractItem(0, 1, false);
                                    }

                                    List<ItemStack> lootItems = toBreak.getDrops(lootContextBuilder);

                                    int maxSlots = itemHandler.getSlots();

                                    ItemStack[] toInsert = new ItemStack[maxSlots - 1];

                                    for (int i = 0; i < maxSlots; i++) {
                                        ItemStack toRemove = null;

                                        for (ItemStack drop : lootItems) {
                                            ItemStack remain = itemHandler.insertItem(i, drop, true);
                                            if (remain.isEmpty()) {
                                                toInsert[i] = drop.copy();
                                                toRemove = drop;
                                                break;
                                            } else if (remain.getCount() != drop.getCount()) {
                                                drop.shrink(drop.getCount() - remain.getCount());
                                                toInsert[i] = drop.copy();
                                                break;
                                            }
                                        }
                                        if (toRemove != null) {
                                            lootItems.remove(toRemove);
                                        }
                                    }

                                    if (ForgeEventFactory.onEntityDestroyBlock(this, actionData.pos(), toBreak)) {
                                        this.level.destroyBlock(actionData.pos(), false, this, 512);
                                        level.gameEvent(this, GameEvent.BLOCK_DESTROY, actionData.pos());
                                        for (int i = 0; i < toInsert.length; i++) {
                                            if (toInsert[i] != null) {
                                                itemHandler.insertItem(i, toInsert[i], false);
                                            }
                                        }
                                        for (ItemStack itemStack : lootItems) {
                                            this.spawnAtLocation(itemStack);
                                        }
                                        ((ServerLevel) this.level).sendParticles(ParticleTypes.EXPLOSION,
                                                this.xo, this.yo, this.zo,
                                                0,
                                                this.xo, this.yo, this.zo, 0);
                                    }
                                }
                            }
                        }
                    }
                    actionData = RobotActionData.waiting(this.tickCount + delay, action);
                }
                case disassemble -> {
                    this.setHealth(-1f);
                    this.die(DamageSource.GENERIC);
                    ((ServerLevel) this.level).sendParticles(ParticleTypes.ANGRY_VILLAGER,
                            this.xo, this.yo, this.zo,
                            0,
                            this.xo, this.yo, this.zo, 0);
                }
            }
        }
    }

    private void drop() {
        ItemStack item = ModItems.robotBottle.get().getDefaultInstance();
        CompoundTag tag = new CompoundTag();
        addRobotTag(tag, this.entityData.get(robotStandByPos), this.entityData.get(robotContainerPos));

        item.addTagElement("EntityTag", tag);
        this.spawnAtLocation(item);
    }

    @Override
    public void die(@NotNull DamageSource source) {
        if (net.minecraftforge.common.ForgeHooks.onLivingDeath(this, source)) {
            return;
        }
        this.drop();
        this.playSound(SoundEvents.IRON_GOLEM_REPAIR, 1.0F, 1.0F);
        actionData = RobotActionData.waiting(Integer.MAX_VALUE, this.actionData.action());
        this.level.broadcastEntityEvent(this, (byte) 3);
    }

    @Override
    public boolean hurt(@NotNull DamageSource source, float damages) {
        if (this.level.isClientSide || this.isDeadOrDying()) {
            return false;
        } else if (source.getEntity() instanceof ServerPlayer || source.isBypassInvul()) {
            this.setHealth(-1f);
            this.die(source);
            return true;
        } else {
            return false;
        }
    }

    protected void defineSynchedData() {
        super.defineSynchedData();
        this.entityData.define(robotStandByPos, BlockPos.ZERO);
        this.entityData.define(robotContainerPos, BlockPos.ZERO);
    }

    @Override
    protected @NotNull InteractionResult mobInteract(@NotNull Player player,
                                                     @NotNull InteractionHand hand) {
        if (!this.level.isClientSide && player instanceof ServerPlayer serverPlayer) {

            boolean isShifting = serverPlayer.isShiftKeyDown();

            RobotData robotData = new RobotData(
                    this.entityData.get(isShifting ? robotStandByPos : robotContainerPos));

            NetworkHooks.openGui(serverPlayer,
                    new MenuProviderRobot(robotData, this, isShifting), robotData.pos());

            return InteractionResult.SUCCESS;
        }
        return InteractionResult.PASS;
    }

    public static @NotNull AttributeSupplier.Builder createLivingAttributes() {
        return Mob.createMobAttributes()
                .add(Attributes.MAX_HEALTH)
                .add(Attributes.FOLLOW_RANGE);
    }

    public void readAdditionalSaveData(@NotNull CompoundTag tag) {
        super.readAdditionalSaveData(tag);
        this.actionData = RobotActionData.standby(RobotAction.standby);

        if (tag.contains("RobotCodeData")
            && tag.get("RobotCodeData") instanceof CompoundTag tagCode) {
            this.entityData.set(robotStandByPos, new BlockPos(
                    tagCode.getInt("x"),
                    tagCode.getInt("y"),
                    tagCode.getInt("z")));
        }

        if (tag.contains("RobotContainerData")
            && tag.get("RobotContainerData") instanceof CompoundTag tagCode) {
            this.entityData.set(robotContainerPos, new BlockPos(
                    tagCode.getInt("x"),
                    tagCode.getInt("y"),
                    tagCode.getInt("z")));
        }

        if (tag.contains("stored_variables")
            && tag.get("stored_variables") instanceof ListTag tagVariables) {
            storedVariables.clear();
            for (int i = 0; i < tagVariables.size(); i++) {
                CompoundTag compoundTag = tagVariables.getCompound(i);
                String name = compoundTag.getString("name");

                Variable variable = switch (compoundTag.getString("type")) {
                    case "string" -> new StringVariable((String) null);
                    case "complex" -> new ComplexVariable(0, 0);
                    case "number" -> new NumberVariable(0);
                    case "boolean" -> new BooleanVariable(false);
                    default -> null;
                };
                if (variable != null) {
                    variable.deserialize(tag);
                    storedVariables.add(Pair.of(name, variable));
                }
            }
        }
    }

    @Override
    public boolean isNoGravity() {
        return true;
    }

    @Override
    public void addAdditionalSaveData(@NotNull CompoundTag tag) {
        super.addAdditionalSaveData(tag);
        addRobotTag(tag, this.entityData.get(robotStandByPos), this.entityData.get(robotContainerPos));
        ListTag storedVariableTag = new ListTag();
        for (Pair<String, Variable> storedVariable : this.storedVariables) {
            storedVariableTag.add(storedVariable.getValue().serialize(storedVariable.getKey()));
        }
        tag.put("stored_variables", storedVariableTag);
    }

    public static void addRobotTag(CompoundTag tag, BlockPos posStandBy, BlockPos posContainer) {
        CompoundTag tagCode = new CompoundTag();
        tagCode.putInt("x", posStandBy.getX());
        tagCode.putInt("y", posStandBy.getY());
        tagCode.putInt("z", posStandBy.getZ());
        tag.put("RobotCodeData", tagCode);

        CompoundTag tagContainer = new CompoundTag();
        tagContainer.putInt("x", posContainer.getX());
        tagContainer.putInt("y", posContainer.getY());
        tagContainer.putInt("z", posContainer.getZ());
        tag.put("RobotContainerData", tagContainer);
    }

    @Override
    protected @NotNull AABB makeBoundingBox() {
        return new AABB(
                super.position().x - 0.5f,
                super.position().y,
                super.position().z - 0.5f,
                super.position().x + 0.5f,
                super.position().y + 1f,
                super.position().z + 0.5f);
    }

    @Override
    public boolean isPushable() {
        return true;
    }

    @Override
    public boolean canBeCollidedWith() {
        return true;
    }

    @Override
    public boolean canCollideWith(@NotNull Entity p_20303_) {
        return this.isAlive();
    }

    @Override
    public boolean isAttackable() {
        return true;
    }

    @Override
    public boolean shouldShowName() {
        return false;
    }

    @Override
    public boolean displayFireAnimation() {
        return false;
    }

    @Override
    protected boolean canRide(@NotNull Entity p_20339_) {
        return false;
    }

    public void setRobotStandByPos(int x, int y, int z) {
        BlockPos pos = new BlockPos(x, y, z);
        if (level.isLoaded(pos)) {
            this.entityData.set(robotStandByPos, pos);
        }
    }

    public void setRobotContainer(int x, int y, int z) {
        BlockPos pos = new BlockPos(x, y, z);
        if (level.isLoaded(pos)) {
            this.entityData.set(robotContainerPos, pos);
        }
    }

    public void addVariable(String name, Variable variable) {
        this.storedVariables.removeIf((pair) -> (pair.getKey().equals(name)));
        if (this.storedVariables.size() >= 10) {
            this.storedVariables.remove(0);
        }
        this.storedVariables.add(Pair.of(name, variable));
    }
}