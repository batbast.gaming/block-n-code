package eu.tyrano.blockncode.mod.common.network;

import eu.tyrano.blockncode.mod.common.blockentities.BlockEntityValueNumberInput;
import io.netty.buffer.ByteBuf;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ValueNumberMessage {

    private double value;
    private BlockPos blockPos;

    private final boolean failed;

    public ValueNumberMessage(double value, BlockPos blockPos) {
        this.value = value;
        this.blockPos = blockPos;
        this.failed = false;
    }

    private ValueNumberMessage(boolean failed) {
        this.failed = failed;
    }

    public static ValueNumberMessage decode(ByteBuf buf) {
        try {
            double value = buf.readDouble();
            int x = buf.readInt();
            int y = buf.readInt();
            int z = buf.readInt();
            return new ValueNumberMessage(value, new BlockPos(x, y, z));
        } catch (IndexOutOfBoundsException e) {
            return new ValueNumberMessage(true);
        }
    }

    public static void messageConsumer(ValueNumberMessage msg, Supplier<NetworkEvent.Context> ctx) {
        handle(msg, ctx);
        ctx.get().setPacketHandled(true);
    }

    public static void encode(ValueNumberMessage msg, FriendlyByteBuf buf) {
        buf.writeDouble(msg.value);
        buf.writeInt(msg.blockPos.getX());
        buf.writeInt(msg.blockPos.getY());
        buf.writeInt(msg.blockPos.getZ());
    }

    public static void handle(ValueNumberMessage msg, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            if (!msg.failed) {
                if (ctx.get().getSender() != null && ctx.get().getSender().position().distanceTo(Vec3.atCenterOf(msg.blockPos)) <= 10) {
                    BlockEntity blockEntity = ctx.get().getSender().getLevel().getBlockEntity(msg.blockPos);
                    if (blockEntity instanceof BlockEntityValueNumberInput valueNumberInput) {
                        valueNumberInput.setValue(msg.value);
                    }
                }
            }
        });
    }
}
