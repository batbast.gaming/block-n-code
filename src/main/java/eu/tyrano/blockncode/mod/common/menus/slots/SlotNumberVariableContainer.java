package eu.tyrano.blockncode.mod.common.menus.slots;

import eu.tyrano.blockncode.mod.common.items.NumberVariableItem;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class SlotNumberVariableContainer extends SlotVariableContainer {
    public SlotNumberVariableContainer(IItemHandler itemHandler, int index, int xPosition, int yPosition) {
        super(itemHandler, index, xPosition, yPosition);
    }

    public boolean isRightType(ItemStack itemStack) {
        return itemStack.getItem() instanceof NumberVariableItem;
    };
}
