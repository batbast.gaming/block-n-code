package eu.tyrano.blockncode.mod.datagen;

import eu.tyrano.blockncode.mod.common.ModBlocks;
import eu.tyrano.blockncode.mod.common.ModItems;
import eu.tyrano.blockncode.mod.common.blocks.code.BlockCodeDebug;
import eu.tyrano.blockncode.mod.common.blocks.sign.BlockStandingMetalSign;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

public class ModRecipes extends RecipeProvider {
    public ModRecipes(DataGenerator generator) {
        super(generator);
    }

    @Override
    protected void buildCraftingRecipes(@NotNull Consumer<FinishedRecipe> consumer) {
        for (RegistryObject<Block> block : ModBlocks.BLOCKS.getEntries()) {
            if (!(block.get() instanceof BlockCodeDebug) && !(block.get() instanceof BlockStandingMetalSign)) {
                stonecutterResultFromBase(consumer, block.get(), ModBlocks.codeBaseBlock.get());
            }
        }

        stonecutterResultFromBase(consumer, ModItems.booleanVariable.get(), ModItems.codeCore.get(), 4);
        stonecutterResultFromBase(consumer, ModItems.numberVariable.get(), ModItems.codeCore.get(), 4);
        stonecutterResultFromBase(consumer, ModItems.stringVariable.get(), ModItems.codeCore.get(), 4);
        stonecutterResultFromBase(consumer, ModItems.arrayVariable.get(), ModItems.codeCore.get(), 4);
    }
}
