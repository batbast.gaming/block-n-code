package eu.tyrano.blockncode.mod.client.tts;

import com.mojang.text2speech.Narrator;

public class NarratorManager {
    private static final Narrator narrator = Narrator.getNarrator();

    public static void add(String message) {
        narrator.say(message, false);
    }

    public static void clear() {
        narrator.clear();
    }
}
