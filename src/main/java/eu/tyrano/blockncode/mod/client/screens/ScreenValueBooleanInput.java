package eu.tyrano.blockncode.mod.client.screens;

import com.mojang.blaze3d.vertex.PoseStack;
import eu.tyrano.blockncode.mod.common.menus.MenuValueBooleanInput;
import eu.tyrano.blockncode.mod.common.ModPacketHandler;
import eu.tyrano.blockncode.mod.common.network.ValueBooleanMessage;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.CycleButton;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.CommonComponents;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.entity.player.Inventory;

public class ScreenValueBooleanInput extends AbstractContainerScreen<MenuValueBooleanInput> {

    private CycleButton<Boolean> button;
    private final BlockPos blockPos;

    public ScreenValueBooleanInput(MenuValueBooleanInput menu, Inventory p_97742_, Component p_97743_) {
        super(menu, p_97742_, p_97743_);
        this.blockPos = menu.getBlockPos();
    }

    public void resize(Minecraft p_97677_, int p_97678_, int p_97679_) {
        Boolean value = this.button.getValue();
        this.init(p_97677_, p_97678_, p_97679_);
        this.button.setValue(value);
    }

    @Override
    protected void init() {
        this.addRenderableWidget(new Button(this.width / 2 - 75, this.height / 2 - 10, 150, 20,
                CommonComponents.GUI_DONE, (p_97691_) -> this.onClose(true)));

        this.addRenderableWidget(new Button(this.width / 2 - 75, this.height * 5 / 8 - 10, 150, 20,
                CommonComponents.GUI_CANCEL, (p_97691_) -> this.onClose(false)));

        this.button = CycleButton
                .onOffBuilder(this.menu.getBaseValue())
                .displayOnlyValue()
                .create(this.width / 2 - 75, this.height * 3 / 8 - 10, 150, 20,
                        new TextComponent("Value"));

        this.addRenderableWidget(this.button);
    }

    @Override
    protected void renderBg(PoseStack poseStack, float pPartialTick, int pMouseX, int pMouseY) {
        this.renderBackground(poseStack);
    }

    public void onClose(boolean save) {
        if (save) {
            boolean value = this.button.getValue();
            ModPacketHandler.INSTANCE.sendToServer(new ValueBooleanMessage(value, blockPos));
        }
        super.onClose();
    }
}
