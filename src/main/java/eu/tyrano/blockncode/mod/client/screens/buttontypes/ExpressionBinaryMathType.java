package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum ExpressionBinaryMathType implements ButtonType {
    add("Add"),
    subtract("Subtract"),
    multiply("Multiply"),
    divide("Divide"),
    pow("Power"),
    intRandom("Random integer"),
    doubleRandom("Random double"),
    modulo("Modulo"),
    min("Minimum"),
    max("maximum"),
    log("Logarithm");

    public static List<ButtonType> getEnumConstants() {
        return List.of(ExpressionBinaryMathType.values());
    }

    private final String key;

    ExpressionBinaryMathType(String p_19027_) {
        this.key = p_19027_;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}
