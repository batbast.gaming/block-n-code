package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum ModificationPlayerListenType implements ButtonType {
    playerListen("Get player voice");

    public static List<ButtonType> getEnumConstants() {
        return List.of(ModificationPlayerListenType.values());
    }

    private final String key;

    ModificationPlayerListenType(String p_19027_) {
        this.key = p_19027_;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}
