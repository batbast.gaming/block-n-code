package eu.tyrano.blockncode.mod.client;

import eu.tyrano.blockncode.mod.client.entities.RobotRenderer;
import eu.tyrano.blockncode.mod.common.ModEntities;
import net.minecraftforge.client.event.EntityRenderersEvent;

public class ModEntityRenderers {
    public static void register(EntityRenderersEvent.RegisterRenderers registerRenderers) {
        registerRenderers.registerEntityRenderer(ModEntities.robot.get(), RobotRenderer::new);
    }
}
