package eu.tyrano.blockncode.mod.client.screens;

import com.mojang.blaze3d.vertex.PoseStack;
import eu.tyrano.blockncode.mod.client.screens.custom.NumericEditBox;
import eu.tyrano.blockncode.mod.common.ModPacketHandler;
import eu.tyrano.blockncode.mod.common.menus.MenuCodeRobot;
import eu.tyrano.blockncode.mod.common.menus.MenuContainerRobot;
import eu.tyrano.blockncode.mod.common.menus.robot.RobotData;
import eu.tyrano.blockncode.mod.common.network.RobotContainerMessage;
import eu.tyrano.blockncode.mod.common.network.RobotCoordinatesMessage;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.CommonComponents;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.entity.player.Inventory;

public class ScreenContainerRobot extends AbstractContainerScreen<MenuContainerRobot> {

    private NumericEditBox xEditBox;
    private NumericEditBox yEditBox;
    private NumericEditBox zEditBox;
    private final RobotData robotData;

    public ScreenContainerRobot(MenuContainerRobot menu, Inventory p_97742_, Component p_97743_) {
        super(menu, p_97742_, p_97743_);
        this.robotData = menu.getRobotData();
    }

    public void resize(Minecraft p_97677_, int p_97678_, int p_97679_) {
        String x = this.xEditBox.getValue();
        String y = this.yEditBox.getValue();
        String z = this.zEditBox.getValue();
        this.init(p_97677_, p_97678_, p_97679_);
        this.xEditBox.setValue(x);
        this.yEditBox.setValue(y);
        this.zEditBox.setValue(z);
    }

    @Override
    protected void containerTick() {
        this.xEditBox.tick();
        this.yEditBox.tick();
        this.zEditBox.tick();

        super.containerTick();
    }

    @Override
    protected void init() {
        this.addRenderableWidget(new Button(this.width / 4 - 75, this.height * 7 / 8 - 10, 150, 20,
                CommonComponents.GUI_DONE, (p_97691_) -> this.onClose(true)));

        this.addRenderableWidget(new Button(this.width * 3 / 4 - 75, this.height * 7 / 8 - 10, 150, 20,
                CommonComponents.GUI_CANCEL, (p_97691_) -> this.onClose(false)));

        this.minecraft.keyboardHandler.setSendRepeatsToGui(true);

        BlockPos valuePos = this.robotData.pos();

        this.xEditBox = new NumericEditBox(this.font, this.width / 2 - 92, this.height / 8 - 10, 184,
                20, new TextComponent("Insert x-axis value"), false);
        this.xEditBox.setMaxLength(10); //TODO : add config for this value
        this.xEditBox.setFocus(true);
        this.xEditBox.setCanLoseFocus(false);
        this.xEditBox.setEditable(true);
        this.xEditBox.setValue(valuePos == null ? "0" : Integer.toString(valuePos.getX()));
        this.xEditBox.setVisible(true);
        this.addRenderableWidget(this.xEditBox);

        this.yEditBox = new NumericEditBox(this.font, this.width / 2 - 92, this.height * 3 / 8 - 10, 184,
                20, new TextComponent("Insert y-axis value"), false);
        this.yEditBox.setMaxLength(10); //TODO : add config for this value
        this.yEditBox.setFocus(true);
        this.yEditBox.setCanLoseFocus(false);
        this.yEditBox.setEditable(true);
        this.yEditBox.setValue(valuePos == null ? "0" : Integer.toString(valuePos.getY()));
        this.yEditBox.setVisible(true);
        this.addRenderableWidget(this.yEditBox);

        this.zEditBox = new NumericEditBox(this.font, this.width / 2 - 92, this.height * 5 / 8 - 10, 184,
                20, new TextComponent("Insert z-axis value"), false);
        this.zEditBox.setMaxLength(10); //TODO : add config for this value
        this.zEditBox.setFocus(true);
        this.zEditBox.setCanLoseFocus(false);
        this.zEditBox.setEditable(true);
        this.zEditBox.setValue(valuePos == null ? "0" : Integer.toString(valuePos.getZ()));
        this.zEditBox.setVisible(true);
        this.addRenderableWidget(this.zEditBox);
    }

    @Override
    protected void renderBg(PoseStack poseStack, float pPartialTick, int pMouseX, int pMouseY) {
        this.renderBackground(poseStack);
    }

    public void onClose(boolean save) {
        if (save) {
            try {
                int x = Integer.parseInt(xEditBox.getValue());
                int y = Integer.parseInt(yEditBox.getValue());
                int z = Integer.parseInt(zEditBox.getValue());
                ModPacketHandler.INSTANCE.sendToServer(new RobotContainerMessage(x, y, z));
            } catch (NumberFormatException ignored) {
            }
        } else {
            super.onClose();
        }
    }
}
