package eu.tyrano.blockncode.mod.client.screens.custom;

import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.network.chat.Component;

public class NumericEditBox extends EditBox {

    private final boolean isFloatingPoint;

    public NumericEditBox(Font p_94114_, int p_94115_, int p_94116_,
                          int p_94117_, int p_94118_, Component p_94119_, boolean isFloatingPoint) {
        super(p_94114_, p_94115_, p_94116_, p_94117_, p_94118_, p_94119_);
        this.isFloatingPoint = isFloatingPoint;
    }

    @Override
    public boolean charTyped(char character, int p_94123_) {
        if (Character.isDigit(character)) {
            return super.charTyped(character, p_94123_);
        } else if (isFloatingPoint && character == '.' && !this.getValue().contains(".")) {
            return super.charTyped(character, p_94123_);
        } else if (character == '-' && this.getValue().equals("")) {
            return super.charTyped(character, p_94123_);
        } else {
            return false;
        }
    }
}
