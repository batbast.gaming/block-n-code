package eu.tyrano.blockncode.mod.client.screens;

import com.mojang.blaze3d.vertex.PoseStack;
import eu.tyrano.blockncode.mod.client.screens.custom.NumericEditBox;
import eu.tyrano.blockncode.mod.common.menus.MenuValueComplexInput;
import eu.tyrano.blockncode.mod.common.menus.MenuValueNumberInput;
import eu.tyrano.blockncode.mod.common.ModPacketHandler;
import eu.tyrano.blockncode.mod.common.network.ValueComplexMessage;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.CommonComponents;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.entity.player.Inventory;

public class ScreenValueComplexInput extends AbstractContainerScreen<MenuValueComplexInput> {

    private NumericEditBox realEditBox;
    private NumericEditBox imaginaryEditBox;
    private final BlockPos blockPos;

    public ScreenValueComplexInput(MenuValueComplexInput menu, Inventory p_97742_, Component p_97743_) {
        super(menu, p_97742_, p_97743_);
        this.blockPos = menu.getBlockPos();
    }

    public void resize(Minecraft p_97677_, int p_97678_, int p_97679_) {
        String realValue = this.realEditBox.getValue();
        String imaginaryValue = this.imaginaryEditBox.getValue();
        this.init(p_97677_, p_97678_, p_97679_);
        this.realEditBox.setValue(realValue);
        this.imaginaryEditBox.setValue(imaginaryValue);
    }

    @Override
    protected void containerTick() {
        this.realEditBox.tick();
        this.imaginaryEditBox.tick();
        super.containerTick();
    }

    @Override
    protected void init() {
        this.addRenderableWidget(new Button(this.width / 2 - 75, this.height / 2 - 10, 150, 20,
                CommonComponents.GUI_DONE, (p_97691_) -> this.onClose(true)));

        this.addRenderableWidget(new Button(this.width / 2 - 75, this.height * 5 / 8 - 10, 150, 20,
                CommonComponents.GUI_CANCEL, (p_97691_) -> this.onClose(false)));

        this.minecraft.keyboardHandler.setSendRepeatsToGui(true);

        this.realEditBox = new NumericEditBox(this.font, this.width / 2 - 92, this.height / 8 - 10, 184,
                20, new TextComponent("Insert real value"), true);
        this.realEditBox.setMaxLength(10); //TODO : add config for this value
        this.realEditBox.setFocus(true);
        this.realEditBox.setCanLoseFocus(false);
        this.realEditBox.setEditable(true);
        this.realEditBox.setValue(Double.toString(this.menu.getBaseRealValue()));
        this.realEditBox.setVisible(true);
        this.addRenderableWidget(this.realEditBox);

        this.imaginaryEditBox = new NumericEditBox(this.font, this.width / 2 - 92, this.height * 3 / 8 - 10, 184,
                20, new TextComponent("Insert imaginary value"), true);
        this.imaginaryEditBox.setMaxLength(10); //TODO : add config for this value
        this.imaginaryEditBox.setFocus(true);
        this.imaginaryEditBox.setCanLoseFocus(false);
        this.imaginaryEditBox.setEditable(true);
        this.imaginaryEditBox.setValue(Double.toString(this.menu.getBaseImaginaryValue()));
        this.imaginaryEditBox.setVisible(true);
        this.addRenderableWidget(this.imaginaryEditBox);

    }

    @Override
    protected void renderBg(PoseStack poseStack, float pPartialTick, int pMouseX, int pMouseY) {
        this.renderBackground(poseStack);
    }

    public void onClose(boolean save) {
        if (save) {
            double realValue = Double.parseDouble(this.realEditBox.getValue());
            double imaginaryValue = Double.parseDouble(this.imaginaryEditBox.getValue());
            ModPacketHandler.INSTANCE.sendToServer(new ValueComplexMessage(realValue, imaginaryValue, blockPos));
        }
        super.onClose();
    }
}
