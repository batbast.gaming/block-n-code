package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum ExpressionUnaryMathType implements ButtonType {
    sqrt("Square root"),
    absolute("Absolute"),
    read("Read"),
    getImaginary("Get imaginary part"),
    getReal("Get real part"),
    sin("Sinus"),
    cos("Cosinus"),
    tan("Tangent"),
    ln("Natural logarithm");

    public static List<ButtonType> getEnumConstants() {
        return List.of(ExpressionUnaryMathType.values());
    }

    private final String key;

    ExpressionUnaryMathType(String key) {
        this.key = key;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}
