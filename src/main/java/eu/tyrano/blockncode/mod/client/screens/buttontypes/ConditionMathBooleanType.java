package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum ConditionMathBooleanType implements ButtonType {
    equal("Equal"),
    different("Different"),
    inferior("Inferior"),
    superior("Superior"),
    strictInferior("Strictly inferior"),
    strictSuperior("Strictly superior");

    public static List<ButtonType> getEnumConstants() {
        return List.of(ConditionMathBooleanType.values());
    }

    private final String key;

    ConditionMathBooleanType(String p_19027_) {
        this.key = p_19027_;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}