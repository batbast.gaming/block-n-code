package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;

import java.util.List;

public interface ButtonType {
    Component getDisplayName();
}
