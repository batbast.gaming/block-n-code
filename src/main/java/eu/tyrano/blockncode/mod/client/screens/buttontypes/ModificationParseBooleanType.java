package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum ModificationParseBooleanType implements ButtonType {
    parseBoolean("Boolean to String");

    public static List<ButtonType> getEnumConstants() {
        return List.of(ModificationParseBooleanType.values());
    }

    private final String key;

    ModificationParseBooleanType(String p_19027_) {
        this.key = p_19027_;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}
