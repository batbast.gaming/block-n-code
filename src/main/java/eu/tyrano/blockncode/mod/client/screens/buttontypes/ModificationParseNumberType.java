package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum ModificationParseNumberType implements ButtonType {
    parseNumber("Number to String");

    public static List<ButtonType> getEnumConstants() {
        return List.of(ModificationParseNumberType.values());
    }

    private final String key;

    ModificationParseNumberType(String p_19027_) {
        this.key = p_19027_;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}
