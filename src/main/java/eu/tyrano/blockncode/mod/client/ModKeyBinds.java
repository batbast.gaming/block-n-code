package eu.tyrano.blockncode.mod.client;

import com.mojang.blaze3d.platform.InputConstants;
import eu.tyrano.blockncode.mod.client.tts.NarratorManager;
import net.minecraft.client.KeyMapping;
import net.minecraftforge.client.ClientRegistry;
import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.lwjgl.glfw.GLFW;

public class ModKeyBinds {
    private static final Lazy<KeyMapping> stopTTSKey = Lazy.of(() -> new KeyMapping(
            "modKeys.stopTTS",
            InputConstants.Type.KEYSYM,
            GLFW.GLFW_KEY_L,
            KeyMapping.CATEGORY_MISC
    ));

    public static void register() {
        ClientRegistry.registerKeyBinding(stopTTSKey.get());
    }


    @SubscribeEvent
    public void onClientTick(TickEvent.ClientTickEvent event) {
        if (event.phase == TickEvent.Phase.START) {
            while (stopTTSKey.get().consumeClick()) {
                NarratorManager.clear();
            }
        }
    }
}
