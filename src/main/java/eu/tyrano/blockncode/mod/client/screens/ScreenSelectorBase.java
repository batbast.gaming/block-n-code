package eu.tyrano.blockncode.mod.client.screens;

import com.mojang.blaze3d.vertex.PoseStack;
import eu.tyrano.blockncode.mod.client.screens.buttontypes.*;
import eu.tyrano.blockncode.mod.common.ModPacketHandler;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorType;
import eu.tyrano.blockncode.mod.common.menus.MenuSelectorBase;
import eu.tyrano.blockncode.mod.common.network.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.CycleButton;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.CommonComponents;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.entity.player.Inventory;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ScreenSelectorBase<B extends MenuSelectorBase<?, ?>> extends AbstractContainerScreen<B> {

    private CycleButton<ButtonType> button;
    private final BlockPos blockPos;
    private final BlockEntitySelectorType selectorType;

    public ScreenSelectorBase(B menu, Inventory p_97742_, Component p_97743_) {
        super(menu, p_97742_, p_97743_);
        this.blockPos = menu.getBlockPos();
        this.selectorType = menu.getSelectorType();
    }

    public void resize(@NotNull Minecraft p_97677_, int p_97678_, int p_97679_) {
        ButtonType value = this.button.getValue();
        this.init(p_97677_, p_97678_, p_97679_);
        this.button.setValue(value);
    }

    @Override
    protected void init() {
        this.addRenderableWidget(new Button(this.width / 2 - 75, this.height / 2 - 10, 150, 20,
                CommonComponents.GUI_DONE, (p_97691_) -> this.onClose(true)));

        this.addRenderableWidget(new Button(this.width / 2 - 75, this.height * 5 / 8 - 10, 150, 20,
                CommonComponents.GUI_CANCEL, (p_97691_) -> this.onClose(false)));

        List<ButtonType> valueList = null;
        switch (this.selectorType) {
            case unaryBoolean -> valueList = ConditionUnaryBooleanType.getEnumConstants();
            case binaryBoolean -> valueList = ConditionBinaryBooleanType.getEnumConstants();
            case mathBoolean -> valueList = ConditionMathBooleanType.getEnumConstants();
            case unaryMath -> valueList = ExpressionUnaryMathType.getEnumConstants();
            case chatToString -> valueList = ModificationChatReadType.getEnumConstants();
            case voiceToString -> valueList = ModificationPlayerListenType.getEnumConstants();
            case binaryMath -> valueList = ExpressionBinaryMathType.getEnumConstants();
            case binaryString -> valueList = ModificationStringType.getEnumConstants();
            case numberToString -> valueList = ModificationParseNumberType.getEnumConstants();
            case booleanToString -> valueList = ModificationParseBooleanType.getEnumConstants();
            case stringToBoolean -> valueList = ConditionParseStringType.getEnumConstants();
            case stringToNumber -> valueList = ExpressionParseStringType.getEnumConstants();
            case stringBoolean -> valueList = ConditionStringBooleanType.getEnumConstants();
            case robotInstruction -> valueList = RobotActionType.getEnumConstants();
            case direction -> valueList = DirectionType.getEnumConstants();
        }

        if (valueList == null) {
            throw new IllegalStateException();
        }

        this.button = CycleButton
                .builder(ButtonType::getDisplayName)
                .withValues(valueList)
                .withInitialValue(this.menu.getBaseType())
                .create(this.width / 2 - 75, this.height * 3 / 8 - 10, 150, 20,
                        new TextComponent("Type"));

        this.addRenderableWidget(this.button);
    }

    @Override
    protected void renderBg(@NotNull PoseStack poseStack, float pPartialTick, int pMouseX, int pMouseY) {
        this.renderBackground(poseStack);
    }

    public void onClose(boolean save) {
        if (save) {
            SelectorMessageBase selectorNetworkMessage = null;

            Class<? extends ButtonType> aClass = this.button.getValue().getClass();
            if (ConditionUnaryBooleanType.class.equals(aClass) || ConditionBinaryBooleanType.class.equals(aClass) || ConditionParseStringType.class.equals(aClass)) {
                selectorNetworkMessage = new ConditionBooleanMessage();
            } else if (ConditionMathBooleanType.class.equals(aClass)) {
                selectorNetworkMessage = new ConditionMathMessage();
            } else if (ConditionStringBooleanType.class.equals(aClass)) {
                selectorNetworkMessage = new ConditionStringMessage();
            } else if (ExpressionUnaryMathType.class.equals(aClass) || ExpressionBinaryMathType.class.equals(aClass) || ExpressionParseStringType.class.equals(aClass)) {
                selectorNetworkMessage = new ExpressionMessage();
            } else if (ModificationStringType.class.equals(aClass) || ModificationParseBooleanType.class.equals(aClass) || ModificationParseNumberType.class.equals(aClass)
                       || ModificationChatReadType.class.equals(aClass) || ModificationPlayerListenType.class.equals(aClass)) {
                selectorNetworkMessage = new ModificationMessage();
            } else if (RobotActionType.class.equals(aClass)) {
                selectorNetworkMessage = new RobotActionMessage();
            } else if (DirectionType.class.equals(aClass)) {
                selectorNetworkMessage = new DirectionMessage();
            }

            if (selectorNetworkMessage == null) {
                return;
            }
            selectorNetworkMessage.setBlockPos(this.blockPos);
            selectorNetworkMessage.setValue(this.button.getValue());
            selectorNetworkMessage.setReady();

            ModPacketHandler.INSTANCE.sendToServer(selectorNetworkMessage);
        }
        super.onClose();
    }
}
