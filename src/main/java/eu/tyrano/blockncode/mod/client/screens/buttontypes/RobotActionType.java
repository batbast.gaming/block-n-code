package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum RobotActionType implements ButtonType {
    moving("Moving"),
    placing("Placing"),
    breaking("Breaking"),
    waiting("Waiting"),
    disassemble("Disassemble");

    public static List<ButtonType> getEnumConstants() {
        return List.of(RobotActionType.values());
    }

    private final String key;

    RobotActionType(String p_19027_) {
        this.key = p_19027_;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}