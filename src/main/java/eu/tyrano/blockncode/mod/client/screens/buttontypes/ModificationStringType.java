package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum ModificationStringType implements ButtonType{
    regex("Apply regex"),
    remove("Remove"),
    append("Append");

    public static List<ButtonType> getEnumConstants() {
        return List.of(ModificationStringType.values());
    }

    private final String key;

    ModificationStringType(String p_19027_) {
        this.key = p_19027_;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}
