package eu.tyrano.blockncode.mod.client.tts;

import java.nio.ByteBuffer;

public class ClientTTSPacketHandler {

    public static ByteBuffer buffer;

    public static void handlePacket(String value) {
        if (value != null && !value.equals("")) {
            NarratorManager.add(value);
        }
    }
}
