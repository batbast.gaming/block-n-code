package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum ConditionBinaryBooleanType implements ButtonType {
    and("And"),
    or("Or"),
    nor("Nor"),
    xor("Xor");

    public static List<ButtonType> getEnumConstants() {
        return List.of(ConditionBinaryBooleanType.values());
    }

    private final String key;

    ConditionBinaryBooleanType(String p_19027_) {
        this.key = p_19027_;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}
