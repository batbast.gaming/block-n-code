package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum ConditionUnaryBooleanType implements ButtonType {
    no("No"),
    read("Read");

    public static List<ButtonType> getEnumConstants() {
        return List.of(ConditionUnaryBooleanType.values());
    }

    private final String key;

    ConditionUnaryBooleanType(String p_19027_) {
        this.key = p_19027_;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}
