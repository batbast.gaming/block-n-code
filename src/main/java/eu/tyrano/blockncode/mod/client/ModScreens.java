package eu.tyrano.blockncode.mod.client;

import eu.tyrano.blockncode.backend.variable.bool.ConditionBooleanType;
import eu.tyrano.blockncode.backend.variable.bool.ConditionMathType;
import eu.tyrano.blockncode.backend.variable.bool.ConditionStringType;
import eu.tyrano.blockncode.backend.variable.number.OperationType;
import eu.tyrano.blockncode.backend.variable.string.ModificationType;
import eu.tyrano.blockncode.mod.client.screens.*;
import eu.tyrano.blockncode.mod.client.screens.buttontypes.*;
import eu.tyrano.blockncode.mod.common.ModBlockEntities;
import eu.tyrano.blockncode.mod.common.ModMenus;
import eu.tyrano.blockncode.mod.common.ModWoodTypes;
import eu.tyrano.blockncode.mod.common.blockentities.BlockEntitySelectorBase;
import eu.tyrano.blockncode.mod.common.entities.robot.RobotAction;
import eu.tyrano.blockncode.mod.common.menus.MenuSelectorBase;
import net.minecraft.client.gui.screens.MenuScreens;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderers;
import net.minecraft.client.renderer.blockentity.SignRenderer;
import net.minecraft.core.Direction;
import net.minecraft.world.level.block.state.properties.WoodType;

public class ModScreens {
    public static void register() {
        MenuScreens.register(ModMenus.menuVariableWriter.get(), ScreenVariableWriter::new);

        MenuScreens.register(ModMenus.menuVariableNumberReader.get(), ScreenVariableReader::new);
        MenuScreens.register(ModMenus.menuVariableBooleanReader.get(), ScreenVariableReader::new);
        MenuScreens.register(ModMenus.menuVariableStringReader.get(), ScreenVariableReader::new);

        MenuScreens.register(ModMenus.menuVariableRename.get(), ScreenVariableRename::new);

        MenuScreens.register(ModMenus.menuValueBooleanInput.get(), ScreenValueBooleanInput::new);
        MenuScreens.register(ModMenus.menuValueNumberInput.get(), ScreenValueNumberInput::new);
        MenuScreens.register(ModMenus.menuValueComplexInput.get(), ScreenValueComplexInput::new);
        MenuScreens.register(ModMenus.menuValueStringInput.get(), ScreenValueStringInput::new);

        MenuScreens.register(ModMenus.menuFunctionCoordinates.get(), ScreenFunctionCoordinates::new);

        MenuScreens.register(ModMenus.menuConditionUnaryBoolean.get(), ScreenSelectorBase<
                MenuSelectorBase<ConditionUnaryBooleanType,
                        BlockEntitySelectorBase<ConditionBooleanType, ConditionUnaryBooleanType>>>::new);


        MenuScreens.register(ModMenus.menuConditionBinaryBoolean.get(), ScreenSelectorBase<
                MenuSelectorBase<ConditionBinaryBooleanType,
                        BlockEntitySelectorBase<ConditionBooleanType, ConditionBinaryBooleanType>>>::new);


        MenuScreens.register(ModMenus.menuConditionMathBoolean.get(), ScreenSelectorBase<
                MenuSelectorBase<ConditionMathBooleanType,
                        BlockEntitySelectorBase<ConditionMathType, ConditionMathBooleanType>>>::new);


        MenuScreens.register(ModMenus.menuConditionStringBoolean.get(), ScreenSelectorBase<
                MenuSelectorBase<ConditionStringBooleanType,
                        BlockEntitySelectorBase<ConditionStringType, ConditionStringBooleanType>>>::new);


        MenuScreens.register(ModMenus.menuOperationUnaryMath.get(), ScreenSelectorBase<
                MenuSelectorBase<ExpressionUnaryMathType,
                        BlockEntitySelectorBase<OperationType, ExpressionUnaryMathType>>>::new);


        MenuScreens.register(ModMenus.menuOperationBinaryMath.get(), ScreenSelectorBase<
                MenuSelectorBase<ExpressionBinaryMathType,
                        BlockEntitySelectorBase<OperationType, ExpressionBinaryMathType>>>::new);


        MenuScreens.register(ModMenus.menuModificationBinaryString.get(), ScreenSelectorBase<
                MenuSelectorBase<ModificationStringType,
                        BlockEntitySelectorBase<ModificationType, ModificationStringType>>>::new);


        MenuScreens.register(ModMenus.menuModificationBooleanString.get(), ScreenSelectorBase<
                MenuSelectorBase<ModificationParseBooleanType,
                        BlockEntitySelectorBase<ModificationType, ModificationParseBooleanType>>>::new);


        MenuScreens.register(ModMenus.menuModificationNumberString.get(), ScreenSelectorBase<
                MenuSelectorBase<ModificationParseNumberType,
                        BlockEntitySelectorBase<ModificationType, ModificationParseNumberType>>>::new);


        MenuScreens.register(ModMenus.menuModificationChatRead.get(), ScreenSelectorBase<
                MenuSelectorBase<ModificationChatReadType,
                        BlockEntitySelectorBase<ModificationType, ModificationChatReadType>>>::new);


        MenuScreens.register(ModMenus.menuModificationPlayerListen.get(), ScreenSelectorBase<
                MenuSelectorBase<ModificationPlayerListenType,
                        BlockEntitySelectorBase<ModificationType, ModificationPlayerListenType>>>::new);


        MenuScreens.register(ModMenus.menuOperationStringToNumber.get(), ScreenSelectorBase<
                MenuSelectorBase<ExpressionParseStringType,
                        BlockEntitySelectorBase<OperationType, ExpressionParseStringType>>>::new);


        MenuScreens.register(ModMenus.menuConditionStringToBoolean.get(), ScreenSelectorBase<
                MenuSelectorBase<ConditionParseStringType,
                        BlockEntitySelectorBase<ConditionBooleanType, ConditionParseStringType>>>::new);


        MenuScreens.register(ModMenus.menuRobotInstruction.get(), ScreenSelectorBase<
                MenuSelectorBase<RobotActionType,
                        BlockEntitySelectorBase<RobotAction, RobotActionType>>>::new);


        MenuScreens.register(ModMenus.menuDirection.get(), ScreenSelectorBase<
                MenuSelectorBase<DirectionType,
                        BlockEntitySelectorBase<Direction, DirectionType>>>::new);


        MenuScreens.register(ModMenus.menuCodeRobot.get(), ScreenCodeRobot::new);
        MenuScreens.register(ModMenus.menuContainerRobot.get(), ScreenContainerRobot::new);

        WoodType.register(ModWoodTypes.metal);
        BlockEntityRenderers.register(ModBlockEntities.metalSign.get(), SignRenderer::new);
    }
}
