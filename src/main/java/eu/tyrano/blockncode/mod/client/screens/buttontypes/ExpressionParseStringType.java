package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum ExpressionParseStringType implements ButtonType {
    parseString("String to number");

    public static List<ButtonType> getEnumConstants() {
        return List.of(ExpressionParseStringType.values());
    }

    private final String key;

    ExpressionParseStringType(String p_19027_) {
        this.key = p_19027_;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}
