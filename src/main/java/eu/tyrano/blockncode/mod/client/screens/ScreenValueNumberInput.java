package eu.tyrano.blockncode.mod.client.screens;

import com.mojang.blaze3d.vertex.PoseStack;
import eu.tyrano.blockncode.mod.client.screens.custom.NumericEditBox;
import eu.tyrano.blockncode.mod.common.menus.MenuValueNumberInput;
import eu.tyrano.blockncode.mod.common.ModPacketHandler;
import eu.tyrano.blockncode.mod.common.network.ValueNumberMessage;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.CommonComponents;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.entity.player.Inventory;

public class ScreenValueNumberInput extends AbstractContainerScreen<MenuValueNumberInput> {

    private NumericEditBox editBox;
    private final BlockPos blockPos;

    public ScreenValueNumberInput(MenuValueNumberInput menu, Inventory p_97742_, Component p_97743_) {
        super(menu, p_97742_, p_97743_);
        this.blockPos = menu.getBlockPos();
    }

    public void resize(Minecraft p_97677_, int p_97678_, int p_97679_) {
        String value = this.editBox.getValue();
        this.init(p_97677_, p_97678_, p_97679_);
        this.editBox.setValue(value);
    }

    @Override
    protected void containerTick() {
        this.editBox.tick();
        super.containerTick();
    }

    @Override
    protected void init() {
        this.addRenderableWidget(new Button(this.width / 2 - 75, this.height / 2 - 10, 150, 20,
                CommonComponents.GUI_DONE, (p_97691_) -> this.onClose(true)));

        this.addRenderableWidget(new Button(this.width / 2 - 75, this.height * 5 / 8 - 10, 150, 20,
                CommonComponents.GUI_CANCEL, (p_97691_) -> this.onClose(false)));

        this.minecraft.keyboardHandler.setSendRepeatsToGui(true);

        this.editBox = new NumericEditBox(this.font, this.width / 2 - 92, this.height / 4 - 10, 184,
                20, new TextComponent("Insert number"), true);
        this.editBox.setMaxLength(10); //TODO : add config for this value
        this.editBox.setFocus(true);
        this.editBox.setCanLoseFocus(false);
        this.editBox.setEditable(true);
        this.editBox.setValue(Double.toString(this.menu.getBaseValue()));
        this.editBox.setVisible(true);
        this.addRenderableWidget(this.editBox);

    }

    @Override
    protected void renderBg(PoseStack poseStack, float pPartialTick, int pMouseX, int pMouseY) {
        this.renderBackground(poseStack);
    }

    public void onClose(boolean save) {
        if (save) {
            double value = Double.parseDouble(this.editBox.getValue());
            ModPacketHandler.INSTANCE.sendToServer(new ValueNumberMessage(value, blockPos));
        }
        super.onClose();
    }
}
