package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum ModificationChatReadType implements ButtonType {
    readChat("Read ChatGpt answer");

    public static List<ButtonType> getEnumConstants() {
        return List.of(ModificationChatReadType.values());
    }

    private final String key;

    ModificationChatReadType(String p_19027_) {
        this.key = p_19027_;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}
