package eu.tyrano.blockncode.mod.client.screens;

import com.mojang.blaze3d.platform.InputConstants;
import com.mojang.blaze3d.vertex.PoseStack;
import eu.tyrano.blockncode.mod.client.screens.custom.NumericEditBox;
import eu.tyrano.blockncode.mod.common.ModPacketHandler;
import eu.tyrano.blockncode.mod.common.menus.MenuValueNumberInput;
import eu.tyrano.blockncode.mod.common.menus.MenuValueStringInput;
import eu.tyrano.blockncode.mod.common.network.ValueNumberMessage;
import eu.tyrano.blockncode.mod.common.network.ValueStringMessage;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.screens.inventory.CommandBlockEditScreen;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.CommonComponents;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.ClickType;

public class ScreenValueStringInput extends AbstractContainerScreen<MenuValueStringInput> {

    private EditBox editBox;
    private final BlockPos blockPos;

    public ScreenValueStringInput(MenuValueStringInput menu, Inventory p_97742_, Component p_97743_) {
        super(menu, p_97742_, p_97743_);
        this.blockPos = menu.getBlockPos();
    }

    public void resize(Minecraft p_97677_, int p_97678_, int p_97679_) {
        String value = this.editBox.getValue();
        this.init(p_97677_, p_97678_, p_97679_);
        this.editBox.setValue(value);
    }

    private boolean keyPressedSuper(int p_96552_, int p_96553_, int p_96554_) {
        if (p_96552_ == 256 && this.shouldCloseOnEsc()) {
            this.onClose();
            return true;
        } else if (p_96552_ == 258) {
            boolean flag = !hasShiftDown();
            if (!this.changeFocus(flag)) {
                this.changeFocus(flag);
            }

            return false;
        } else {
            return this.getFocused() != null && this.getFocused().keyPressed(p_96552_, p_96553_, p_96554_);
        }
    }

    @Override
    public boolean keyPressed(int p_97765_, int p_97766_, int p_97767_) {
        InputConstants.Key mouseKey = InputConstants.getKey(p_97765_, p_97766_);
        if (keyPressedSuper(p_97765_, p_97766_, p_97767_)) {
            return true;
        } else if (this.minecraft.options.keyInventory.isActiveAndMatches(mouseKey)) {
            return false;
        } else {
            boolean handled = this.checkHotbarKeyPressed(p_97765_, p_97766_);// Forge MC-146650: Needs to return true when the key is handled
            if (this.hoveredSlot != null && this.hoveredSlot.hasItem()) {
                if (this.minecraft.options.keyPickItem.isActiveAndMatches(mouseKey)) {
                    this.slotClicked(this.hoveredSlot, this.hoveredSlot.index, 0, ClickType.CLONE);
                    handled = true;
                } else if (this.minecraft.options.keyDrop.isActiveAndMatches(mouseKey)) {
                    this.slotClicked(this.hoveredSlot, this.hoveredSlot.index, hasControlDown() ? 1 : 0, ClickType.THROW);
                    handled = true;
                }
            } else if (this.minecraft.options.keyDrop.isActiveAndMatches(mouseKey)) {
                handled = true; // Forge MC-146650: Emulate MC bug, so we don't drop from hotbar when pressing drop without hovering over a item.
            }

            return handled;
        }
    }

    @Override
    protected void containerTick() {
        this.editBox.tick();
        super.containerTick();
    }

    @Override
    protected void init() {
        this.addRenderableWidget(new Button(this.width / 2 - 75, this.height / 2 - 10, 150, 20,
                CommonComponents.GUI_DONE, (p_97691_) -> this.onClose(true)));

        this.addRenderableWidget(new Button(this.width / 2 - 75, this.height * 5 / 8 - 10, 150, 20,
                CommonComponents.GUI_CANCEL, (p_97691_) -> this.onClose(false)));

        this.minecraft.keyboardHandler.setSendRepeatsToGui(true);

        this.editBox = new EditBox(this.font, this.width / 2 - 92, this.height / 4 - 10, 184,
                20, new TextComponent("Input text value"));
        this.editBox.setMaxLength(256); //TODO : add config for this value
        this.editBox.setFocus(true);
        this.editBox.setCanLoseFocus(false);
        this.editBox.setEditable(true);
        this.editBox.setValue(this.menu.getBaseValue());
        this.editBox.setVisible(true);
        this.addRenderableWidget(this.editBox);

    }

    @Override
    protected void renderBg(PoseStack poseStack, float pPartialTick, int pMouseX, int pMouseY) {
        this.renderBackground(poseStack);
    }

    public void onClose(boolean save) {
        if (save) {
            String value = this.editBox.getValue();
            ModPacketHandler.INSTANCE.sendToServer(new ValueStringMessage(value, blockPos));
        }
        super.onClose();
    }
}
