package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum ConditionStringBooleanType implements ButtonType {
    regex("Regex"),
    isSame("Are same");

    public static List<ButtonType> getEnumConstants() {
        return List.of(ConditionStringBooleanType.values());
    }

    private final String key;

    ConditionStringBooleanType(String p_19027_) {
        this.key = p_19027_;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}
