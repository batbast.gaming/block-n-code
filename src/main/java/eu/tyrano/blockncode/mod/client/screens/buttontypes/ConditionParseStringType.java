package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum ConditionParseStringType implements ButtonType {
    parseString("String to boolean");

    public static List<ButtonType> getEnumConstants() {
        return List.of(ConditionParseStringType.values());
    }

    private final String key;

    ConditionParseStringType(String p_19027_) {
        this.key = p_19027_;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}
