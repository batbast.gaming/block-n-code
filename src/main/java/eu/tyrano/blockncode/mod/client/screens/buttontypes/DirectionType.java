package eu.tyrano.blockncode.mod.client.screens.buttontypes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;

import java.util.List;

public enum DirectionType implements ButtonType {
    down( "down"),
    up("up"),
    north( "north"),
    south( "south"),
    west("west"),
    east("east");

    public static List<ButtonType> getEnumConstants() {
        return List.of(DirectionType.values());
    }

    private final String key;

    DirectionType(String p_19027_) {
        this.key = p_19027_;
    }

    public Component getDisplayName() {
        return new TextComponent(this.key);
    }
}