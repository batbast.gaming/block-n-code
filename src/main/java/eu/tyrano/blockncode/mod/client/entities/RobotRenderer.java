package eu.tyrano.blockncode.mod.client.entities;

import com.mojang.blaze3d.vertex.PoseStack;
import eu.tyrano.blockncode.mod.common.entities.robot.RobotEntity;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.LivingEntityRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import org.jetbrains.annotations.NotNull;

public class RobotRenderer extends LivingEntityRenderer<RobotEntity, RobotModel> {
    public RobotRenderer(EntityRendererProvider.Context p_174289_) {
        super(p_174289_, new RobotModel(RobotModel.createBodyLayer().bakeRoot()), 0.0f);
    }

    @Override
    public void render(RobotEntity p_114634_, float p_114635_, float p_114636_,
                       PoseStack p_114637_, MultiBufferSource p_114638_, int p_114639_) {
        super.render(p_114634_, p_114635_, p_114636_, p_114637_, p_114638_, p_114639_);
    }

    @Override
    protected void renderNameTag(RobotEntity p_114498_, Component p_114499_, PoseStack p_114500_, MultiBufferSource p_114501_, int p_114502_) {
    }

    @Override
    public @NotNull ResourceLocation getTextureLocation(@NotNull RobotEntity p_114482_) {
        return RobotModel.LAYER_LOCATION.getModel();
    }
}
