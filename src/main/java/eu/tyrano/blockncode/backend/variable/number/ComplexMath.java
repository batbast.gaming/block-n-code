package eu.tyrano.blockncode.backend.variable.number;

import java.util.UUID;

import static java.lang.Math.*;
import static java.lang.Math.sqrt;

public class ComplexMath {
    public static ComplexVariable multiply(ComplexVariable A, ComplexVariable B) {
        return multiply(A.getRealValue(), A.getImaginaryValue(), B.getRealValue(), B.getImaginaryValue());
    }

    public static ComplexVariable multiply(double realValueA, double imaginaryValueA, double realValueB, double imaginaryValueB) {
        double newRealValue = (realValueB * realValueA) - (imaginaryValueB * imaginaryValueA);
        double newImaginaryValue = (realValueB * imaginaryValueA) + (realValueA * imaginaryValueB);
        return new ComplexVariable( newRealValue, newImaginaryValue);
    }

    public static ComplexVariable power(ComplexVariable A, ComplexVariable B) {
        return power(A.getRealValue(), A.getImaginaryValue(), B.getRealValue(), B.getImaginaryValue());
    }

    public static ComplexVariable power(double realValueA, double imaginaryValueA, double realValueB, double imaginaryValueB) {
        double r = hypot(realValueA, imaginaryValueA);
        double theta = atan2(imaginaryValueA, realValueA);
        if (imaginaryValueB == 0) {
            double powTheta = realValueB * theta;
            return multiply(Math.cos(powTheta), Math.sin(powTheta), pow(r, realValueB), 0);
        } else {
            double lnR = Math.log(r);
            double exp = exp(-theta * imaginaryValueB + realValueB * lnR);
            double real = exp * Math.cos(imaginaryValueB * lnR + theta * realValueB);
            double imaginary = exp * Math.sin(imaginaryValueB * lnR + theta * realValueB);
            return new ComplexVariable( real, imaginary);
        }
    }

    private static double absolute(double realValueA, double imaginaryValueA) {
        return hypot(realValueA, imaginaryValueA);
    }

    public static NumberVariable absolute(ComplexVariable A) {
        return new NumberVariable( absolute(A.getRealValue(), A.getImaginaryValue()));
    }

    public static ComplexVariable add(ComplexVariable A, ComplexVariable B) {
        return add(A.getRealValue(), A.getImaginaryValue(), B.getRealValue(), B.getImaginaryValue());
    }

    public static ComplexVariable add(double realValueA, double imaginaryValueA, double realValueB, double imaginaryValueB) {
        return new ComplexVariable( realValueA + realValueB, imaginaryValueA + imaginaryValueB);
    }

    public static ComplexVariable subtract(ComplexVariable A, ComplexVariable B) {
        return subtract(A.getRealValue(), A.getImaginaryValue(), B.getRealValue(), B.getImaginaryValue());
    }

    public static ComplexVariable subtract(double realValueA, double imaginaryValueA, double realValueB, double imaginaryValueB) {
        return new ComplexVariable( realValueA - realValueB, imaginaryValueA - imaginaryValueB);
    }

    //divide A by B
    public static ComplexVariable divide(ComplexVariable A, ComplexVariable B) {
        return divide(A.getRealValue(), A.getImaginaryValue(), B.getRealValue(), B.getImaginaryValue());
    }

    public static ComplexVariable divide(double realValueA, double imaginaryValueA, double realValueB, double imaginaryValueB) {
        double pow = pow(realValueB, 2) + pow(imaginaryValueB, 2);
        double newRealValue = (realValueA * realValueB + imaginaryValueA * imaginaryValueB) / pow;
        double newImaginaryValue = (imaginaryValueA * realValueB - realValueA * imaginaryValueB) / pow;
        return new ComplexVariable( newRealValue, newImaginaryValue);
    }

    public static ComplexVariable squareRoot(ComplexVariable A) {
        return squareRoot(A.getRealValue(), A.getImaginaryValue());
    }

    public static ComplexVariable squareRoot(double realValueA, double imaginaryValueA) {
        double newRealValue;
        double newImaginaryValue;
        if (imaginaryValueA != 0) {
            newRealValue = sqrt((absolute(realValueA, imaginaryValueA)
                                 + realValueA) / 2);
            newImaginaryValue = (imaginaryValueA / abs(imaginaryValueA))
                                * sqrt((absolute(realValueA, imaginaryValueA) - realValueA) / 2);
        } else {
            double value = sqrt(abs(realValueA));
            if (realValueA >= 0) {
                newRealValue = value;
                newImaginaryValue = 0;
            } else {
                newRealValue = 0;
                newImaginaryValue = value;
            }
        }
        return new ComplexVariable( newRealValue, newImaginaryValue);
    }

    public static ComplexVariable randomInt(ComplexVariable complexA, ComplexVariable complexB) {
        return new ComplexVariable( NumberMath.randomInt(complexA.getRealValue(), complexB.getRealValue()),
                NumberMath.randomInt(complexA.getImaginaryValue(), complexB.getImaginaryValue()));
    }

    public static ComplexVariable randomDouble(ComplexVariable complexA, ComplexVariable complexB) {
        return new ComplexVariable( NumberMath.randomDouble(complexA.getRealValue(), complexB.getRealValue()),
                NumberMath.randomDouble(complexA.getImaginaryValue(), complexB.getImaginaryValue()));
    }

    public static ComplexVariable sin(ComplexVariable complexA) {
        return sin(complexA.getRealValue(), complexA.getImaginaryValue());
    }

    public static ComplexVariable sin(double realValue, double imaginaryValue) {
        return new ComplexVariable(
                (Math.sin(realValue) * Math.cosh(imaginaryValue)),
                (Math.cos(realValue) * Math.sinh(imaginaryValue)));
    }

    public static ComplexVariable cos(ComplexVariable complexA) {
        return cos(complexA.getRealValue(), complexA.getImaginaryValue());
    }

    public static ComplexVariable cos(double realValue, double imaginaryValue) {
        return new ComplexVariable(
                (Math.cos(realValue) * Math.cosh(imaginaryValue)),
                -(Math.sin(realValue) * Math.sinh(imaginaryValue)));
    }

    public static ComplexVariable tan(ComplexVariable complexA) {
        return tan(complexA.getRealValue(), complexA.getImaginaryValue());
    }

    public static ComplexVariable tan(double realValue, double imaginaryValue) {
        return new ComplexVariable(
                divide(sin(realValue, imaginaryValue),
                        cos(realValue, imaginaryValue)));
    }

    public static ComplexVariable log(ComplexVariable complexA, ComplexVariable complexB) {
        return new ComplexVariable(
                divide(ln(complexA.getRealValue(), complexA.getImaginaryValue()),
                        ln(complexB.getRealValue(), complexB.getImaginaryValue())));
    }

    public static ComplexVariable ln(ComplexVariable complexVariable) {
        return ln(complexVariable.getRealValue(), complexVariable.getImaginaryValue());
    }

    private static ComplexVariable ln(double realValue, double imaginaryValue) {
        return new ComplexVariable(
                Math.log(hypot(realValue, imaginaryValue)),
                atan2(imaginaryValue, realValue));
    }
}