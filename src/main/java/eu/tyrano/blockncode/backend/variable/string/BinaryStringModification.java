package eu.tyrano.blockncode.backend.variable.string;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BinaryStringModification extends StringModification {

    private final StringModification firstString;
    private final StringModification secondString;
    private final ModificationType type;

    public BinaryStringModification(StringModification firstString, StringModification secondString, ModificationType type) {
        this.firstString = firstString;
        this.secondString = secondString;
        this.type = type;
    }

    public ModificationResult execute() {
        ModificationResult modificationResultA = firstString.execute();
        ModificationResult modificationResultB = secondString.execute();

        if (modificationResultA.error() == null && modificationResultB.error() == null) {
            switch (type) {
                case regex -> {
                    Pattern pattern = Pattern.compile(modificationResultB.value());
                    Matcher matcher = pattern.matcher(modificationResultA.value());
                    return new ModificationResult(matcher.matches() ? matcher.group() : "", null);
                }
                case append -> {
                    return new ModificationResult(modificationResultA.value() + modificationResultB.value(), null);
                }
                case remove -> {
                    return new ModificationResult(modificationResultA.value().replaceAll(modificationResultB.value(), ""), null);
                }
                default -> {
                    return new ModificationResult(null, "Unknown string modification type");
                }
            }
        } else {
            return modificationResultA.error() != null ? modificationResultA : modificationResultB;
        }
    }
}
