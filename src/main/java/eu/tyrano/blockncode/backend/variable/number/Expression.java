package eu.tyrano.blockncode.backend.variable.number;

public abstract class Expression {
    public abstract ExpressionResult execute();
}
