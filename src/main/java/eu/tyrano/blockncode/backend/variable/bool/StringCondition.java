package eu.tyrano.blockncode.backend.variable.bool;

import eu.tyrano.blockncode.backend.variable.string.ModificationResult;
import eu.tyrano.blockncode.backend.variable.string.StringModification;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringCondition extends Condition {
    private final ConditionStringType type;
    private final StringModification firstModification;
    private final StringModification secondModification;

    public StringCondition(StringModification firstModification, StringModification secondModification, ConditionStringType type) {
        this.type = type;
        this.firstModification = firstModification;
        this.secondModification = secondModification;
    }

    @Override
    public ConditionResult execute() {

        ModificationResult modificationResultA = firstModification.execute();
        ModificationResult modificationResultB = secondModification.execute();

        String resultA = modificationResultA.value();
        String resultB = modificationResultB.value();

        if (modificationResultA.error() == null && modificationResultB.error() == null) {
            switch (type) {
                case regex -> {
                    Pattern pattern = Pattern.compile(modificationResultB.value());
                    Matcher matcher = pattern.matcher(modificationResultA.value());
                    return new ConditionResult(matcher.matches(), null);
                }
                case isSame -> {
                    return new ConditionResult(resultA.equals(resultB), null);
                }
                default -> {
                    return new ConditionResult(false, "Unknown string condition type");
                }
            }
        } else {
            return modificationResultA.error() != null ? new ConditionResult(false, modificationResultA.error()) : new ConditionResult(false, modificationResultB.error());
        }
    }
}