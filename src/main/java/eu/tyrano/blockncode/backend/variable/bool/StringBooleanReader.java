package eu.tyrano.blockncode.backend.variable.bool;

import eu.tyrano.blockncode.backend.variable.string.ModificationResult;
import eu.tyrano.blockncode.backend.variable.string.StringModification;

public class StringBooleanReader extends Condition {

    private final StringModification firstModification;
    private final ConditionBooleanType type;

    public StringBooleanReader(StringModification firstModification, ConditionBooleanType type) {
        this.firstModification = firstModification;
        this.type = type;
    }

    public ConditionResult execute() {
        ModificationResult modificationResult = firstModification.execute();

        if (modificationResult.error() == null) {

            String result = modificationResult.value();

            if (type == ConditionBooleanType.parseString) {
                try {
                    boolean value = Boolean.parseBoolean(result);
                    return new ConditionResult(value, null);
                } catch (NumberFormatException ignored1) {
                    return new ConditionResult(false, "String is not a boolean representation");
                }
            } else {
                return new ConditionResult(false, "Unknown boolean reader operation type");
            }
        } else {
            return new ConditionResult(false, modificationResult.error());
        }
    }
}