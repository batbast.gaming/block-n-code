package eu.tyrano.blockncode.backend.variable.number;

public class BinaryMathExpression extends UnaryMathExpression {
    private final Expression secondNumber;

    public BinaryMathExpression(Expression firstNumber, Expression secondNumber, OperationType type) {
        super(firstNumber, type);
        this.secondNumber = secondNumber;
    }

    public ExpressionResult execute() {
        ExpressionResult expressionResultA = firstNumber.execute();
        ExpressionResult expressionResultB = secondNumber.execute();

        if (expressionResultA.error() == null && expressionResultB.error() == null) {
            if (expressionResultA.variable() instanceof ComplexVariable || expressionResultB.variable() instanceof ComplexVariable) {

                ComplexVariable complexA = new ComplexVariable(expressionResultA.variable());
                ComplexVariable complexB = new ComplexVariable(expressionResultB.variable());

                switch (type) {
                    case add -> {
                        return new ExpressionResult(ComplexMath.add(complexA, complexB), null);
                    }
                    case subtract -> {
                        return new ExpressionResult(ComplexMath.subtract(complexA, complexB), null);
                    }
                    case multiply -> {
                        return new ExpressionResult(ComplexMath.multiply(complexA, complexB), null);
                    }
                    case divide -> {
                        if (complexB.getRealValue() == 0 && complexB.getImaginaryValue() == 0) {
                            return new ExpressionResult(null, "Division by zero is not allowed");
                        } else {
                            return new ExpressionResult(ComplexMath.divide(complexA, complexB), null);
                        }
                    }
                    case pow -> {
                        return new ExpressionResult(ComplexMath.power(complexA, complexB), null);
                    }
                    case intRandom -> {
                        return new ExpressionResult(ComplexMath.randomInt(complexA, complexB), null);
                    }
                    case doubleRandom -> {
                        return new ExpressionResult(ComplexMath.randomDouble(complexA, complexB), null);
                    }
                    case log -> {
                        if ((complexA.getRealValue() == 0 && complexA.getImaginaryValue() == 0)
                            || complexB.getRealValue() == 0 && complexB.getImaginaryValue() == 0) {
                            return new ExpressionResult(null, "Logarithm cannot take argument that is equal to 0");
                        } else {
                            return new ExpressionResult(ComplexMath.log(complexA, complexB), null);
                        }
                    }
                    default -> {
                        return new ExpressionResult(null, "Unknown complex math expression type");
                    }
                }
            } else {
                switch (type) {
                    case add -> {
                        return new ExpressionResult(new NumberVariable(
                                expressionResultA.variable().getRealValue() + expressionResultB.variable().getRealValue()), null);
                    }
                    case subtract -> {
                        return new ExpressionResult(new NumberVariable(
                                expressionResultA.variable().getRealValue() - expressionResultB.variable().getRealValue()), null);
                    }
                    case multiply -> {
                        return new ExpressionResult(new NumberVariable(
                                expressionResultA.variable().getRealValue() * expressionResultB.variable().getRealValue()), null);
                    }
                    case divide -> {
                        if (expressionResultB.variable().getRealValue() != 0) {
                            return new ExpressionResult(new NumberVariable(
                                    expressionResultA.variable().getRealValue() / expressionResultB.variable().getRealValue()), null);
                        } else {
                            return new ExpressionResult(null, "Division by zero is not allowed");
                        }
                    }
                    case pow -> {
                        return new ExpressionResult(new NumberVariable(
                                Math.pow(expressionResultA.variable().getRealValue(), expressionResultB.variable().getRealValue())), null);
                    }
                    case intRandom -> {
                        return new ExpressionResult(new NumberVariable(
                                NumberMath.randomInt(expressionResultA.variable().getRealValue(), expressionResultB.variable().getRealValue())), null);
                    }
                    case doubleRandom -> {
                        return new ExpressionResult(new NumberVariable(
                                NumberMath.randomDouble(expressionResultA.variable().getRealValue(), expressionResultB.variable().getRealValue())), null);
                    }
                    case modulo -> {
                        return new ExpressionResult(new NumberVariable(
                                expressionResultA.variable().getRealValue() % expressionResultB.variable().getRealValue()), null);
                    }
                    case min -> {
                        return new ExpressionResult(new NumberVariable(
                                Math.min(expressionResultA.variable().getRealValue(), expressionResultB.variable().getRealValue())), null);
                    }
                    case max -> {
                        return new ExpressionResult(new NumberVariable(
                                Math.max(expressionResultA.variable().getRealValue(), expressionResultB.variable().getRealValue())), null);
                    }
                    case log -> {
                        return new ExpressionResult(new NumberVariable(
                                Math.log(expressionResultA.variable().getRealValue())
                                / Math.log(expressionResultB.variable().getRealValue())), null);
                    }
                    default -> {
                        return new ExpressionResult(null, "Unknown math expression type");
                    }
                }
            }
        } else {
            return expressionResultA.error() != null ? expressionResultA : expressionResultB;
        }
    }
}
