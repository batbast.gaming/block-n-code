package eu.tyrano.blockncode.backend.variable.string;

public class StringReader extends StringModification {
    private final StringVariable value;

    public StringReader(StringVariable value) {
        this.value = value;
    }

    @Override
    public ModificationResult execute() {
        return new ModificationResult(value.getValue(), null);
    }
}
