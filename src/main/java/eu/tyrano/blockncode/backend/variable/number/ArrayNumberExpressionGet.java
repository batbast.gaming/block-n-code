package eu.tyrano.blockncode.backend.variable.number;

import eu.tyrano.blockncode.backend.variable.array.ArrayExpression;
import eu.tyrano.blockncode.backend.variable.array.ArrayExpressionResult;
import eu.tyrano.blockncode.backend.variable.array.ArrayVariable;
import eu.tyrano.blockncode.backend.variable.bool.BooleanVariable;

public class ArrayNumberExpressionGet<T extends ArrayVariable<?>> extends Expression {
    private final ArrayExpression<T> arrayExpression;
    private final Expression expression;

    public ArrayNumberExpressionGet(ArrayExpression<T> arrayExpression, Expression expression) {
        this.arrayExpression = arrayExpression;
        this.expression = expression;
    }

    @Override
    public ExpressionResult execute() {
        ArrayExpressionResult<T> resultA = arrayExpression.execute();
        ExpressionResult resultB = expression.execute();

        if (resultA.error() == null && resultB.error() == null) {
            if (resultA.variable().getClazz() == NumberVariable.class) {
                return new ExpressionResult(((NumberVariable) resultA.variable()
                                .get((int) resultB.variable().getRealValue())), null);
            } else {
                return new ExpressionResult(null, "Array do not contains number!");
            }
        } else {
            return new ExpressionResult(null, resultA.error() != null ? resultA.error() : resultB.error());
        }
    }
}
