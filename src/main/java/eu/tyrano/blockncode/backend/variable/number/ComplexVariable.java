package eu.tyrano.blockncode.backend.variable.number;

import eu.tyrano.blockncode.backend.variable.Variable;
import net.minecraft.nbt.CompoundTag;

public class ComplexVariable extends NumberVariable {

    private double imaginaryValue;

    public ComplexVariable(double realValue, double imaginaryValue) {
        super(realValue);
        this.imaginaryValue = imaginaryValue;
    }

    public ComplexVariable(NumberVariable numberVariable) {
        super(numberVariable.getRealValue());
        if (numberVariable instanceof ComplexVariable) {
            this.imaginaryValue = ((ComplexVariable) numberVariable).getImaginaryValue();
        } else {
            this.imaginaryValue = 0;
        }
    }

    public ComplexVariable(ComplexVariable complexVariable) {
        super(complexVariable.getRealValue());
        this.imaginaryValue = complexVariable.getImaginaryValue();
    }

    @Override
    public CompoundTag serialize(String key) {
        CompoundTag tag = new CompoundTag();
        tag.putString("type", "complex");
        tag.putString("name", key);
        tag.putDouble("value_real", realValue);
        tag.putDouble("value_imaginary", imaginaryValue);
        return tag;
    }

    @Override
    public void deserialize(CompoundTag serialized) {
        this.realValue = serialized.getDouble("value_real");
        this.imaginaryValue = serialized.getDouble("value_imaginary");
    }

    public double getImaginaryValue() {
        return imaginaryValue;
    }

    @Override
    public Variable copy() {
        return new ComplexVariable(this.realValue, this.imaginaryValue);
    }

    @Override
    public String toString() {
        return realValue + "+i" + imaginaryValue;
    }
}
