package eu.tyrano.blockncode.backend.variable.bool;

public class BooleanConditionReader extends Condition {

    private final BooleanVariable value;

    public BooleanConditionReader(BooleanVariable value) {
        this.value = value;
    }

    @Override
    public ConditionResult execute() {
        return new ConditionResult(value.getData(), null);
    }
}
