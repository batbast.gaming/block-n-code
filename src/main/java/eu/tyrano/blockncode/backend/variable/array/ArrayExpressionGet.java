package eu.tyrano.blockncode.backend.variable.array;

import eu.tyrano.blockncode.backend.variable.number.Expression;
import eu.tyrano.blockncode.backend.variable.number.ExpressionResult;

public class ArrayExpressionGet<T extends ArrayVariable<?>> extends ArrayExpression<T> {
    private final ArrayExpression<T> arrayExpression;
    private final Expression expression;

    public ArrayExpressionGet(ArrayExpression<T> arrayExpression, Expression expression) {
        this.arrayExpression = arrayExpression;
        this.expression = expression;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ArrayExpressionResult<T> execute() {
        ArrayExpressionResult<T> resultA = arrayExpression.execute();
        ExpressionResult resultB = expression.execute();

        if (resultA.error() == null && resultB.error() == null) {
            if (resultA.variable().getClazz() == ArrayVariable.class) {
                return (ArrayExpressionResult<T>)
                        new ArrayExpressionResult<>(resultA.variable()
                                .get((int) resultB.variable().getRealValue()), null);
            } else {
                return new ArrayExpressionResult<>(null, "Array do not contains arrays!");
            }
        } else {
            return new ArrayExpressionResult<>(null, resultA.error() != null ? resultA.error() : resultB.error());
        }
    }
}
