package eu.tyrano.blockncode.backend.variable.array;

import eu.tyrano.blockncode.backend.variable.Variable;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class ArrayVariable<T extends Variable> extends ArrayList<T> implements Variable {

    private final Class<T> clazz;

    public ArrayVariable(ArrayVariable<T> arrayVariable, Class<T> clazz) {
        this(clazz);
        this.addAll(arrayVariable);
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public ArrayVariable(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public CompoundTag serialize(String key) {
        CompoundTag compoundTag = new CompoundTag();
        ListTag listTag = new ListTag();
        int i = 0;
        for (int j = 0; j < this.size(); j++) {
            listTag.add(this.get(j).serialize(String.valueOf(i++)));
        }
        compoundTag.put("variables", listTag);
        return compoundTag;
    }

    @Override
    public void deserialize(CompoundTag serialized) {
        Tag tag = serialized.get("variables");
        try {
            if (tag instanceof ListTag listTag) {
                for (Tag compound : listTag) {
                    if (compound instanceof CompoundTag compoundTag) {
                        T var = clazz.getDeclaredConstructor(String.class).newInstance("");
                        var.deserialize(compoundTag);
                        this.set(Integer.parseInt(compoundTag.getString("name")), var);
                    }
                }
            }
        } catch (InstantiationException | IllegalAccessException | NumberFormatException | InvocationTargetException |
                 NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public boolean add(Variable variable) {
        if (variable.getClass().isAssignableFrom(clazz)) {
            return super.add((T) variable);
        }
        return false;
    }

    @Override
    public Variable copy() {
        ArrayVariable<T> cloned = new ArrayVariable<>(this.clazz);
        this.forEach(v -> cloned.add(v.copy()));
        return cloned;
    }
}