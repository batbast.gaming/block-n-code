package eu.tyrano.blockncode.backend.variable.string;

import eu.tyrano.blockncode.backend.variable.bool.Condition;
import eu.tyrano.blockncode.backend.variable.bool.ConditionResult;

public class BooleanParserModification extends StringModification {

    private final Condition firstCondition;
    private final ModificationType type;

    public BooleanParserModification(Condition firstCondition, ModificationType type) {
        this.firstCondition = firstCondition;
        this.type = type;
    }

    public ModificationResult execute() {
        ConditionResult conditionResult = firstCondition.execute();

        if (conditionResult.error() == null) {
            if (type == ModificationType.parseBoolean) {
                return new ModificationResult(Boolean.toString(conditionResult.result()), null);
            } else {
                return new ModificationResult(null, "Unknown boolean parsing type");
            }
        } else {
            return new ModificationResult(null, conditionResult.error());
        }
    }
}
