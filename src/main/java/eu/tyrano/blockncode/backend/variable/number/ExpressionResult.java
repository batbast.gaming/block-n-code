package eu.tyrano.blockncode.backend.variable.number;

public record ExpressionResult(NumberVariable variable, String error) {
}
