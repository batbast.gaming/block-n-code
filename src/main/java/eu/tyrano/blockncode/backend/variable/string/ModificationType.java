package eu.tyrano.blockncode.backend.variable.string;

public enum ModificationType {
    regex,
    remove,
    append,
    parseBoolean,
    parseNumber,
    readChat,
    playerListen,
    levenshtein
}
