package eu.tyrano.blockncode.backend.variable.bool;

public abstract class Condition {
    public abstract ConditionResult execute();
}