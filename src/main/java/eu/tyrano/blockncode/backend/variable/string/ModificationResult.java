package eu.tyrano.blockncode.backend.variable.string;

public record ModificationResult(String value, String error) {
}
