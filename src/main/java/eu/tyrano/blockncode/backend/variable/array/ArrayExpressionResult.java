package eu.tyrano.blockncode.backend.variable.array;

import eu.tyrano.blockncode.backend.variable.Variable;

public record ArrayExpressionResult<T extends Variable>(T variable, String error) {
}
