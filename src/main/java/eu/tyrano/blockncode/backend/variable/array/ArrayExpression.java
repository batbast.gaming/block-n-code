package eu.tyrano.blockncode.backend.variable.array;

public abstract class ArrayExpression<T extends ArrayVariable<?>> {
    public abstract ArrayExpressionResult<T> execute();
}
