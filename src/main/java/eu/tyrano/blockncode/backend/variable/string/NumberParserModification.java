package eu.tyrano.blockncode.backend.variable.string;

import eu.tyrano.blockncode.backend.variable.number.ComplexVariable;
import eu.tyrano.blockncode.backend.variable.number.Expression;
import eu.tyrano.blockncode.backend.variable.number.ExpressionResult;
import eu.tyrano.blockncode.backend.variable.number.NumberVariable;

public class NumberParserModification extends StringModification {

    private final Expression firstExpression;
    private final ModificationType type;

    public NumberParserModification(Expression firstExpression, ModificationType type) {
        this.firstExpression = firstExpression;
        this.type = type;
    }

    public ModificationResult execute() {
        ExpressionResult expressionResult = firstExpression.execute();

        if (expressionResult.error() == null) {

            NumberVariable result = expressionResult.variable();

            if (type == ModificationType.parseNumber) {
                if (result instanceof ComplexVariable complexResult) {
                    return new ModificationResult(complexResult.getRealValue() + "+i"+ complexResult.getImaginaryValue(), null);
                } else {
                    return new ModificationResult(Double.toString(result.getRealValue()), null);
                }
            } else {
                return new ModificationResult(null, "Unknown boolean parsing type");
            }
        } else {
            return new ModificationResult(null, expressionResult.error());
        }
    }
}
