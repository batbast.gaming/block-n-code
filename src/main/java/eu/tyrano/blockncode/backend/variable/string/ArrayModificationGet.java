package eu.tyrano.blockncode.backend.variable.string;

import eu.tyrano.blockncode.backend.variable.array.ArrayExpression;
import eu.tyrano.blockncode.backend.variable.array.ArrayExpressionResult;
import eu.tyrano.blockncode.backend.variable.array.ArrayVariable;
import eu.tyrano.blockncode.backend.variable.number.Expression;
import eu.tyrano.blockncode.backend.variable.number.ExpressionResult;

public class ArrayModificationGet<T extends ArrayVariable<?>> extends StringModification {
    private final ArrayExpression<T> arrayExpression;
    private final Expression expression;

    public ArrayModificationGet(ArrayExpression<T> arrayExpression, Expression expression) {
        this.arrayExpression = arrayExpression;
        this.expression = expression;
    }

    @Override
    public ModificationResult execute() {
        ArrayExpressionResult<T> resultA = arrayExpression.execute();
        ExpressionResult resultB = expression.execute();

        if (resultA.error() == null && resultB.error() == null) {
            if (resultA.variable().getClazz() == StringVariable.class) {
                return new ModificationResult(((StringVariable) resultA.variable()
                                .get((int) resultB.variable().getRealValue())).value, null);
            } else {
                return new ModificationResult(null, "Array do not contains string!");
            }
        } else {
            return new ModificationResult(null, resultA.error() != null ? resultA.error() : resultB.error());
        }
    }
}
