package eu.tyrano.blockncode.backend.variable.number;

import eu.tyrano.blockncode.backend.variable.string.ModificationResult;
import eu.tyrano.blockncode.backend.variable.string.StringModification;

public class StringNumberReader extends Expression {

    private final StringModification firstModification;
    private final OperationType type;

    public StringNumberReader(StringModification firstModification, OperationType type) {
        this.firstModification = firstModification;
        this.type = type;
    }

    public ExpressionResult execute() {
        ModificationResult modificationResult = firstModification.execute();

        if (modificationResult.error() == null) {

            String result = modificationResult.value();

            if (type == OperationType.parseString) {
                try {
                    double value = Double.parseDouble(result);
                    return new ExpressionResult(new NumberVariable(value), null);
                } catch (NumberFormatException ignored1) {
                    String[] complexResult = result.split("\\+i");
                    try {
                        double realValue = Double.parseDouble(complexResult[0]);
                        double imaginaryValue = Double.parseDouble(complexResult[1]);
                        return new ExpressionResult(new ComplexVariable(realValue, imaginaryValue), null);
                    } catch (NumberFormatException ignored2) {
                        return new ExpressionResult(null, "String is not a number representation");
                    }
                }
            } else {
                return new ExpressionResult(null, "Unknown number reader operation type");
            }
        } else {
            return new ExpressionResult(null, modificationResult.error());
        }
    }
}
