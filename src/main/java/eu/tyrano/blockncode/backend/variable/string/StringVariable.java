package eu.tyrano.blockncode.backend.variable.string;

import eu.tyrano.blockncode.backend.variable.Variable;
import net.minecraft.nbt.CompoundTag;

public class StringVariable implements Variable {
    protected String value;

    public StringVariable(String value) {
        this.value = value;
    }

    public StringVariable(StringVariable stringVariable) {
        this.value = stringVariable.value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public CompoundTag serialize(String key) {
        CompoundTag tag = new CompoundTag();
        tag.putString("type", "string");
        tag.putString("name", key);
        tag.putString("value", this.value);
        return tag;
    }

    @Override
    public void deserialize(CompoundTag serialized) {
        this.value = serialized.getString("value");
    }

    public void setValue(String realValue) {
        this.value = realValue;
    }

    @Override
    public Variable copy() {
        return new StringVariable(this.value);
    }

    @Override
    public String toString() {
        return "\"" + value + "\"";
    }
}
