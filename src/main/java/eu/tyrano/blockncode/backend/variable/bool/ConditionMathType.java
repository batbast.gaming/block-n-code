package eu.tyrano.blockncode.backend.variable.bool;

public enum ConditionMathType {
    equal, different,
    inferior, superior,
    strictInferior, strictSuperior
}
