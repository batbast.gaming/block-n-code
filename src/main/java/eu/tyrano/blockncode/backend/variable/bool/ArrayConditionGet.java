package eu.tyrano.blockncode.backend.variable.bool;

import eu.tyrano.blockncode.backend.variable.array.ArrayExpression;
import eu.tyrano.blockncode.backend.variable.array.ArrayExpressionResult;
import eu.tyrano.blockncode.backend.variable.array.ArrayVariable;
import eu.tyrano.blockncode.backend.variable.number.Expression;
import eu.tyrano.blockncode.backend.variable.number.ExpressionResult;

public class ArrayConditionGet<T extends ArrayVariable<?>> extends Condition {
    private final ArrayExpression<T> arrayExpression;
    private final Expression expression;

    public ArrayConditionGet(ArrayExpression<T> arrayExpression, Expression expression) {
        this.arrayExpression = arrayExpression;
        this.expression = expression;
    }

    @Override
    public ConditionResult execute() {
        ArrayExpressionResult<T> resultA = arrayExpression.execute();
        ExpressionResult resultB = expression.execute();

        if (resultA.error() == null && resultB.error() == null) {
            if (resultA.variable().getClazz() == BooleanVariable.class) {
                return new ConditionResult(((BooleanVariable) resultA.variable()
                                .get((int) resultB.variable().getRealValue())).getData(), null);
            } else {
                return new ConditionResult(true, "Array do not contains boolean!");
            }
        } else {
            return new ConditionResult(true, resultA.error() != null ? resultA.error() : resultB.error());
        }
    }
}
