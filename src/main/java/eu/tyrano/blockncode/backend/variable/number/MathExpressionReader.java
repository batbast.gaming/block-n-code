package eu.tyrano.blockncode.backend.variable.number;

public class MathExpressionReader extends Expression {

    private final NumberVariable value;

    public MathExpressionReader(NumberVariable value) {
        this.value = value;
    }

    @Override
    public ExpressionResult execute() {
        return new ExpressionResult(value, null);
    }
}
