package eu.tyrano.blockncode.backend.variable.bool;

public record ConditionResult(boolean result, String error) {
}
