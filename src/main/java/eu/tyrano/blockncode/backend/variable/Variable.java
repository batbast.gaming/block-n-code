package eu.tyrano.blockncode.backend.variable;

import net.minecraft.nbt.CompoundTag;

public interface Variable {
    @Override
    String toString();

    CompoundTag serialize(String key);

    void deserialize(CompoundTag serialized);

    Variable copy();
}
