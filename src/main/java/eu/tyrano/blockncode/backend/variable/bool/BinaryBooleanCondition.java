package eu.tyrano.blockncode.backend.variable.bool;

public class BinaryBooleanCondition extends UnaryBooleanCondition {
    private final Condition secondCondition;

    public BinaryBooleanCondition(Condition firstCondition, Condition secondCondition, ConditionBooleanType type) {
        super(firstCondition, type);
        this.secondCondition = secondCondition;
    }

    @Override
    public ConditionResult execute() {
        ConditionResult resultA = firstCondition.execute();
        ConditionResult resultB = secondCondition.execute();

        if (resultA.error() == null && resultB.error() == null) {

            switch (type) {
                case and -> {
                    return new ConditionResult(resultA.result() && resultB.result(), null);
                }
                case or -> {
                    return new ConditionResult(resultA.result() || resultB.result(), null);
                }
                case nor -> {
                    return new ConditionResult(!resultA.result() && !resultB.result(), null);
                }
                case xor -> {
                    return new ConditionResult(resultA.result() ^ resultB.result(), null);
                }
                default -> {
                    return new ConditionResult(false, "Unknown binary condition type");
                }
            }
        } else {
            return resultA.error() != null ? resultA : resultB;
        }
    }
}
