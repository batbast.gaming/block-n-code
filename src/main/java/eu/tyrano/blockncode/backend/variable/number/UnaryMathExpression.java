package eu.tyrano.blockncode.backend.variable.number;

public class UnaryMathExpression extends Expression {
    protected final Expression firstNumber;
    protected final OperationType type;

    public UnaryMathExpression(Expression firstNumber, OperationType type) {
        this.firstNumber = firstNumber;
        this.type = type;
    }

    public ExpressionResult execute() {
        ExpressionResult expressionResult = firstNumber.execute();

        NumberVariable result = expressionResult.variable();

        if (expressionResult.error() == null) {
            if (result instanceof ComplexVariable complexResult) {
                switch (type) {
                    case sqrt -> {
                        return new ExpressionResult(ComplexMath.squareRoot(complexResult), null);
                    }
                    case absolute -> {
                        return new ExpressionResult(ComplexMath.absolute(complexResult), null);
                    }
                    case read -> {
                        return new ExpressionResult(new ComplexVariable(complexResult), null);
                    }
                    case getImaginary -> {
                        return new ExpressionResult(new NumberVariable(complexResult.getImaginaryValue()), null);
                    }
                    case getReal -> {
                        return new ExpressionResult(new NumberVariable(complexResult.getRealValue()), null);
                    }
                    case sin -> {
                        return new ExpressionResult(ComplexMath.sin(complexResult), null);
                    }
                    case cos -> {
                        return new ExpressionResult(ComplexMath.cos(complexResult), null);
                    }
                    case tan -> {
                        return new ExpressionResult(ComplexMath.tan(complexResult), null);
                    }
                    case ln -> {
                        if (complexResult.getRealValue() == 0 && complexResult.getImaginaryValue() == 0) {
                            return new ExpressionResult(null, "The natural logarithm of 0 is not defined");
                        } else {
                            return new ExpressionResult(ComplexMath.ln(complexResult), null);
                        }
                    }
                    default -> {
                        return new ExpressionResult(null, "Unknown complex math expression type");
                    }
                }
            } else {
                switch (type) {
                    case sqrt -> {
                        double real = result.getRealValue();
                        if (real >= 0) {
                            return new ExpressionResult(new NumberVariable(Math.sqrt(real)), null);
                        } else {
                            return new ExpressionResult(new ComplexVariable(0, Math.sqrt(Math.abs(real))), null);
                        }
                    }
                    case absolute -> {
                        return new ExpressionResult(new NumberVariable(Math.abs(result.getRealValue())), null);
                    }
                    case read, getReal -> {
                        return new ExpressionResult(new NumberVariable(result.getRealValue()), null);
                    }
                    case getImaginary -> {
                        return new ExpressionResult(new NumberVariable(0), null);
                    }
                    case sin -> {
                        return new ExpressionResult(new NumberVariable(Math.sin(result.getRealValue())), null);
                    }
                    case cos -> {
                        return new ExpressionResult(new NumberVariable(Math.cos(result.getRealValue())), null);
                    }
                    case tan -> {
                        return new ExpressionResult(new NumberVariable(Math.tan(result.getRealValue())), null);
                    }
                    default -> {
                        return new ExpressionResult(null, "Unknown math expression type");
                    }
                }
            }
        } else {
            return expressionResult;
        }
    }
}