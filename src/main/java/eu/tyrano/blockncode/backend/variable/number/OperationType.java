package eu.tyrano.blockncode.backend.variable.number;

public enum OperationType {
    add,
    subtract,
    multiply,
    divide,
    pow,
    sqrt,
    absolute,
    read,
    intRandom,
    doubleRandom,
    getReal,
    getImaginary,
    modulo,
    sin,
    cos,
    tan,
    parseString,
    min,
    max,
    log,
    ln
}
