package eu.tyrano.blockncode.backend.variable.bool;

import eu.tyrano.blockncode.backend.variable.Variable;
import net.minecraft.nbt.CompoundTag;

public class BooleanVariable implements Variable {

    private boolean data;

    public BooleanVariable(boolean data) {
        this.data = data;
    }

    @Override
    public CompoundTag serialize(String key) {
        CompoundTag tag = new CompoundTag();
        tag.putString("type", "boolean");
        tag.putString("name", key);
        tag.putBoolean("value", this.data);
        return tag;
    }

    @Override
    public void deserialize(CompoundTag serialized) {
        this.data = serialized.getBoolean("value");
    }

    @Override
    public Variable copy() {
        return new BooleanVariable(this.data);
    }

    public void setData(boolean data) {
        this.data = data;
    }

    public boolean getData() {
        return data;
    }

    @Override
    public String toString() {
        return String.valueOf(data);
    }
}
