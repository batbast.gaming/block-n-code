package eu.tyrano.blockncode.backend.variable.number;

public class NumberMath {
    public static int randomInt(double max, double min) {

        int intMax = (int) Math.floor(Math.max(max, min));
        int intMin = (int) Math.ceil(Math.min(max, min));
        int intDiff = intMax - intMin;

        return (int) ((Math.random() * intDiff) + intMin);
    }

    public static double randomDouble(double max, double min) {

        double doubleMax =  Math.floor(Math.max(max, min));
        double doubleMin = Math.ceil(Math.min(max, min));
        double doubleDiff = doubleMax - doubleMin;

        return (Math.random() * doubleDiff) + doubleMin;
    }
}
