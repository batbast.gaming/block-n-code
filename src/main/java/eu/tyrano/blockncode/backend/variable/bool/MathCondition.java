package eu.tyrano.blockncode.backend.variable.bool;

import eu.tyrano.blockncode.backend.variable.number.ComplexVariable;
import eu.tyrano.blockncode.backend.variable.number.Expression;
import eu.tyrano.blockncode.backend.variable.number.ExpressionResult;
import eu.tyrano.blockncode.backend.variable.number.NumberVariable;

import java.util.UUID;

public class MathCondition extends Condition {
    private final ConditionMathType type;
    private final Expression firstExpression;
    private final Expression secondExpression;

    public MathCondition(Expression firstExpression, Expression secondExpression, ConditionMathType type) {
        this.type = type;
        this.firstExpression = firstExpression;
        this.secondExpression = secondExpression;
    }

    @Override
    public ConditionResult execute() {

        ExpressionResult expressionResultA = firstExpression.execute();
        ExpressionResult expressionResultB = secondExpression.execute();

        NumberVariable resultA = expressionResultA.variable();
        NumberVariable resultB = expressionResultB.variable();

        if (expressionResultA.error() == null && expressionResultB.error() == null) {
            if (resultA instanceof ComplexVariable || resultB instanceof ComplexVariable) {

                ComplexVariable complexA = new ComplexVariable(resultA);
                ComplexVariable complexB = new ComplexVariable(resultB);

                switch (type) {
                    case equal -> {
                        return new ConditionResult(complexA.getRealValue() == complexB.getRealValue() && complexA.getImaginaryValue() == complexB.getImaginaryValue(), null);
                    }
                    case different -> {
                        return new ConditionResult(complexA.getRealValue() != complexB.getRealValue() || complexA.getImaginaryValue() != complexB.getImaginaryValue(), null);
                    }
                    default -> {
                        return new ConditionResult(false, "Unknown complex math condition type");
                    }
                }
            } else {
                switch (type) {
                    case equal -> {
                        return new ConditionResult(resultA.getRealValue() == resultB.getRealValue(), null);
                    }
                    case different -> {
                        return new ConditionResult(resultA.getRealValue() != resultB.getRealValue(), null);
                    }
                    case inferior -> {
                        return new ConditionResult(resultA.getRealValue() <= resultB.getRealValue(), null);
                    }
                    case superior -> {
                        return new ConditionResult(resultA.getRealValue() >= resultB.getRealValue(), null);
                    }
                    case strictInferior -> {
                        return new ConditionResult(resultA.getRealValue() < resultB.getRealValue(), null);
                    }
                    case strictSuperior -> {
                        return new ConditionResult(resultA.getRealValue() > resultB.getRealValue(), null);
                    }
                    default -> {
                        return new ConditionResult(false, "Unknown math condition type");
                    }
                }
            }
        } else {
            return expressionResultA.error() != null ? new ConditionResult(false, expressionResultA.error()) : new ConditionResult(false, expressionResultB.error());
        }
    }
}