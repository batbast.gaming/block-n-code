package eu.tyrano.blockncode.backend.variable.bool;

public class UnaryBooleanCondition extends Condition {
    protected final Condition firstCondition;
    protected final ConditionBooleanType type;

    public UnaryBooleanCondition(Condition firstCondition, ConditionBooleanType type) {
        this.firstCondition = firstCondition;
        this.type = type;
    }

    @Override
    public ConditionResult execute() {
        ConditionResult result = firstCondition.execute();

        if (result.error() == null) {
            switch (type) {
                case read -> {
                    return new ConditionResult(result.result(), null);
                }
                case no -> {
                    return new ConditionResult(!result.result(), null);
                }
                default -> {
                    return new ConditionResult(false, "Unknown unary condition type");
                }
            }
        } else {
            return result;
        }
    }
}
