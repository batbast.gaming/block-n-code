package eu.tyrano.blockncode.backend.variable.string;

public abstract class StringModification {
    public abstract ModificationResult execute();
}
