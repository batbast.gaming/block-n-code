package eu.tyrano.blockncode.backend.variable.number;

import eu.tyrano.blockncode.backend.variable.Variable;
import net.minecraft.nbt.CompoundTag;

public class NumberVariable implements Variable {

    protected double realValue;

    public NumberVariable(double real) {
        this.realValue = real;
    }

    public NumberVariable(NumberVariable numberVariable) {
        this.realValue = numberVariable.realValue;
    }

    public double getRealValue() {
        return realValue;
    }

    @Override
    public CompoundTag serialize(String key) {
        CompoundTag tag = new CompoundTag();
        tag.putString("type", "number");
        tag.putString("name", key);
        tag.putDouble("value_real", realValue);
        return tag;
    }

    @Override
    public void deserialize(CompoundTag serialized) {
        this.realValue = serialized.getDouble("value_real");
    }

    @Override
    public Variable copy() {
        return new NumberVariable(this.realValue);
    }

    @Override
    public String toString() {
        return String.valueOf(realValue);
    }
}
