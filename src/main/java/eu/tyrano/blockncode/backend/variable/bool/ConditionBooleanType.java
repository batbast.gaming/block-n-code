package eu.tyrano.blockncode.backend.variable.bool;

public enum ConditionBooleanType {
    and,
    or,
    nor,
    xor,
    no,
    read,
    parseString
}
