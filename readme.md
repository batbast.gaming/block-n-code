# Block'n'Code
**By Tyranobast**

## 0 - Why using Block'n'Code ?

Block'n'Code make Minecraft automation possible beyond every limit, and help every one learning imperative programming without any bases requirement



## 1 - Installation prerequisites

To use this mod you'll need Minecraft Forge 1.18.2 :
(https://files.minecraftforge.net/net/minecraftforge/forge/index_1.18.2.html)

Here is the Minecraft Forge official installation tutorial :
(https://github.com/MinecraftForge/MinecraftForge#installing-forge)



## 2 - Block'n'Code installation

To install the Block'n'Code mod, you must move the compiled jar into your .minecraft/mods folder



## 3 - Block'n'Code launch

Once installed, Block'n'Code mod will automatically be launched with the 1.18.2 Forge instance. You can verify if the installation succeed by checking in the Mods button on the Minecraft main menu



## 4 - How to use it

[Wiki](https://gitlab.com/batbast.gaming/block-n-code/-/wikis/home)



## 5 - Contacts

<blockncode@tyrano.eu>
